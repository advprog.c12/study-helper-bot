package io.java.studyhelper.studyhelper.core.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.commands.Command;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommandValidatorTest {

    private Class<?> commandValidatorClass;
    private CommandValidator instance;

    @BeforeEach
    public void setUp() throws Exception {
        commandValidatorClass = Class.forName("io.java.studyhelper.studyhelper.core.validator.CommandValidator");
        instance = new CommandValidator();
    }

    @Test
    public void testCommandValidatorIsPublicClass() {
        int classModifiers = commandValidatorClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new GuildOnlyValidator() instanceof Validator);
    }

    @Test
    public void testHasValidateMethod() throws NoSuchMethodException {
        Method method = commandValidatorClass.getDeclaredMethod("validate", Message.class, List.class, Command.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(!Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testValidateSuccess() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(false);
        when(command.isDMOnly()).thenReturn(false);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.VALID, validityStatus);

        verify(command, times(1)).isArgsRequired();
        verify(command, times(1)).isGuildOnly();
        verify(message, times(2)).getChannel();
        verify(messageChannel, times(0)).getType();
    }

    @Test
    public void testValidateFailAtCommandExistValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(false);
        when(command.isDMOnly()).thenReturn(false);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);

        ValidityStatus validityStatus = instance.validate(message, args, null);

        assertEquals(ValidityStatus.INVALID_COMMAND, validityStatus);

        verify(command, times(0)).isArgsRequired();
        verify(command, times(0)).isGuildOnly();
        verify(message, times(0)).getChannel();
        verify(messageChannel, times(0)).getType();
    }

    @Test
    public void testValidateFailAtArgsValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isArgsRequired()).thenReturn(true);
        when(command.isGuildOnly()).thenReturn(false);
        when(command.isDMOnly()).thenReturn(false);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.INVALID_ARGS, validityStatus);

        verify(command, times(1)).isArgsRequired();
        verify(command, times(0)).isGuildOnly();
        verify(message, times(0)).getChannel();
        verify(messageChannel, times(0)).getType();
    }

    @Test
    public void testValidateFailAtGuildOnlyValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(true);
        when(command.isDMOnly()).thenReturn(false);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.INVALID_CHANNEL_GUILD_ONLY, validityStatus);

        verify(command, times(1)).isArgsRequired();
        verify(command, times(1)).isGuildOnly();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).getType();
    }

    @Test
    public void testValidateFailAtDMOnlyValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(false);
        when(command.isDMOnly()).thenReturn(true);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.INVALID_CHANNEL_DM_ONLY, validityStatus);

        verify(command, times(1)).isArgsRequired();
        verify(command, times(1)).isGuildOnly();
        verify(message, times(2)).getChannel();
        verify(messageChannel, times(1)).getType();
    }
}
