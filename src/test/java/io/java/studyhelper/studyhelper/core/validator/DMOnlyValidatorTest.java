package io.java.studyhelper.studyhelper.core.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.commands.Command;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DMOnlyValidatorTest {

    private Class<?> dmOnlyValidatorClass;
    private DMOnlyValidator instance;

    @BeforeEach
    public void setUp() throws Exception {
        dmOnlyValidatorClass = Class.forName("io.java.studyhelper.studyhelper.core.validator.DMOnlyValidator");
        instance = new DMOnlyValidator();
    }

    @Test
    public void testGuildOnlyValidatorIsPublicClass() {
        int classModifiers = dmOnlyValidatorClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new DMOnlyValidator() instanceof Validator);
    }

    @Test
    public void testHasValidateMethod() throws NoSuchMethodException {
        Method method = dmOnlyValidatorClass.getDeclaredMethod("validate", Message.class, List.class, Command.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(!Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testHasSetNextValidator() throws NoSuchMethodException {
        Method method = dmOnlyValidatorClass.getDeclaredMethod("setNextValidator", Validator.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testValidateSuccess() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isDMOnly()).thenReturn(false);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.VALID, validityStatus);

        verify(command, times(1)).isDMOnly();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(0)).getType();
    }

    @Test
    public void testValidateFail() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        when(command.isDMOnly()).thenReturn(true);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.INVALID_CHANNEL_DM_ONLY, validityStatus);

        verify(command, times(1)).isDMOnly();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).getType();
    }

    @Test
    public void testValidateNextHandler() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();
        Validator nextValidator = mock(Validator.class);

        when(command.isDMOnly()).thenReturn(false);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(nextValidator.validate(any(Message.class), any(List.class), any(Command.class)))
                .thenReturn(ValidityStatus.INVALID_COMMAND);

        instance.setNextValidator(nextValidator);
        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.INVALID_COMMAND, validityStatus);

        verify(command, times(1)).isDMOnly();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(0)).getType();
    }
}
