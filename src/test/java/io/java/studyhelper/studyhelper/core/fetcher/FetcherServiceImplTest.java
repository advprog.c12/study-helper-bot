package io.java.studyhelper.studyhelper.core.fetcher;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FetcherServiceImplTest {

    @InjectMocks
    private FetcherServiceImpl fetcherService;

    @Test
    public void testSendGetRequest() throws Exception {
        String url = "https://jsonplaceholder.typicode.com/todos/1";
        String response = fetcherService.sendGetRequest(url);
        assertEquals("{  " +
                "\"userId\": 1,  " +
                "\"id\": 1,  " +
                "\"title\": \"delectus aut autem\",  " +
                "\"completed\": false" +
                "}", response);
    }

    @Test
    public void testSendPostRequest() throws Exception {
        String url = "https://jsonplaceholder.typicode.com/posts";
        String response = fetcherService.sendPostRequest(url, "");
        assertEquals("{  \"id\": 101}", response);
    }
}
