package io.java.studyhelper.studyhelper.core.validator;

import static org.junit.jupiter.api.Assertions.assertTrue;

import io.java.studyhelper.studyhelper.commands.Command;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ValidatorTest {

    private Class<?> validatorClass;

    @BeforeEach
    public void setUp() throws Exception {
        validatorClass = Class.forName("io.java.studyhelper.studyhelper.core.validator.Validator");
    }

    @Test
    public void testValidatorIsPublicInterface() {
        int classModifiers = validatorClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testHasValidateMethod() throws NoSuchMethodException {
        Method method = validatorClass.getDeclaredMethod("validate", Message.class, List.class, Command.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
