package io.java.studyhelper.studyhelper.core.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.internal.entities.ReceivedMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommandParserTest {

    private Class<?> commandParserClass;
    private CommandParser instance;

    @BeforeEach
    public void setUp() throws Exception {
        commandParserClass = Class.forName("io.java.studyhelper.studyhelper.core.tools.CommandParser");
        instance = new CommandParser();
    }

    @Test
    public void testCommandParserIsPublicClass() {
        int classModifiers = commandParserClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testHasTokenizeMessageMethod() throws NoSuchMethodException {
        Method method = commandParserClass.getDeclaredMethod("tokenizeMessage", Message.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPrivate(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testHasExtractCommandNameMethod() throws NoSuchMethodException {
        Method method = commandParserClass.getDeclaredMethod("extractCommandName", Message.class, String.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testHasExtractCommandArgsMethod() throws NoSuchMethodException {
        Method method = commandParserClass.getDeclaredMethod("extractCommandArgs", Message.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testExtractCommandNameMethodFunctionality() {
        String prefix = "!";
        String command = "echo";
        String args = "Hello World!";

        Message message = mock(ReceivedMessage.class);

        when(message.getContentRaw()).thenReturn(String.format("%s%s %s", prefix, command, args));

        String commandName = CommandParser.extractCommandName(message, prefix);

        assertEquals(command, commandName);

        verify(message, times(1)).getContentRaw();
    }

    @Test
    public void testExtractCommandNameMethodDynamicBasedOnPrefix() {
        String prefixShort = "!";
        String prefixLong = "!!!";
        String command = "echo";
        String args = "Hello World!";

        Message message = mock(ReceivedMessage.class);

        when(message.getContentRaw()).thenReturn(String.format("%s%s %s", prefixShort, command, args));
        String commandNameShort = CommandParser.extractCommandName(message, prefixShort);

        when(message.getContentRaw()).thenReturn(String.format("%s%s %s", prefixLong, command, args));
        String commandNameLong = CommandParser.extractCommandName(message, prefixLong);

        assertEquals(command, commandNameShort);
        assertEquals(command, commandNameLong);

        verify(message, times(2)).getContentRaw();
    }

    @Test
    public void testExtractCommandArgsFunctionality() {
        String prefix = "!";
        String command = "echo";
        String args = "Hello World!";

        Message message = mock(ReceivedMessage.class);

        when(message.getContentRaw()).thenReturn(String.format("%s%s %s", prefix, command, args));

        List<String> commandArgs = CommandParser.extractCommandArgs(message);

        assertEquals(2, commandArgs.size());
        assertEquals("Hello", commandArgs.get(0));
        assertEquals("World!", commandArgs.get(1));

        verify(message, times(1)).getContentRaw();
    }

    @Test
    public void testExtractCommandArgsWhenNoArgs() {
        String prefix = "!";
        String command = "ping";
        String args = "";

        Message message = mock(ReceivedMessage.class);

        when(message.getContentRaw()).thenReturn(String.format("%s%s %s", prefix, command, args));

        List<String> commandArgs = CommandParser.extractCommandArgs(message);

        assertEquals(0, commandArgs.size());

        verify(message, times(1)).getContentRaw();
    }
}
