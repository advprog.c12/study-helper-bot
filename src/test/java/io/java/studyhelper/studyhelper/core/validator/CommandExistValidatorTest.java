package io.java.studyhelper.studyhelper.core.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommandExistValidatorTest {

    private Class<?> commandExistValidatorClass;
    private CommandExistValidator instance;

    @BeforeEach
    public void setUp() throws Exception {
        commandExistValidatorClass = Class.forName(
                "io.java.studyhelper.studyhelper.core.validator.CommandExistValidator");
        instance = new CommandExistValidator();
    }

    @Test
    public void testCommandExistValidatorIsPublicClass() {
        int classModifiers = commandExistValidatorClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new GuildOnlyValidator() instanceof Validator);
    }

    @Test
    public void testHasValidateMethod() throws NoSuchMethodException {
        Method method = commandExistValidatorClass.getDeclaredMethod("validate",
                Message.class, List.class, Command.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(!Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testHasSetNextValidator() throws NoSuchMethodException {
        Method method = commandExistValidatorClass.getDeclaredMethod("setNextValidator", Validator.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testValidateSuccess() {
        Message message = mock(Message.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();

        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.VALID, validityStatus);
    }

    @Test
    public void testValidateFail() {
        Message message = mock(Message.class);
        List<String> args = new ArrayList<>();

        ValidityStatus validityStatus = instance.validate(message, args, null);

        assertEquals(ValidityStatus.INVALID_COMMAND, validityStatus);
    }

    @Test
    public void testValidateNextHandler() {
        Message message = mock(Message.class);
        Command command = mock(Command.class);
        List<String> args = new ArrayList<>();
        Validator nextValidator = mock(Validator.class);

        when(nextValidator.validate(any(Message.class), any(List.class), any(Command.class)))
                .thenReturn(ValidityStatus.INVALID_ARGS);

        instance.setNextValidator(nextValidator);
        ValidityStatus validityStatus = instance.validate(message, args, command);

        assertEquals(ValidityStatus.INVALID_ARGS, validityStatus);
    }
}
