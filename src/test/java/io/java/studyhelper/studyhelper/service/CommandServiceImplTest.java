package io.java.studyhelper.studyhelper.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.repository.CommandRepository;
import java.lang.reflect.Field;
import java.util.ArrayList;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CommandServiceImplTest {

    @Mock
    private CommandRepository commandRepository;

    @InjectMocks
    private CommandServiceImpl commandService;

    @BeforeEach
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        Field prefixField = CommandServiceImpl.class.getDeclaredField("prefix");
        prefixField.setAccessible(true);
        prefixField.set(commandService, "!");
    }

    @Test
    public void testProcessMessageValid() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        Command command = mock(Command.class);
        String messageContent = "!commandName args1 args2";

        when(message.getContentRaw()).thenReturn(messageContent);
        when(message.getChannel()).thenReturn(messageChannel);
        when(commandRepository.getCommandByName(anyString())).thenReturn(command);

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(false);
        when(command.isDMOnly()).thenReturn(false);

        commandService.processMessage(message);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(1)).executeCommand(any(Message.class), any(ArrayList.class));
    }

    @Test
    public void testProcessMessageInvalidAtCommandExistValidator() {
        Message message = mock(Message.class);
        Command command = mock(Command.class);
        String messageContent = "!commandName args1 args2";

        when(message.getContentRaw()).thenReturn(messageContent);
        when(commandRepository.getCommandByName(anyString())).thenReturn(null);

        commandService.processMessage(message);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(0)).executeCommand(any(Message.class), any(ArrayList.class));
    }

    @Test
    public void testProcessMessageInvalidAtArgsValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        Command command = mock(Command.class);
        String messageContent = "!commandName";

        when(message.getContentRaw()).thenReturn(messageContent);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);
        when(commandRepository.getCommandByName(anyString())).thenReturn(command);

        when(command.isArgsRequired()).thenReturn(true);

        commandService.processMessage(message);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(0)).executeCommand(any(Message.class), any(ArrayList.class));
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        verify(messageAction, times(1)).queue();
    }

    @Test
    public void testProcessMessageInvalidAtGuildOnlyValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        Command command = mock(Command.class);
        String messageContent = "!commandName args1 args2";

        when(message.getContentRaw()).thenReturn(messageContent);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);
        when(commandRepository.getCommandByName(anyString())).thenReturn(command);

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(true);

        commandService.processMessage(message);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(0)).executeCommand(any(Message.class), any(ArrayList.class));
        verify(message, times(2)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        verify(messageAction, times(1)).queue();
    }

    @Test
    public void testProcessMessageInvalidAtDMOnlyValidator() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        Command command = mock(Command.class);
        String messageContent = "!commandName args1 args2";

        when(message.getContentRaw()).thenReturn(messageContent);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);
        when(commandRepository.getCommandByName(anyString())).thenReturn(command);

        when(command.isArgsRequired()).thenReturn(false);
        when(command.isGuildOnly()).thenReturn(false);
        when(command.isDMOnly()).thenReturn(true);

        commandService.processMessage(message);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(0)).executeCommand(any(Message.class), any(ArrayList.class));
        verify(message, times(3)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        verify(messageAction, times(1)).queue();
    }
}
