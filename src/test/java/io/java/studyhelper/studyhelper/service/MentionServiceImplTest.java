package io.java.studyhelper.studyhelper.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveService;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MentionServiceImplTest {

    @Mock
    private ArchiveService archiveService;

    @InjectMocks
    private MentionServiceImpl mentionService;

    @Test
    public void testSaveMessageMethodWantsArchive() {
        Message message = mock(Message.class);
        Guild guild = mock(Guild.class);

        User mentionedUser = mock(User.class);
        List<User> mentionedUsers = new ArrayList<>();
        mentionedUsers.add(mentionedUser);

        when(message.getMentionedUsers()).thenReturn(mentionedUsers);
        when(message.getGuild()).thenReturn(guild);
        when(archiveService.userWantsArchive(any(User.class), any(Guild.class))).thenReturn(true);

        mentionService.saveMessage(message);

        verify(archiveService, times(1)).userWantsArchive(any(User.class), any(Guild.class));
        verify(archiveService, times(1)).saveMessageForUser(any(User.class), any(Message.class));
    }

    @Test
    public void testSaveMessageMethodNotWantsArchive() {
        Message message = mock(Message.class);
        Guild guild = mock(Guild.class);

        User mentionedUser = mock(User.class);
        List<User> mentionedUsers = new ArrayList<>();
        mentionedUsers.add(mentionedUser);

        when(message.getMentionedUsers()).thenReturn(mentionedUsers);
        when(message.getGuild()).thenReturn(guild);
        when(archiveService.userWantsArchive(any(User.class), any(Guild.class))).thenReturn(false);

        mentionService.saveMessage(message);

        verify(archiveService, times(1)).userWantsArchive(any(User.class), any(Guild.class));
        verify(archiveService, times(0)).saveMessageForUser(any(User.class), any(Message.class));
    }
}
