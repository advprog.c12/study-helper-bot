package io.java.studyhelper.studyhelper.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

@Repository
public class CommandRepositoryImplTest {

    private CommandRepository commandRepository;

    @Mock
    private Map<String, Command>  commands;

    @Mock
    private Map<String, String> commandAliases;

    private Command sampleCommand;

    @BeforeEach
    public void setUp() {
        commandRepository = new CommandRepositoryImpl();
        commands = new HashMap<>();
        commandAliases = new HashMap<>();
        sampleCommand = mock(Command.class);

        when(sampleCommand.getCommandName()).thenReturn("commandName");
        when(sampleCommand.getCommandAliases()).thenReturn(new String[] {"cmd"});
        commands.put(sampleCommand.getCommandName(), sampleCommand);
        commandAliases.put(sampleCommand.getCommandName(), sampleCommand.getCommandName());
        commandAliases.put(sampleCommand.getCommandAliases()[0], sampleCommand.getCommandName());
    }

    @Test
    public void testGetAllCommands() {
        ReflectionTestUtils.setField(commandRepository, "commands", commands);
        ReflectionTestUtils.setField(commandRepository, "commandAliases", commandAliases);

        List<Command> acquiredCommands = commandRepository.getAllCommands();

        assertThat(acquiredCommands).isEqualTo(new ArrayList<>(commands.values()));
    }

    @Test
    public void testGetCommandByName() {
        ReflectionTestUtils.setField(commandRepository, "commands", commands);
        ReflectionTestUtils.setField(commandRepository, "commandAliases", commandAliases);

        Command acquiredCommand = commandRepository.getCommandByName("commandName");

        assertThat(acquiredCommand).isEqualTo(sampleCommand);
    }

    @Test
    public void testAddCommand() {
        ReflectionTestUtils.setField(commandRepository, "commands", commands);
        ReflectionTestUtils.setField(commandRepository, "commandAliases", commandAliases);

        Command newSampleCommand = mock(Command.class);
        when(newSampleCommand.getCommandName()).thenReturn("newSampleCommand");
        when(newSampleCommand.getCommandAliases()).thenReturn(new String[0]);

        commandRepository.addCommand(newSampleCommand);
        Command acquiredCommand = commandRepository.getCommandByName("newSampleCommand");

        assertThat(acquiredCommand).isEqualTo(newSampleCommand);
    }
}
