package io.java.studyhelper.studyhelper.listeners;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.SelfUser;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.managers.Presence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ReadyEventListenerTest {

    @InjectMocks
    private ReadyEventListener readyEventListener;

    private Class<?> readyEventListenerClass;

    @BeforeEach
    public void setUp() throws Exception {
        readyEventListenerClass = Class.forName("io.java.studyhelper.studyhelper.listeners.ReadyEventListener");
    }

    @Test
    public void testReadyEventListenerIsPublicClass() {
        int classModifiers = readyEventListenerClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new ReadyEventListener() instanceof ListenerAdapter);
    }

    @Test
    public void testOnReadyExist() throws NoSuchMethodException {
        Method method = readyEventListenerClass.getDeclaredMethod("onReady", ReadyEvent.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOnReadyFunctionality() throws InterruptedException {
        ReadyEvent readyEvent = mock(ReadyEvent.class);
        JDA jdaClient = mock(JDA.class);
        SelfUser selfUser = mock(SelfUser.class);
        Presence presence = mock(Presence.class);

        when(readyEvent.getJDA()).thenReturn(jdaClient);
        when(jdaClient.getSelfUser()).thenReturn(selfUser);
        when(selfUser.getName()).thenReturn("BotName");
        when(selfUser.getDiscriminator()).thenReturn("1234");
        when(jdaClient.getPresence()).thenReturn(presence);

        readyEventListener.onReady(readyEvent);

        Thread.sleep(30000);

        verify(readyEvent, times(1)).getJDA();
        verify(jdaClient, times(1)).getSelfUser();
        verify(selfUser, times(1)).getName();
        verify(selfUser, times(1)).getDiscriminator();
        verify(jdaClient, atLeast(1)).getPresence();
        verify(presence, atLeast(1)).setActivity(any(Activity.class));
    }
}
