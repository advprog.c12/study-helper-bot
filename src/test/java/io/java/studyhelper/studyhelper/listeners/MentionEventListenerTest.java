package io.java.studyhelper.studyhelper.listeners;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.service.MentionService;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MentionEventListenerTest {

    @Mock
    private MentionService mentionService;

    @InjectMocks
    private MentionEventListener mentionEventListener;

    private Class<?> mentionEventListenerClass;

    @BeforeEach
    public void setUp() throws Exception {
        mentionEventListenerClass = Class.forName("io.java.studyhelper.studyhelper.listeners.MentionEventListener");
    }

    @Test
    public void testMentionEventListenerIsPublicClass() {
        int classModifiers = mentionEventListenerClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new MentionEventListener() instanceof ListenerAdapter);
    }

    @Test
    public void testOnMessageReceivedMethodExist() throws NoSuchMethodException {
        Method method = mentionEventListenerClass.getDeclaredMethod("onMessageReceived", MessageReceivedEvent.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHandlerPassToService() {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(Message.class);
        User author = mock(User.class);

        User mentionedUser = mock(User.class);
        List<User> mentionedUsers = new ArrayList<>();
        mentionedUsers.add(mentionedUser);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getAuthor()).thenReturn(author);
        when(author.isBot()).thenReturn(false);
        when(message.getMentionedUsers()).thenReturn(mentionedUsers);

        mentionEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).isBot();
        verify(message, times(1)).getMentionedUsers();
        verify(mentionService, times(1)).saveMessage(message);
    }

    @Test
    public void testHandlerException() {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(Message.class);
        User author = mock(User.class);
        String messageContent = "@MentionedUser#1234 content";

        User mentionedUser = mock(User.class);
        List<User> mentionedUsers = new ArrayList<>();
        mentionedUsers.add(mentionedUser);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getAuthor()).thenReturn(author);
        when(author.isBot()).thenReturn(false);
        when(message.getMentionedUsers()).thenReturn(mentionedUsers);
        doThrow(IllegalStateException.class).when(mentionService).saveMessage(any(Message.class));
        when(message.getContentRaw()).thenReturn(messageContent);

        mentionEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).isBot();
        verify(message, times(1)).getContentRaw();
        verify(mentionService, times(1)).saveMessage(message);
    }

    @Test
    public void testHandlerAuthorIsBot() {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(Message.class);
        User author = mock(User.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getAuthor()).thenReturn(author);
        when(author.isBot()).thenReturn(true);

        mentionEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).isBot();
        verify(mentionService, times(0)).saveMessage(message);
    }
}
