package io.java.studyhelper.studyhelper.listeners;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.service.CommandService;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CommandEventListenerTest {

    @Mock
    private CommandService commandService;

    @InjectMocks
    private CommandEventListener commandEventListener;

    private Class<?> commandEventListenerClass;

    @BeforeEach
    public void setUp() throws Exception {
        commandEventListenerClass = Class.forName("io.java.studyhelper.studyhelper.listeners.CommandEventListener");
    }

    @Test
    public void testCommandEventListenerIsPublicClass() {
        int classModifiers = commandEventListenerClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new CommandEventListener() instanceof ListenerAdapter);
    }

    @Test
    public void testOnMessageReceivedMethodExist() throws NoSuchMethodException {
        Method method = commandEventListenerClass.getDeclaredMethod("onMessageReceived", MessageReceivedEvent.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHandlerPassToService() throws NoSuchFieldException, IllegalAccessException {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(Message.class);
        User author = mock(User.class);
        String messageContent = "!command args1 args2";

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getAuthor()).thenReturn(author);
        when(author.isBot()).thenReturn(false);
        when(message.getContentRaw()).thenReturn(messageContent);

        Field prefixField = CommandEventListener.class.getDeclaredField("prefix");
        prefixField.setAccessible(true);
        prefixField.set(commandEventListener, "!");

        commandEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).isBot();
        verify(message, times(1)).getContentRaw();
        verify(commandService, times(1)).processMessage(message);
    }

    @Test
    public void testHandlerException() throws NoSuchFieldException, IllegalAccessException {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        String messageContent = "!command args1 args2";

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getAuthor()).thenReturn(author);
        when(author.isBot()).thenReturn(false);
        when(message.getContentRaw()).thenReturn(messageContent);
        doThrow(IllegalStateException.class).when(commandService).processMessage(any(Message.class));
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        Field prefixField = CommandEventListener.class.getDeclaredField("prefix");
        prefixField.setAccessible(true);
        prefixField.set(commandEventListener, "!");

        commandEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).isBot();
        verify(message, times(2)).getContentRaw();
        verify(commandService, times(1)).processMessage(message);
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        verify(messageAction, times(1)).queue();
    }

    @Test
    public void testHandlerAuthorIsBot() {
        MessageReceivedEvent messageReceivedEvent = mock(MessageReceivedEvent.class);
        Message message = mock(Message.class);
        User author = mock(User.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(message.getAuthor()).thenReturn(author);
        when(author.isBot()).thenReturn(true);

        commandEventListener.onMessageReceived(messageReceivedEvent);

        verify(messageReceivedEvent, times(1)).getMessage();
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).isBot();
        verify(commandService, times(0)).processMessage(message);
    }
}
