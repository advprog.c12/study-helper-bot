package io.java.studyhelper.studyhelper.commands.calculator.microserviceTests;

import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpecialMicroserviceTest {

    private FetcherService fetcher;

    @BeforeEach
    public void setup() throws Exception {
        this.fetcher = new FetcherServiceImpl();
    }

    @Test
    public void testMicroserviceWorksForCNDFCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/special/cndf/1.2");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 0.8849302684180189", reply);
    }

    @Test
    public void testMicroserviceWorksForToBinaryCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/special/toBinary/17");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 10001.0", reply);
    }

    @Test
    public void testMicroserviceWorksForToDecimalCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/special/toDecimal/10010");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 18.0", reply);
    }

}
