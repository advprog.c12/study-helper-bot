package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import java.util.ArrayList;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ShowInfoAllCommandTest {
    @Mock
    private MaterialService materialService;

    @InjectMocks
    private ShowInfoAllCommand showInfoAllCommand;

    @BeforeEach
    public void setUp() {
        showInfoAllCommand = new ShowInfoAllCommand(materialService);
    }

    @Test
    public void testShowAllCommandGetCommandName() {
        assertEquals("showinfoall", showInfoAllCommand.getCommandName());
    }

    @Test
    public void testShowAllCommandGetCommandDescription() {
        assertEquals("show info all command", showInfoAllCommand.getCommandDescription());
    }

    @Test
    public void testShowAllCommandExecuteCommand() {
        Guild guild = mock(Guild.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn("835350969038012468");
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        showInfoAllCommand.executeCommand(message, new ArrayList<>());

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(any(MessageEmbed.class));
    }

    @Test
    public void testShowAllCommandGetCommandCategory() {
        assertEquals("Study Material Storage", showInfoAllCommand.getCommandCategory());
    }

    @Test
    public void testShowAllCommandIsGuildOnly() {
        assertTrue(showInfoAllCommand.isGuildOnly());
    }
}
