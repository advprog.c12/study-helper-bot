package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GroupingFileCommandTest {
    @Mock
    private FetcherServiceImpl fetcherService;

    private Class<?> groupingFileCommandClass;

    private GroupingFileCommand instance;

    @BeforeEach
    public void setUp() throws Exception {
        groupingFileCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands."
                + "randomizer.commands.GroupingFileCommand");
        instance = new GroupingFileCommand(fetcherService);
    }

    @Test
    public void testRegisterCommandIsAPublicClass() {
        int classModifiers = groupingFileCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testRegisterCommandOnGetCommandName() throws NoSuchMethodException {
        Method method = groupingFileCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("groupingfile", commandName);

    }

    @Test
    public void testRegisterCommandOnGetCommandDescription() throws NoSuchMethodException {
        Method method = groupingFileCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("Grouping the list of members as you need and write the result to a file", commandName);
    }

    @Test
    public void testRegisterCommandOnGetCommandUsage() throws NoSuchMethodException {
        Method method = groupingFileCommandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("<group/groupby> <jumlah_group/jumlah_per_group> "
                + "<file_name_for_output. e.g : text_3>", commandName);

    }

    @Test
    public void testRegisterCommandOnIsArgsRequired() {
        assertTrue(instance.isArgsRequired());

    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = groupingFileCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Randomizer", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = groupingFileCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");
        args.add("2");
        args.add("test");

        String jsonString = String.format("{\n"
                + "    \"result\": \"Group 1 \\na\\n\\nGroup 2\\nb\\n\\n\""
                + "\n}");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());

        File file = new File("test.txt");
        file.delete();


    }

    @Test
    public void testExecuteCommandContainsPlease() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");
        args.add("2");
        args.add("test");

        String jsonString = String.format("{\n"
                + "    \"result\": \"Please \\na\\n\\nGroup 2\\nb\\n\\n\""
                + "\n}");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());

    }

    @Test
    public void testExecuteCommandArgsNotComplete() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");

        String response = "The proper usage would be: --groupingfile <group/groupby> "
                + "<jumlah_group/jumlah_per_group> "
                + "<file_path_and_name_for_output. e.g : text_3>";

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(response);

    }

    @Test
    public void testExecuteCommandException() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");
        args.add("2");
        args.add("test");

        doThrow(IllegalStateException.class).when(fetcherService).sendGetRequest(anyString());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());

    }

    @Test
    public void testCreateFile() {
        File file = new File("filename.txt");
        assertTrue(instance.createFile("filename.txt"));
        file.exists();
        assertFalse(instance.createFile("filename.txt"));
        file.delete();
    }

    @Test
    public void testWriteToFile() throws IOException {
        File file = new File("filename.txt");
        String a = "a";
        String filename = "filename.txt";
        assertTrue(instance.writeToFile(filename,a));
        file.delete();
    }

    @Test
    public void testExecuteCommandFileError() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");
        args.add("2");
        args.add("test");

        String jsonString = String.format("{\n"
                + "    \"result\": \"Please \\na\\n\\nGroup 2\\nb\\n\\n\""
                + "\n}");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());

    }
}
