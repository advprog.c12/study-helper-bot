package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DeletePermissionCommandTest {
    @Mock
    private PermitService permitService;

    @InjectMocks
    private DeletePermissionCommand deletePermissionCommand;

    private String prefix = "!";

    private String serverId = "835350969038012468";

    @BeforeEach
    public void setUp() {
        deletePermissionCommand = new DeletePermissionCommand(permitService, prefix);
    }

    @Test
    public void testDeletePermissionCommandGetCommandName() {
        assertEquals("deletepermission", deletePermissionCommand.getCommandName());
    }

    @Test
    public void testDeletePermissionCommandGetCommandDescription() {
        assertEquals("delete permission command", deletePermissionCommand.getCommandDescription());
    }

    @Test
    public void testDeletePermissionCommandExecuteCommandIsOwner() {
        List<String> args = new ArrayList<>(Arrays.asList("MANAGE_SERVER"));

        User user = mock(User.class);
        Guild guild = mock(Guild.class);
        Member member = mock(Member.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn(serverId);
        when(message.getAuthor()).thenReturn(user);
        when(guild.getMember(user)).thenReturn(member);
        when(member.isOwner()).thenReturn(true);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        deletePermissionCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testDeletePermissionCommandExecuteCommandIsNotOwner() {
        List<String> args = new ArrayList<>(Arrays.asList("MANAGE_SERVER"));

        User user = mock(User.class);
        Guild guild = mock(Guild.class);
        Member member = mock(Member.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn(serverId);
        when(message.getAuthor()).thenReturn(user);
        when(guild.getMember(user)).thenReturn(member);
        when(member.isOwner()).thenReturn(false);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        deletePermissionCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testDeletePermissionCommandIsArgsRequired() {
        assertTrue(deletePermissionCommand.isArgsRequired());
    }

    @Test
    public void testDeletePermissionCommandGetCommandUsage() {
        assertEquals("<PERMISSION_NAME>", deletePermissionCommand.getCommandUsage());
    }

    @Test
    public void testDeletePermissionCommandGetCommandCategory() {
        assertEquals("Study Material Storage", deletePermissionCommand.getCommandCategory());
    }

    @Test
    public void testDeletePermissionCommandIsGuildOnly() {
        assertTrue(deletePermissionCommand.isGuildOnly());
    }
}
