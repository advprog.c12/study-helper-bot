package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UpdateremindExecutionTest {
    @Mock
    private SchoolworksService schoolworksService;

    @InjectMocks
    private UpdateremindExecution updateremindExecution;

    private Class<?> updateremindExecutionClass;

    @BeforeEach
    public void setUp() throws Exception {
        updateremindExecutionClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.executions.UpdateremindExecution");
        updateremindExecution = new UpdateremindExecution(schoolworksService);
    }

    @Test
    public void testUpdateremindExecutionIsConcreteClass() {
        assertFalse(Modifier.isAbstract(updateremindExecutionClass.getModifiers()));
    }

    @Test
    public void testUpdateremindExecutionIsAnExecution() {
        Collection<Type> interfaces = Arrays.asList(updateremindExecutionClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution"))
        );
    }

    @Test
    public void testUpdateremindExecutionOverridesExecute() throws Exception {
        Class<?>[] executeArgs = new Class[2];
        executeArgs[0] = List.class;
        executeArgs[1] = Message.class;
        Method execute = updateremindExecutionClass.getDeclaredMethod("execute", executeArgs);
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(2, execute.getParameterCount());
        assertTrue(Modifier.isPublic(execute.getModifiers()));
    }

    @Test
    public void testUpdateremindExecutionExecute() {
        UpdateremindExecution execution = new UpdateremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES1", "Mario");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        execution.execute(args, message);

        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(args.get(0), author.getName());
        verify(schoolworksService, times(1)).updateSchoolworkByNameAndAuthorName(args.get(0), author.getName(), schoolwork);
    }

    @Test
    public void testUpdateremindExecutionExecuteInvalidCommandUsage(){
        UpdateremindExecution execution = new UpdateremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("tes"));

        Message message = mock(Message.class);
        assertEquals("Invalid command usage!", execution.execute(args, message));
    }

    @Test
    public void testUpdateremindExecutionExecuteIfNameAndAuthorDoesExist() {
        UpdateremindExecution execution = new UpdateremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES1", "Mario");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        assertEquals(String.format("Updated reminder for %s named **%s** with description:\"%s\"", author.getName(), args.get(0), args.get(1)), execution.execute(args, message));
    }

    @Test
    public void testUpdateremindExecutionExecuteIfNameAndAuthorDoesNotExist() {
        UpdateremindExecution execution = new UpdateremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES1", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES1", "Mario");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(false);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        assertEquals(String.format("Schoolwork with name **%s** does not exist!", args.get(0)), execution.execute(args, message));
    }
}