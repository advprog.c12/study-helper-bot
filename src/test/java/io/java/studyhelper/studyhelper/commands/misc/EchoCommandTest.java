package io.java.studyhelper.studyhelper.commands.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EchoCommandTest {

    private Class<?> echoCommandClass;
    private EchoCommand instance;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        echoCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.misc.EchoCommand");
        instance = new EchoCommand();
    }

    @Test
    public void testEchoCommandIsAPublicClass() {
        int classModifiers = echoCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = echoCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("echo", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = echoCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Basic echo command", commandName);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = echoCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Miscellaneous", commandName);
    }

    @Test
    public void testIsArgsRequiredMethod() throws NoSuchMethodException {
        Method method = echoCommandClass.getDeclaredMethod("isArgsRequired");
        int methodModifiers = method.getModifiers();

        boolean commandName = instance.isArgsRequired();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(true, commandName);
    }

    @Test
    public void testGetCommandUsageMethod() throws NoSuchMethodException {
        Method method = echoCommandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("<args>", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = echoCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteCommand() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("Hello");
        args.add("World!");

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }
}
