package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class DelremindExecutionTest {
    @Mock
    private SchoolworksService schoolworksService;

    @InjectMocks
    private DelremindExecution delremindExecution;

    private Class<?> delremindExecutionClass;

    @BeforeEach
    public void setUp() throws Exception {
        delremindExecutionClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.executions.RemindExecution");
        delremindExecution = new DelremindExecution(schoolworksService);
    }

    @Test
    public void testDelremindExecutionIsConcreteClass() {
        assertFalse(Modifier.isAbstract(delremindExecutionClass.getModifiers()));
    }

    @Test
    public void testDelremindExecutionIsAnExecution() {
        Collection<Type> interfaces = Arrays.asList(delremindExecutionClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution"))
        );
    }

    @Test
    public void testDelremindExecutionOverridesExecute() throws Exception {
        Class<?>[] executeArgs = new Class[2];
        executeArgs[0] = List.class;
        executeArgs[1] = Message.class;
        Method execute = delremindExecutionClass.getDeclaredMethod("execute", executeArgs);
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(2, execute.getParameterCount());
        assertTrue(Modifier.isPublic(execute.getModifiers()));
    }

    @Test
    public void testDelremindExecutionExecute() {
        DelremindExecution execution = new DelremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);

        execution.execute(args, message);

        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(args.get(0), author.getName());
        verify(schoolworksService, times(1)).deleteSchoolworkByNameAndAuthorName(args.get(0), author.getName());
    }

    @Test
    public void testDelremindExecutionExecuteInvalidCommandUsage(){
        DelremindExecution execution = new DelremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("tes", "tes"));

        Message message = mock(Message.class);
        assertEquals("Invalid command usage!", execution.execute(args, message));
    }

    @Test
    public void testDelremindExecutionExecuteIfNameAndAuthorDoesNotExist(){
        DelremindExecution execution = new DelremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("tes"));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(false);

        assertEquals(String.format("Schoolwork with name **%s** does not exist!", args.get(0)), execution.execute(args, message));
    }

    @Test
    public void testDelremindExecutionExecuteIfNameAndAuthorDoesExist(){
        DelremindExecution execution = new DelremindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("tes"));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);

        assertEquals(String.format("Successfully deleted schoolwork with name **%s**", args.get(0)), execution.execute(args, message));
    }
}