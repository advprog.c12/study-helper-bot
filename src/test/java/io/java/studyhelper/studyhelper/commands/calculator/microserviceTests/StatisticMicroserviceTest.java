package io.java.studyhelper.studyhelper.commands.calculator.microserviceTests;

import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StatisticMicroserviceTest {

    private FetcherService fetcher;

    @BeforeEach
    public void setup() throws Exception {
        this.fetcher = new FetcherServiceImpl();
    }

    @Test
    public void testMicroserviceWorksForMeanCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/statistics/mean/?values=2,3,2,3,2");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 2", reply);
    }

    @Test
    public void testMicroserviceWorksForModeCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/statistics/mode/?values=2,3,2,3,2");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 2", reply);
    }

    @Test
    public void testMicroserviceWorksForMedianCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/statistics/median/?values=2,3,2,3,2");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 2", reply);
    }
}
