package io.java.studyhelper.studyhelper.commands.info;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.repository.CommandRepository;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HelpCommandTest {

    @Mock
    private CommandRepository commandRepository;

    private Class<?> helpCommandClass;
    private HelpCommand instance;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        helpCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.info.HelpCommand");
        instance = new HelpCommand(commandRepository, "--");
    }

    @Test
    public void testHelpCommandIsAPublicClass() {
        int classModifiers = helpCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = helpCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("help", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = helpCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Lists all of my commands or get info about a specific command", commandDescription);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = helpCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Information", commandDescription);
    }

    @Test
    public void testGetCommandUsageMethod() throws NoSuchMethodException {
        Method method = helpCommandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("<none or command name>", commandDescription);
    }

    @Test
    public void testGetCommandAliasesMethod() throws NoSuchMethodException {
        Method method = helpCommandClass.getDeclaredMethod("getCommandAliases");
        int methodModifiers = method.getModifiers();

        String[] commandDescription = instance.getCommandAliases();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("h", commandDescription[0]);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = helpCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testDefaultHelp() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        Command command = mock(Command.class);

        List<String> args = new ArrayList<>();

        when(commandRepository.getAllCommands()).thenReturn(Arrays.asList(new Command[] {command}));
        when(command.getCommandCategory()).thenReturn("My Category");
        when(command.getCommandName()).thenReturn("myCommand");
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(commandRepository, times(1)).getAllCommands();
        verify(command, times(1)).getCommandCategory();
        verify(command, times(1)).getCommandName();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(any(MessageEmbed.class));
    }

    @Test
    public void testSpecificHelp() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        Command command = mock(Command.class);

        List<String> args = new ArrayList<>();
        args.add("myCommand");

        when(commandRepository.getCommandByName(anyString())).thenReturn(command);
        when(command.getCommandName()).thenReturn("myCommand");
        when(command.getCommandDescription()).thenReturn("This is the description");
        when(command.getCommandCategory()).thenReturn("My Category");
        when(command.getCommandUsage()).thenReturn("<usage>");
        when(command.getCommandAliases()).thenReturn(new String[] {"myCmd"});
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(1)).getCommandName();
        verify(command, times(1)).getCommandDescription();
        verify(command, times(1)).getCommandCategory();
        verify(command, times(1)).getCommandUsage();
        verify(command, times(1)).getCommandAliases();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(any(MessageEmbed.class));
    }

    @Test
    public void testSpecificHelpNoCommandUsageNoAlias() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        Command command = mock(Command.class);

        List<String> args = new ArrayList<>();
        args.add("myCommand");

        when(commandRepository.getCommandByName(anyString())).thenReturn(command);
        when(command.getCommandName()).thenReturn("myCommand");
        when(command.getCommandDescription()).thenReturn("This is the description");
        when(command.getCommandCategory()).thenReturn("My Category");
        when(command.getCommandUsage()).thenReturn("");
        when(command.getCommandAliases()).thenReturn(new String[0]);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(commandRepository, times(1)).getCommandByName(anyString());
        verify(command, times(1)).getCommandName();
        verify(command, times(1)).getCommandDescription();
        verify(command, times(1)).getCommandCategory();
        verify(command, times(1)).getCommandUsage();
        verify(command, times(1)).getCommandAliases();
        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(any(MessageEmbed.class));
    }
}
