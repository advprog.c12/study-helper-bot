package io.java.studyhelper.studyhelper.commands.archive.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.model.UserEntity;
import io.java.studyhelper.studyhelper.commands.archive.model.UserMessage;
import io.java.studyhelper.studyhelper.commands.archive.repository.ArchivingStatusRepository;
import io.java.studyhelper.studyhelper.commands.archive.repository.UserEntityRepository;
import io.java.studyhelper.studyhelper.commands.archive.repository.UserMessagesRepository;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ArchiveServiceImplTest {

    @Mock
    private UserEntityRepository userEntityRepository;

    @Mock
    private ArchivingStatusRepository archivingStatusRepository;

    @Mock
    private UserMessagesRepository userMessagesRepository;

    @InjectMocks
    private ArchiveServiceImpl archiveService;

    private ArchivingStatus archivingStatus;
    private UserEntity userEntity;
    private UserMessage userMessage;
    private User mentionedUser;
    private User messageAuthor;
    private Guild guild;
    private Message message;

    @BeforeEach
    public void setUp() {
        Guild guild = mock(Guild.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        User mentionedUser = mock(User.class);
        User messageAuthor = mock(User.class);
        Message message = mock(Message.class);

        when(guild.getId()).thenReturn("GUILD-01");
        when(guild.getName()).thenReturn("My Discord Server");

        when(messageChannel.getId()).thenReturn("CHANNEL-01");
        when(messageChannel.getName()).thenReturn("my-channel");

        when(mentionedUser.getId()).thenReturn("USER-01");

        when(messageAuthor.getName()).thenReturn("Author");
        when(messageAuthor.getDiscriminator()).thenReturn("1234");

        when(message.getId()).thenReturn("MESSAGE-01");
        when(message.getGuild()).thenReturn(guild);
        when(message.getChannel()).thenReturn(messageChannel);
        when(message.getAuthor()).thenReturn(messageAuthor);
        when(message.getContentRaw()).thenReturn("This is a message");

        this.mentionedUser = mentionedUser;
        this.messageAuthor = messageAuthor;
        this.guild = guild;
        this.message = message;

        this.archivingStatus = new ArchivingStatus(mentionedUser.getId(), guild);
        this.userEntity = new UserEntity(mentionedUser.getId());
        this.userMessage = new UserMessage(mentionedUser.getId(), message);

        this.userEntity.setStatuses(new ArrayList<>());
        this.userEntity.setMessages(new ArrayList<>());
    }

    @Test
    public void testUserWantsArchive() {
        when(userEntityRepository.findByUserId(anyString())).thenReturn(null, userEntity);
        when(archivingStatusRepository.findByUserIdAndGuildId(anyString(), anyString()))
                .thenReturn(null, archivingStatus);

        boolean archivingStatus = archiveService.userWantsArchive(mentionedUser, guild);

        assertFalse(archivingStatus);

        verify(userEntityRepository, times(2)).findByUserId(anyString());
        verify(archivingStatusRepository, times(2))
                .findByUserIdAndGuildId(anyString(), anyString());
    }

    @Test
    public void testToggleArchivingStatus() {
        when(userEntityRepository.findByUserId(anyString())).thenReturn(null, userEntity);
        when(archivingStatusRepository.findByUserIdAndGuildId(anyString(), anyString()))
                .thenReturn(null, archivingStatus);

        boolean archivingStatusBefore = archiveService.userWantsArchive(mentionedUser, guild);
        archiveService.toggleArchivingStatus(mentionedUser, guild);
        boolean archivingStatusAfter = archiveService.userWantsArchive(mentionedUser, guild);
        archiveService.toggleArchivingStatus(mentionedUser, guild);
        boolean archivingStatusLast = archiveService.userWantsArchive(mentionedUser, guild);

        assertFalse(archivingStatusBefore);
        assertTrue(archivingStatusAfter);
        assertFalse(archivingStatusLast);

        verify(userEntityRepository, times(10)).findByUserId(anyString());
        verify(archivingStatusRepository, times(10))
                .findByUserIdAndGuildId(anyString(), anyString());
    }

    @Test
    public void testSaveMessageForUserIsNotArchiving() {
        when(userEntityRepository.findByUserId(anyString())).thenReturn(null, userEntity);
        when(archivingStatusRepository.findByUserIdAndGuildId(anyString(), anyString()))
                .thenReturn(null, archivingStatus);

        archiveService.saveMessageForUser(mentionedUser, message);

        int numOfUserMessage = userEntity.getMessages().size();

        assertEquals(0, numOfUserMessage);

        verify(userEntityRepository, times(2)).findByUserId(anyString());
        verify(archivingStatusRepository, times(2))
                .findByUserIdAndGuildId(anyString(), anyString());
    }

    @Test
    public void testSaveMessageForUserIsArchiving() {
        when(userEntityRepository.findByUserId(anyString())).thenReturn(null, userEntity);
        when(archivingStatusRepository.findByUserIdAndGuildId(anyString(), anyString()))
                .thenReturn(null, archivingStatus);

        archiveService.toggleArchivingStatus(mentionedUser, guild);
        archiveService.saveMessageForUser(mentionedUser, message);

        int numOfUserMessage = userEntity.getMessages().size();

        assertEquals(1, numOfUserMessage);

        verify(userEntityRepository, times(6)).findByUserId(anyString());
        verify(archivingStatusRepository, times(4))
                .findByUserIdAndGuildId(anyString(), anyString());
    }

    @Test
    public void testGetMessagesForUser() {
        when(userEntityRepository.findByUserId(anyString())).thenReturn(null, userEntity);
        when(archivingStatusRepository.findByUserIdAndGuildId(anyString(), anyString()))
                .thenReturn(null, archivingStatus);

        archiveService.toggleArchivingStatus(mentionedUser, guild);
        archiveService.saveMessageForUser(mentionedUser, message);

        int numOfUserMessageBefore = userEntity.getMessages().size();
        List<UserMessage> userMessageList = archiveService.getMessagesForUser(mentionedUser);
        int numOfUserMessageAfter = userEntity.getMessages().size();
        int numOfUserMessageList = userMessageList.size();
        UserMessage savedMessage = userMessageList.get(0);

        assertEquals(1, numOfUserMessageBefore);
        assertEquals(0, numOfUserMessageAfter);
        assertEquals(1, numOfUserMessageList);
        assertEquals("MESSAGE-01", savedMessage.getMessageId());
        assertEquals("My Discord Server", savedMessage.getMessageGuild());
        assertEquals("my-channel", savedMessage.getMessageChannel());
        assertEquals("Author", savedMessage.getMessageAuthorName());
        assertEquals("1234", savedMessage.getMessageAuthorDiscriminator());
        assertEquals("This is a message", savedMessage.getMessageContent());
        assertEquals("https://discord.com/channels/GUILD-01/CHANNEL-01/MESSAGE-01",
                savedMessage.getMessageLink());

        verify(userEntityRepository, times(8)).findByUserId(anyString());
        verify(archivingStatusRepository, times(4))
                .findByUserIdAndGuildId(anyString(), anyString());
    }

    @Test
    public void testGetArchivingStatusesForUser() {
        when(userEntityRepository.findByUserId(anyString())).thenReturn(null, userEntity);
        when(archivingStatusRepository.findByUserIdAndGuildId(anyString(), anyString()))
                .thenReturn(null, archivingStatus);

        int numOfArchivingGuildsBefore = userEntity.getStatuses().size();
        archiveService.toggleArchivingStatus(mentionedUser, guild);
        int numOfArchivingGuildsAfter = userEntity.getStatuses().size();
        List<ArchivingStatus> archivingStatusList = archiveService.getArchivingStatusesForUser(mentionedUser);
        ArchivingStatus archivingStatus = archivingStatusList.get(0);
        int numOfArchivingGuildsList = archivingStatusList.size();

        assertEquals(0, numOfArchivingGuildsBefore);
        assertEquals(1, numOfArchivingGuildsAfter);
        assertEquals(1, numOfArchivingGuildsList);

        verify(userEntityRepository, times(4)).findByUserId(anyString());
        verify(archivingStatusRepository, times(2))
                .findByUserIdAndGuildId(anyString(), anyString());
    }
}
