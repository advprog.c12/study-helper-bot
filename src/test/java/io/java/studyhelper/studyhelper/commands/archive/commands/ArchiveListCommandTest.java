package io.java.studyhelper.studyhelper.commands.archive.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ArchiveListCommandTest {

    @Mock
    private ArchiveServiceImpl archiveService;

    private Class<?> archiveListCommand;
    private ArchiveListCommand instance;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        archiveListCommand = Class.forName(
                "io.java.studyhelper.studyhelper.commands.archive.commands.ArchiveListCommand");
        instance = new ArchiveListCommand(archiveService);
    }

    @Test
    public void tesArchiveListCommandIsAPublicClass() {
        int classModifiers = archiveListCommand.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = archiveListCommand.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("archiveList", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = archiveListCommand.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Lists all the servers that I'm archiving", commandDescription);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = archiveListCommand.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandCategory = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Chat Archive", commandCategory);
    }

    @Test
    public void testIsDMOnlyMethod() throws NoSuchMethodException {
        Method method = archiveListCommand.getDeclaredMethod("isDMOnly");
        int methodModifiers = method.getModifiers();

        boolean isDMOnly = instance.isDMOnly();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(true, isDMOnly);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = archiveListCommand.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteMessageNoActiveGuilds() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getArchivingStatusesForUser(author)).thenReturn(new ArrayList<>());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getArchivingStatusesForUser(author);
    }

    @Test
    public void testExecuteMessageWithActiveGuilds() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        ArchivingStatus archivingStatus = mock(ArchivingStatus.class);

        when(archivingStatus.isArchiving()).thenReturn(true);
        when(archivingStatus.getGuildName()).thenReturn("My Server");

        List<ArchivingStatus> archivingStatuses = new ArrayList<>();
        archivingStatuses.add(archivingStatus);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getArchivingStatusesForUser(author)).thenReturn(archivingStatuses);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getArchivingStatusesForUser(author);
        verify(archivingStatus, times(1)).isArchiving();
        verify(archivingStatus, times(1)).getGuildName();
    }
}
