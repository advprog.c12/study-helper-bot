package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RegisterTest {
    private Class<?> registerCommandClass;

    @Mock
    private FetcherServiceImpl fetcherService;


    private RegisterCommand registerCommand;

    @BeforeEach
    public void setUp() throws Exception {
        registerCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands."
                + "randomizer.commands.RegisterCommand");
        registerCommand = new RegisterCommand(fetcherService);
    }

    @Test
    public void testRegisterCommandIsAPublicClass() {
        int classModifiers = registerCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(registerCommand instanceof Command);
    }

    @Test
    public void testRegisterCommandOnGetCommandName() throws NoSuchMethodException {
        Method method = registerCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = registerCommand.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("register", commandName);

    }

    @Test
    public void testRegisterCommandOnGetCommandDescription() throws NoSuchMethodException {
        Method method = registerCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandName = registerCommand.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("Register list of members", commandName);
    }

    @Test
    public void testRegisterCommandOnGetCommandUsage() throws NoSuchMethodException {
        Method method = registerCommandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandName = registerCommand.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("<list_of_first_names_with_enter_to_separate_each_names>", commandName);

    }

    @Test
    public void testRegisterCommandOnIsArgsRequired() {
        assertTrue(registerCommand.isArgsRequired());

    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = registerCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = registerCommand.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Randomizer", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = registerCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRegisterCommandOnExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("a");
        args.add("b");

        String jsonString = String.format("{\n"
                + "    \"result\": \"Succesfully add list of anggota " + "\\n" + " List of anggota : "
                + "\\n " + "a\\n" + "b\\n\""
                + "\n}");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        registerCommand.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }

    @Test
    public void testRegisterCommandExceptionOnExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("a");
        args.add("b");

        doThrow(IllegalStateException.class).when(fetcherService).sendGetRequest(anyString());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        registerCommand.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }
}
