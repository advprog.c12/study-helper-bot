package io.java.studyhelper.studyhelper.commands.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class APITestCommandTest {

    @Mock
    private FetcherServiceImpl fetcherService;

    private Class<?> apiTestCommandClass;
    private APITestCommand instance;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        apiTestCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.misc.APITestCommand");
        instance = new APITestCommand(fetcherService);
    }

    @Test
    public void testEchoCommandIsAPublicClass() {
        int classModifiers = apiTestCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = apiTestCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("api", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = apiTestCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("API testing", commandName);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = apiTestCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Miscellaneous", commandName);
    }

    @Test
    public void testIsArgsRequiredMethod() throws NoSuchMethodException {
        Method method = apiTestCommandClass.getDeclaredMethod("isArgsRequired");
        int methodModifiers = method.getModifiers();

        boolean isRequired = instance.isArgsRequired();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(isRequired);
    }

    @Test
    public void testGetCommandUsageMethod() throws NoSuchMethodException {
        Method method = apiTestCommandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("<get id> or <post id data>", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = apiTestCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGet() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("get");
        args.add("ID-00");

        String jsonString = String.format("{\n"
                + "    \"sampleId\": \"%s\",\n"
                + "    \"dataString\": \"%s\"\n"
                + "}", "ID-00", "Data 00");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }

    @Test
    public void testGetException() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("get");
        args.add("ID-00");

        doThrow(IllegalStateException.class).when(fetcherService).sendGetRequest(anyString());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }

    @Test
    public void testPost() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("post");
        args.add("ID-00");
        args.add("Data 00");

        String jsonString = String.format("{\n"
                + "    \"sampleId\": \"%s\",\n"
                + "    \"dataString\": \"%s\"\n"
                + "}", "ID-00", "Data 00");

        when(fetcherService.sendPostRequest(anyString(), anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(fetcherService, times(1)).sendPostRequest(anyString(), anyString());
    }

    @Test
    public void testPostException() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("post");
        args.add("ID-00");
        args.add("Data 00");

        doThrow(IllegalStateException.class).when(fetcherService).sendPostRequest(anyString(), anyString());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(fetcherService, times(1)).sendPostRequest(anyString(), anyString());
    }
}
