package io.java.studyhelper.studyhelper.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommandTest {

    private Class<?> commandClass;
    private Command instance;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        commandClass = Class.forName("io.java.studyhelper.studyhelper.commands.Command");
        instance = spy(Command.class);
    }

    @Test
    public void testCommandIsAPublicInterface() {
        int classModifiers = commandClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testHasAbstractGetCommandNameMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testHasAbstractGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testHasAbstractExecuteCommandMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandCategory = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Other", commandCategory);
    }

    @Test
    public void testIsArgsRequiredMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("isArgsRequired");
        int methodModifiers = method.getModifiers();

        boolean isArgsRequired = instance.isArgsRequired();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(false, isArgsRequired);
    }

    @Test
    public void testGetCommandUsageMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandUsage = instance.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("", commandUsage);
    }

    @Test
    public void testIsGuildOnlyMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("isGuildOnly");
        int methodModifiers = method.getModifiers();

        boolean isGuildOnly = instance.isGuildOnly();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(false, isGuildOnly);
    }

    @Test
    public void testIsDMOnlyMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("isDMOnly");
        int methodModifiers = method.getModifiers();

        boolean isDMOnly = instance.isDMOnly();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(false, isDMOnly);
    }

    @Test
    public void testGetCommandAliasesMethod() throws NoSuchMethodException {
        Method method = commandClass.getDeclaredMethod("getCommandAliases");
        int methodModifiers = method.getModifiers();

        String[] commandAliases = instance.getCommandAliases();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(commandAliases.length == 0);
    }
}
