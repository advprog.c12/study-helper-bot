package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksServiceImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class CheckremindersCommandTest {
    @Mock
    private SchoolworksServiceImpl schoolworksService;

    @InjectMocks
    private CheckremindersCommand checkremindersCommand;

    private Class<?> checkremindersCommandClass;

    @BeforeEach
    public void setUp() throws Exception {
        checkremindersCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.commands.CheckremindersCommand");
        checkremindersCommand = new CheckremindersCommand(schoolworksService);
    }

    @Test
    public void testCheckremindersCommandIsConcreteClass(){
        assertFalse(Modifier.isAbstract(checkremindersCommandClass.getModifiers()));
    }

    @Test
    public void testCheckremindersCommandIsASchoolworksCommand(){
        Collection<Type> interfaces = Arrays.asList(checkremindersCommandClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.commands.SchoolworksCommand"))
        );
    }

    @Test
    public void testCheckremindersCommandOverrideGetCommandName() throws Exception {
        Class<?>[] getCommandNameArgs = new Class[0];
        Method getCommandName = checkremindersCommandClass.getDeclaredMethod("getCommandName", getCommandNameArgs);
        assertEquals("java.lang.String", getCommandName.getGenericReturnType().getTypeName());
        assertEquals(0, getCommandName.getParameterCount());
        assertTrue(Modifier.isPublic(getCommandName.getModifiers()));
    }
    @Test
    public void testCheckremindersCommandOverrideGetCommandDescription() throws Exception {
        Class<?>[] getCommandDescriptionArgs = new Class[0];
        Method getCommandDescription = checkremindersCommandClass.getDeclaredMethod("getCommandDescription", getCommandDescriptionArgs);
        assertEquals("java.lang.String", getCommandDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getCommandDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getCommandDescription.getModifiers()));
    }

    @Test
    public void testCheckremindersCommandOverrideExecuteCommand() throws Exception {
        Class<?>[] executeCommandArgs = new Class[2];
        executeCommandArgs[0] = Message.class;
        executeCommandArgs[1] = List.class;
        Method executeCommand = checkremindersCommandClass.getDeclaredMethod("executeCommand", executeCommandArgs);
        assertEquals("void", executeCommand.getGenericReturnType().getTypeName());
        assertEquals(2, executeCommand.getParameterCount());
        assertTrue(Modifier.isPublic(executeCommand.getModifiers()));
    }

    @Test
    public void testCheckremindersCommandOverridesGetExecution() throws Exception {
        Class<?>[] getExecutionArgs = new Class[0];
        Method getExecution = checkremindersCommandClass.getDeclaredMethod("getExecution", getExecutionArgs);
        assertEquals(Execution.class, getExecution.getGenericReturnType());
        assertEquals(0, getExecution.getParameterCount());
        assertTrue(Modifier.isPublic(getExecution.getModifiers()));
    }

    @Test
    public void testGetCommandNameWorksAsExpected() {
        assertEquals("checkreminders", checkremindersCommand.getCommandName());
    }

    @Test
    public void testGetCommandDescriptionWorksAsExpected() {
        assertEquals("Get all reminders that is created by that user", checkremindersCommand.getCommandDescription());
    }

    @Test
    public void testGetCommandCategoryWorksAsExpected() {
        assertEquals("Schoolwork Reminder", checkremindersCommand.getCommandCategory());
    }

    @Test
    public void testGetExecutionWorksAsExpected() {
        assertNotNull(checkremindersCommand.getExecution());
    }

    @Test
    public void testCheckremindersCommandExecuteCommand(){
        CheckremindersCommand command = new CheckremindersCommand(this.schoolworksService);
        List<String> args = new ArrayList<>();

        User author = mock(User.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);
        when(message.getChannel()).thenReturn(messageChannel);
        when(message.getAuthor()).thenReturn(author);

        command.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        assertNotNull(command.getExecution().execute(args, message));
    }
}
