package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
 public class RemindExecutionTest {
    @Mock
    private SchoolworksService schoolworksService;

    @InjectMocks
    private RemindExecution remindExecution;

    private Class<?> remindExecutionClass;

    @BeforeEach
    public void setUp() throws Exception {
        remindExecutionClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.executions.RemindExecution");
        remindExecution = new RemindExecution(schoolworksService);
    }

    @Test
    public void testRemindExecutionIsConcreteClass(){
        assertFalse(Modifier.isAbstract(remindExecutionClass.getModifiers()));
    }

    @Test
    public void testRemindExecutionIsAnExecution(){
        Collection<Type> interfaces = Arrays.asList(remindExecutionClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution"))
        );
    }

    @Test
    public void testRemindExecutionOverridesExecute() throws Exception {
        Class<?>[] executeArgs = new Class[2];
        executeArgs[0] = List.class;
        executeArgs[1] = Message.class;
        Method execute = remindExecutionClass.getDeclaredMethod("execute", executeArgs);
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(2, execute.getParameterCount());
        assertTrue(Modifier.isPublic(execute.getModifiers()));
    }

    @Test
    public void testRemindExecutionExecute(){
        RemindExecution execution = new RemindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(message.getAuthor()).thenReturn(author);

        execution.execute(args, message);

        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(args.get(0), author.getName());
        verify(schoolworksService, times(1)).createSchoolwork(args.get(0), args.get(1), author.getName());
    }

    @Test
    public void testRemindExecutionExecuteInvalidCommandUsage(){
        RemindExecution execution = new RemindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES"));

        Message message = mock(Message.class);

        assertEquals("Invalid command usage!", execution.execute(args, message));
    }

    @Test
    public void testRemindExecutionExecuteIfNameAndAuthorDoesNotExist(){
        RemindExecution execution = new RemindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);

        assertEquals(String.format("Schoolwork with name **%s** already exists, use --updateremind to update schoolwork description!", args.get(0)), execution.execute(args, message));
    }

    @Test
    public void testRemindExecutionExecuteIfNameAndAuthorDoesExist(){
        RemindExecution execution = new RemindExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(false);

        assertEquals(String.format("Added new reminder for **%s** named **%s** with description: \"%s\"", author.getName(), args.get(0), args.get(1)), execution.execute(args, message));
    }
}
