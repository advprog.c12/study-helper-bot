package io.java.studyhelper.studyhelper.commands.calculator.microserviceTests;

import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class ArithmeticsMicroserviceTest {

    private FetcherService fetcher;

    @BeforeEach
    public void setup() throws Exception {
        this.fetcher = new FetcherServiceImpl();
    }

    @Test
    public void testMicroserviceWorksForAdditionCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/arithmetic/add/4/3");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 7", reply);
    }

    @Test
    public void testMicroserviceWorksForSubtractionCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/arithmetic/sub/4/3");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 1", reply);
    }

    @Test
    public void testMicroserviceWorksForMultiplicationCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/arithmetic/mul/4/3");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 12", reply);
    }

    @Test
    public void testMicroserviceWorksForDivisionCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/arithmetic/div/4/3");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 1", reply);
    }

    @Test
    public void testMicroserviceWorksForModuloCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/arithmetic/mod/4/3");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 1", reply);
    }

}
