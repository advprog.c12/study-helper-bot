package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksServiceImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UpdateremindCommandTest {
    @Mock
    private SchoolworksServiceImpl schoolworksService;

    @InjectMocks
    private UpdateremindCommand updateremindCommand;

    private Class<?> updateremindCommandClass;

    @BeforeTestMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @BeforeEach
    public void setUp() throws Exception {
        updateremindCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.commands.UpdateremindCommand");
        updateremindCommand = new UpdateremindCommand(schoolworksService);
    }

    @Test
    public void testUpdateremindCommandIsConcreteClass(){
        assertFalse(Modifier.isAbstract(updateremindCommandClass.getModifiers()));
    }

    @Test
    public void testUpdateremindCommandIsASchoolworksCommand(){
        Collection<Type> interfaces = Arrays.asList(updateremindCommandClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.commands.SchoolworksCommand"))
        );
    }

    @Test
    public void testUpdateremindCommandOverrideGetCommandName() throws Exception {
        Class<?>[] getCommandNameArgs = new Class[0];
        Method getCommandName = updateremindCommandClass.getDeclaredMethod("getCommandName", getCommandNameArgs);
        assertEquals("java.lang.String", getCommandName.getGenericReturnType().getTypeName());
        assertEquals(0, getCommandName.getParameterCount());
        assertTrue(Modifier.isPublic(getCommandName.getModifiers()));
    }

    @Test
    public void testUpdateremindCommandOverrideGetCommandDescription() throws Exception {
        Class<?>[] getCommandDescriptionArgs = new Class[0];
        Method getCommandDescription = updateremindCommandClass.getDeclaredMethod("getCommandDescription", getCommandDescriptionArgs);
        assertEquals("java.lang.String", getCommandDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getCommandDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getCommandDescription.getModifiers()));
    }

    @Test
    public void testUpdateremindCommandOverrideExecuteCommand() throws Exception {
        Class<?>[] executeCommandArgs = new Class[2];
        executeCommandArgs[0] = Message.class;
        executeCommandArgs[1] = List.class;
        Method executeCommand = updateremindCommandClass.getDeclaredMethod("executeCommand", executeCommandArgs);
        assertEquals("void", executeCommand.getGenericReturnType().getTypeName());
        assertEquals(2, executeCommand.getParameterCount());
        assertTrue(Modifier.isPublic(executeCommand.getModifiers()));
    }

    @Test
    public void testUpdateremindCommandOverridesGetExecution() throws Exception {
        Class<?>[] getExecutionArgs = new Class[0];
        Method getExecution = updateremindCommandClass.getDeclaredMethod("getExecution", getExecutionArgs);
        assertEquals(Execution.class, getExecution.getGenericReturnType());
        assertEquals(0, getExecution.getParameterCount());
        assertTrue(Modifier.isPublic(getExecution.getModifiers()));
    }

    @Test
    public void testGetCommandNameWorksAsExpected() {
        assertEquals("updateremind", updateremindCommand.getCommandName());
    }

    @Test
    public void testGetCommandDescriptionWorksAsExpected() {
        assertEquals("Update command description from command name", updateremindCommand.getCommandDescription());
    }

    @Test
    public void testGetCommandCategoryWorksAsExpected() {
        assertEquals("Schoolwork Reminder", updateremindCommand.getCommandCategory());
    }

    @Test
    public void testGetExecutionWorksAsExpected() {
        assertNotNull(updateremindCommand.getExecution());
    }

    @Test
    public void testUpdateremindCommandExecuteCommand(){
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TESTING"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);
        when(message.getChannel()).thenReturn(messageChannel);
        when(message.getAuthor()).thenReturn(author);

        updateremindCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        assertNotNull(updateremindCommand.getExecution().execute(args, message));
    }
}
