package io.java.studyhelper.studyhelper.commands.studymaterial.verifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PermissionVerifierTest {
    @Mock
    private PermitService permitService;

    @InjectMocks
    private PermissionVerifier permissionVerifier;

    private List<Permit> permits;

    private List<String> permissionList;

    private String prefix = "!";

    private String serverId = "835350969038012468";

    @BeforeEach
    public void setUp() {
        permits = new ArrayList<>();

        permits.add(new Permit(serverId, "ADMINISTRATOR"));
        permits.add(new Permit(serverId, "BAN_MEMBERS"));
        permits.add(new Permit(serverId, "KICK_MEMBERS"));
        permits.add(new Permit(serverId, "MANAGE_CHANNEL"));
        permits.add(new Permit(serverId, "MANAGE_PERMISSIONS"));
        permits.add(new Permit(serverId, "MANAGE_ROLES"));
        permits.add(new Permit(serverId, "MANAGE_SERVER"));

        permissionList = new ArrayList<>();

        for (Permission permission : Permission.values()) {
            permissionList.add(permission.name());
        }

        when(permitService.getAllPermit(serverId)).thenReturn(permits);

        permissionVerifier = new PermissionVerifier(prefix, serverId, permitService);

    }

    @Test
    public void testPermissionVerifierAddPermissionSucceed() {
        permissionVerifier.addPermission("MANAGE_WEBHOOKS");

        Permit permit = mock(Permit.class);

        when(permit.getPermitValue()).thenReturn("MANAGE_WEBHOOKS");
        assertEquals("MANAGE_WEBHOOKS", permit.getPermitValue());
    }

    @Test
    public void testPermissionVerifierAddPermissionFail() {
        String res = permissionVerifier.addPermission("test");
        StringBuilder expected = new StringBuilder();
        expected.append("There's no such permission!\nPermissions that you can add:\n");

        for (String s : permissionList) {
            expected.append(String.format("- `%s`\n", s));
        }

        assertEquals(expected.toString(), res);
    }

    @Test
    public void testPermissionVerifierDeletePermission() {
        permissionVerifier.deletePermission("MANAGE_SERVER");
    }

    @Test
    public void testPermissionVerifierVerifyTrue() {
        Member member = mock(Member.class);
        when(member.hasPermission(Permission.ADMINISTRATOR)).thenReturn(true);
        assertTrue(permissionVerifier.verify(member));
    }

    @Test
    public void testPermissionVerifierVerifyFalse() {
        Member member = mock(Member.class);
        assertFalse(permissionVerifier.verify(member));
    }

    @Test
    public void testPermissionVerifierGetPermissionListToString() {
        String result = permissionVerifier.getPermissionListToString();
        StringBuilder expected = new StringBuilder();

        for (Permit permit : permitService.getAllPermit(serverId)) {
            expected.append(String.format("- `%s`\n", permit.getPermitValue()));
        }
        assertEquals(expected.toString(), result);
    }
}
