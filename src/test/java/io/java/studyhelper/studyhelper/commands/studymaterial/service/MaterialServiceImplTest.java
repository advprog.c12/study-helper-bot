package io.java.studyhelper.studyhelper.commands.studymaterial.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Material;
import io.java.studyhelper.studyhelper.commands.studymaterial.repository.MaterialRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MaterialServiceImplTest {
    @Mock
    private MaterialRepository materialRepository;

    @InjectMocks
    private MaterialServiceImpl materialService;

    private Material material;

    @BeforeEach
    public void setUp() {
        material = new Material();
        material.setServerId("835350969038012468");
        material.setCategory("Week_01");
        material.setMaterialValue("value1");
    }

    @Test
    public void testServiceCreateMaterialIfExist() {
        when(materialRepository.save(material)).thenReturn(material);
        when(materialRepository.findByCategoryAndServerId(
                "Week_01", "835350969038012468")).thenReturn(material);

        Material result = materialService.createMaterial(
                "835350969038012468", "Week_01", "value1");

        Material newResult = materialService.createMaterial(
                "835350969038012468", "Week_01", "value2");

        assertEquals(result.getCategory(), newResult.getCategory());
    }

    @Test
    public void testServiceCreateMaterialIfNotExist() {
        lenient().when(materialService.createMaterial(
                "835350969038012468", "Week_01", "value1")).thenReturn(material);
    }

    @Test
    public void testServiceGetAllMaterial() {
        List<Material> materials = new ArrayList<>();
        materials.add(material);
        when(materialRepository.findAllByServerId("835350969038012468")).thenReturn(materials);
        String res = materialService.getAllMaterial("835350969038012468");
        assertEquals("\n**1. Week_01**\n- value1\n", res);
    }

    @Test
    public void testServiceGetAllMaterialWithNoValue() {
        when(materialRepository.findAllByServerId("835350969038012468")).thenReturn(new ArrayList<>());
        String res = materialService.getAllMaterial("835350969038012468");
        assertEquals("The list is currently empty...", res);
    }

    @Test
    public void testServiceGetMaterialByCategoryWithOneValue() {
        when(materialRepository.findByCategoryAndServerId("Week_01", "835350969038012468")).thenReturn(material);
        String res = materialService.getMaterialValueByCategory("Week_01", "835350969038012468");
        assertEquals(res, material.getMaterialValue());
    }

    @Test
    public void testServiceGetMaterialByCategoryWithMoreThanOneValues() {
        material.setMaterialValue("value1\nvalue2");
        when(materialRepository.findByCategoryAndServerId("Week_01", "835350969038012468")).thenReturn(material);
        String res = materialService.getMaterialValueByCategory("Week_01", "835350969038012468");
        assertEquals("1. value1 \n2. value2 \n", res);
    }

    @Test
    public void testServiceDeleteMaterialByCategory() {
        when(materialRepository.findByCategoryAndServerId("Week_01", "835350969038012468"))
                .thenReturn(material);

        materialService.deleteMaterialByCategory("Week_01", "835350969038012468");
    }
}
