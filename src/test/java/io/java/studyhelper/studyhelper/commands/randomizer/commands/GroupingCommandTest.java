package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GroupingCommandTest {

    @Mock
    private FetcherServiceImpl fetcherService;

    private Class<?> groupingCommandClass;
    private String groupingC;
    private GroupingCommand groupingCommand;

    @BeforeEach
    public void setUp() throws Exception {
        groupingC = "io.java.studyhelper.studyhelper.commands.randomizer.commands.GroupingCommand";
        groupingCommandClass = Class.forName(groupingC);
        groupingCommand = new GroupingCommand(fetcherService);
    }

    @Test
    public void testGroupingCommandOnGetCommandName() {
        String a = groupingCommand.getCommandName();
        assertThat(a).isEqualTo("grouping");
    }

    @Test
    public void testGroupingCommandOnGetCommandDescription() {
        String a = groupingCommand.getCommandDescription();
        assertThat(a).isEqualTo("Grouping the list of members as you need and return the list to the channel");

    }

    @Test
    public void testGroupingCommandOnGetCommandUsage() {
        String a = groupingCommand.getCommandUsage();
        assertThat(a).isEqualTo("<group/groupby> <jumlah_group/jumlah_per_group>");

    }

    @Test
    public void testGroupingCommandOnIsArgsRequired() {
        assertTrue(groupingCommand.isArgsRequired());

    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = groupingCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = groupingCommand.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Randomizer", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = groupingCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGroupingCommandOnExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");
        args.add("2");

        String jsonString = String.format("{\n"
                + "    \"result\": \"Group 1 \\na\\n\\nGroup 2\\nb\\n\\n\""
                + "\n}");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        groupingCommand.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }

    @Test
    public void testGroupingCommandOnExecuteCommandArgsNotRight() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");

        String response = "The proper usage would be: --grouping <group/groupby> <jumlah_group/jumlah_per_group>";

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        groupingCommand.executeCommand(message,args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(response);

    }

    @Test
    public void testGroupingCommandExceptionOnExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("group");
        args.add("2");

        doThrow(IllegalStateException.class).when(fetcherService).sendGetRequest(anyString());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        groupingCommand.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }
}
