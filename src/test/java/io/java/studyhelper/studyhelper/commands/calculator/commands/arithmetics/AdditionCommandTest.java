package io.java.studyhelper.studyhelper.commands.calculator.commands.arithmetics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import io.java.studyhelper.studyhelper.commands.calculator.commands.AdditionCommand;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
public class AdditionCommandTest {

    private Class<?> CommandClass;
    private AdditionCommand instance;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        CommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.calculator.commands.AdditionCommand");
        instance = new AdditionCommand(new FetcherServiceImpl(), "--");
    }

    @Test
    public void testPingCommandIsAPublicClass() {
        int classModifiers = CommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = CommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("add", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = CommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Addition arithmetic calculator", commandName);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = CommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Calculator", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = CommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteCommand() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();
        args.add("1");
        args.add("2");

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }
}
