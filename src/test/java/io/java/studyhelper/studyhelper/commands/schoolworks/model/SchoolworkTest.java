package io.java.studyhelper.studyhelper.commands.schoolworks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SchoolworkTest {
    private Class<?> schoolworkClass;

    @BeforeEach
    public void setUp() throws Exception {
        schoolworkClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork");
    }

    @Test
    public void testNonBlankConstructor(){
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "TES");
        assertNotNull(schoolwork);
    }

    @Test
    public void testSchoolworkGetNameMethodExist() throws Exception {
        Class<?>[] getNameArgs = new Class[0];
        Method getName = schoolworkClass.getDeclaredMethod("getName", getNameArgs);
        assertEquals(String.class, getName.getGenericReturnType());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSchoolworkGetAuthorNameMethodExist() throws Exception {
        Class<?>[] getAuthorNameArgs = new Class[0];
        Method getAuthorName = schoolworkClass.getDeclaredMethod("getAuthorName", getAuthorNameArgs);
        assertEquals(String.class, getAuthorName.getGenericReturnType());
        assertEquals(0, getAuthorName.getParameterCount());
        assertTrue(Modifier.isPublic(getAuthorName.getModifiers()));
    }

    @Test
    public void testSchoolworkGetDescriptionMethodExist() throws Exception {
        Class<?>[] getDescriptionArgs = new Class[0];
        Method getDescription = schoolworkClass.getDeclaredMethod("getDescription", getDescriptionArgs);
        assertEquals(String.class, getDescription.getGenericReturnType());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSchoolworkSetNameMethodExist() throws Exception {
        Class<?>[] setNameArgs = new Class[1];
        setNameArgs[0] = String.class;
        Method setName = schoolworkClass.getDeclaredMethod("setName", setNameArgs);
        assertEquals(void.class, setName.getGenericReturnType());
        assertEquals(1, setName.getParameterCount());
        assertTrue(Modifier.isPublic(setName.getModifiers()));
    }

    @Test
    public void testSchoolworkSetAuthorNameMethodExist() throws Exception {
        Class<?>[] setAuthorNameArgs = new Class[1];
        setAuthorNameArgs[0] = String.class;
        Method setAuthorName = schoolworkClass.getDeclaredMethod("setAuthorName", setAuthorNameArgs);
        assertEquals(void.class, setAuthorName.getGenericReturnType());
        assertEquals(1, setAuthorName.getParameterCount());
        assertTrue(Modifier.isPublic(setAuthorName.getModifiers()));
    }

    @Test
    public void testSchoolworkSetDescriptionMethodExist() throws Exception {
        Class<?>[] setDescriptionArgs = new Class[1];
        setDescriptionArgs[0] = String.class;
        Method setDescription = schoolworkClass.getDeclaredMethod("setDescription", setDescriptionArgs);
        assertEquals(void.class, setDescription.getGenericReturnType());
        assertEquals(1, setDescription.getParameterCount());
        assertTrue(Modifier.isPublic(setDescription.getModifiers()));
    }

    @Test
    public void testSchoolworkGetNameOutputIsCorrect(){
        String EXPECTED_NAME = "GANTENG";
        Schoolwork schoolwork = new Schoolwork(EXPECTED_NAME, null, null);
        assertEquals(EXPECTED_NAME, schoolwork.getName());
    }

    @Test
    public void testSchoolworkGetAuthorNameOutputIsCorrect(){
        String EXPECTED_AUTHOR_NAME = "GANTENG";
        Schoolwork schoolwork = new Schoolwork(null, null, EXPECTED_AUTHOR_NAME);
        assertEquals(EXPECTED_AUTHOR_NAME, schoolwork.getAuthorName());
    }

    @Test
    public void testSchoolworkGetDescriptionOutputIsCorrect(){
        String EXPECTED_DESCRIPTION = "GANTENG";
        Schoolwork schoolwork = new Schoolwork(null, EXPECTED_DESCRIPTION, null);
        assertEquals(EXPECTED_DESCRIPTION, schoolwork.getDescription());
    }

    @Test
    public void testSetNameSetsProperly() throws Exception {
        String EXPECTED_NAME = "GANTENG";
        final Schoolwork schoolwork = new Schoolwork();

        schoolwork.setName(EXPECTED_NAME);
        assertEquals(EXPECTED_NAME, schoolwork.getName());
    }

    @Test
    public void testSetAuthorNameSetsProperly() throws Exception {
        String EXPECTED_AUTHOR_NAME = "GANTENG";
        final Schoolwork schoolwork = new Schoolwork();

        schoolwork.setAuthorName(EXPECTED_AUTHOR_NAME);
        assertEquals(EXPECTED_AUTHOR_NAME, schoolwork.getAuthorName());
    }

    @Test
    public void testSetDescriptionSetsProperly() throws Exception {
        String EXPECTED_DESCRIPTION = "GANTENG";
        final Schoolwork schoolwork = new Schoolwork();

        schoolwork.setDescription(EXPECTED_DESCRIPTION);
        assertEquals(EXPECTED_DESCRIPTION, schoolwork.getDescription());
    }
}
