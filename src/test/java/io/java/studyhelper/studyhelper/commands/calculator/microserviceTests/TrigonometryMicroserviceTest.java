package io.java.studyhelper.studyhelper.commands.calculator.microserviceTests;

import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrigonometryMicroserviceTest {

    private FetcherService fetcher;

    @BeforeEach
    public void setup() throws Exception {
        this.fetcher = new FetcherServiceImpl();
    }

    @Test
    public void testMicroserviceWorksForCosecantCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/csc/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: Undefined", reply);
    }

    @Test
    public void testMicroserviceWorksForCosinusCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/cos/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 1.0", reply);
    }

    @Test
    public void testMicroserviceWorksForCotangentCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/cot/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: Undefined", reply);
    }

    @Test
    public void testMicroserviceWorksForSecantCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/sec/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 1.0", reply);
    }

    @Test
    public void testMicroserviceWorksForSinusCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/sin/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 0.0", reply);
    }

    @Test
    public void testMicroserviceWorksForTangentCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/tan/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 0.0", reply);
    }

    @Test
    public void testMicroserviceWorksForToDegreesCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/toDegree/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 0.0", reply);
    }

    @Test
    public void testMicroserviceWorksForToRadiansCommand() throws Exception {
        String response = fetcher.sendGetRequest(
                "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/toRad/0");
        JSONObject jsonObject = new JSONObject(response);
        String reply = String.format("Result: %s", jsonObject.getString("result"));
        assertEquals("Result: 0.0", reply);
    }


}
