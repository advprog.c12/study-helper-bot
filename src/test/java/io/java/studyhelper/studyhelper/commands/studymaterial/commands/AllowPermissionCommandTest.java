package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AllowPermissionCommandTest {
    @Mock
    private PermitService permitService;

    @InjectMocks
    private AllowPermissionCommand allowPermissionCommand;

    private String prefix = "!";

    private String serverId = "835350969038012468";

    @BeforeEach
    public void setUp() {
        allowPermissionCommand = new AllowPermissionCommand(permitService, prefix);
    }

    @Test
    public void testAllowPermissionCommandGetCommandName() {
        assertEquals("allowpermission", allowPermissionCommand.getCommandName());
    }

    @Test
    public void testAllowPermissionCommandGetCommandDescription() {
        assertEquals("allow permission command", allowPermissionCommand.getCommandDescription());
    }

    @Test
    public void testAllowPermissionCommandExecuteCommandIsOwner() {
        List<String> args = new ArrayList<>(Arrays.asList("MANAGE_WEBHOOKS"));

        User user = mock(User.class);
        Guild guild = mock(Guild.class);
        Member member = mock(Member.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn(serverId);
        when(message.getAuthor()).thenReturn(user);
        when(guild.getMember(user)).thenReturn(member);
        when(member.isOwner()).thenReturn(true);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        allowPermissionCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testAllowPermissionCommandExecuteCommandIsNotOwner() {
        List<String> args = new ArrayList<>(Arrays.asList("MANAGE_WEBHOOKS"));

        User user = mock(User.class);
        Guild guild = mock(Guild.class);
        Member member = mock(Member.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn(serverId);
        when(message.getAuthor()).thenReturn(user);
        when(guild.getMember(user)).thenReturn(member);
        when(member.isOwner()).thenReturn(false);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        allowPermissionCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testAllowPermissionCommandIsArgsRequired() {
        assertTrue(allowPermissionCommand.isArgsRequired());
    }

    @Test
    public void testAllowPermissionCommandGetCommandUsage() {
        assertEquals("<PERMISSION_NAME>", allowPermissionCommand.getCommandUsage());
    }

    @Test
    public void testAllowPermissionCommandGetCommandCategory() {
        assertEquals("Study Material Storage", allowPermissionCommand.getCommandCategory());
    }

    @Test
    public void testAllowPermissionCommandIsGuildOnly() {
        assertTrue(allowPermissionCommand.isGuildOnly());
    }
}
