package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.commands.studymaterial.verifier.PermissionVerifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SaveInfoCommandTest {
    @Mock
    private MaterialService materialService;

    @Mock
    private PermitService permitService;

    @InjectMocks
    private SaveInfoCommand saveInfoCommand;

    private List<Permit> permits;

    private String prefix = "!";

    private String serverId = "835350969038012468";

    @BeforeEach
    public void setUp() {

        permits = new ArrayList<>();

        permits.add(new Permit(serverId, "ADMINISTRATOR"));
        permits.add(new Permit(serverId, "BAN_MEMBERS"));
        permits.add(new Permit(serverId, "KICK_MEMBERS"));
        permits.add(new Permit(serverId, "MANAGE_CHANNEL"));
        permits.add(new Permit(serverId, "MANAGE_PERMISSIONS"));
        permits.add(new Permit(serverId, "MANAGE_ROLES"));
        permits.add(new Permit(serverId, "MANAGE_SERVER"));

        saveInfoCommand = new SaveInfoCommand(materialService, permitService, prefix);
    }

    @Test
    public void testSaveInfoCommandGetCommandName() {
        assertEquals("saveinfo", saveInfoCommand.getCommandName());
    }

    @Test
    public void testSaveInfoCommandGetCommandDescription() {
        assertEquals("save info command", saveInfoCommand.getCommandDescription());
    }

    @Test
    public void testSaveInfoCommandExecuteCommandAllowed() {
        List<String> args = new ArrayList<>(Arrays.asList("test1, halo"));

        User user = mock(User.class);
        Guild guild = mock(Guild.class);
        Member member = mock(Member.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn(serverId);
        when(message.getAuthor()).thenReturn(user);
        when(guild.getMember(user)).thenReturn(member);

        when(permitService.getAllPermit(serverId)).thenReturn(permits);
        when(member.hasPermission(Permission.ADMINISTRATOR)).thenReturn(true);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        saveInfoCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testSaveInfoCommandExecuteCommandNotAllowed() {
        List<String> args = new ArrayList<>(Arrays.asList("test1, halo"));

        Guild guild = mock(Guild.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        saveInfoCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testSaveInfoCommandIsArgsRequired() {
        assertTrue(saveInfoCommand.isArgsRequired());
    }

    @Test
    public void testSaveInfoCommandGetCommandUsage() {
        assertEquals(
                "<info_category> <info_value> or <info_category> <info_value_1> <info_value_2> ... <info_value_n>",
                saveInfoCommand.getCommandUsage()
        );
    }

    @Test
    public void testSaveInfoCommandGetCommandCategory() {
        assertEquals("Study Material Storage", saveInfoCommand.getCommandCategory());
    }

    @Test
    public void testSaveInfoCommandIsGuildOnly() {
        assertTrue(saveInfoCommand.isGuildOnly());
    }
}
