package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class CopyreminderExecutionTest {

    @Mock
    private SchoolworksService schoolworksService;

    @InjectMocks
    private CopyreminderExecution copyreminderExecution;

    private Class<?> copyreminderExecutionClass;

    @BeforeEach
    public void setUp() throws Exception {
        copyreminderExecutionClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.executions.CopyreminderExecution");
        copyreminderExecution = new CopyreminderExecution(schoolworksService);
    }

    @Test
    public void testCopyreminderExecutionIsConcreteClass(){
        assertFalse(Modifier.isAbstract(copyreminderExecutionClass.getModifiers()));
    }

    @Test
    public void testCopyreminderExecutionIsAnExecution(){
        Collection<Type> interfaces = Arrays.asList(copyreminderExecutionClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution"))
        );
    }

    @Test
    public void testCopyreminderExecutionOverridesExecute() throws Exception {
        Class<?>[] executeArgs = new Class[2];
        executeArgs[0] = List.class;
        executeArgs[1] = Message.class;
        Method execute = copyreminderExecutionClass.getDeclaredMethod("execute", executeArgs);
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(2, execute.getParameterCount());
        assertTrue(Modifier.isPublic(execute.getModifiers()));
    }

    @Test
    public void testCopyreminderExecutionExecute(){
        CopyreminderExecution execution = new CopyreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("Gabriel", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Gabriel");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), args.get(0))).thenReturn(true);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), "Mario")).thenReturn(false);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        execution.execute(args, message);

        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(schoolwork.getName(), schoolwork.getAuthorName());
        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), author.getName());
        verify(schoolworksService, times(1)).createSchoolwork(schoolwork.getName(), schoolwork.getDescription(), author.getName());
    }

    @Test
    public void testCopyreminderExecutionExecuteInvalidCommandUsage(){
        CopyreminderExecution execution = new CopyreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES"));

        Message message = mock(Message.class);

        assertEquals("Invalid command usage!", execution.execute(args, message));
    }

    @Test
    public void testCopyreminderExecutionExecuteFirstIf(){
        CopyreminderExecution execution = new CopyreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("Gabriel", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Gabriel");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), args.get(0))).thenReturn(false);

        assertEquals(String.format("Schoolwork with **%s** by user **%s** does not exist!", args.get(1), args.get(0)), execution.execute(args, message));
    }
    @Test
    public void testCopyreminderExecutionExecuteSecondIf(){
        CopyreminderExecution execution = new CopyreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("Gabriel", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Gabriel");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), args.get(0))).thenReturn(true);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), "Mario")).thenReturn(true);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        assertEquals(String.format("Schoolwork with name **%s** already exists, use --delremind to remove it first!", schoolwork.getName()), execution.execute(args, message));
    }
    @Test
    public void testCopyreminderExecutionCorrectly(){
        CopyreminderExecution execution = new CopyreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("Gabriel", "TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Gabriel");

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), args.get(0))).thenReturn(true);
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(args.get(1), "Mario")).thenReturn(false);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        assertEquals(String.format("Successfully copied reminder by user **%s** with name **%s** to your reminders!", author.getName(), schoolwork.getName()), execution.execute(args, message));
    }
}