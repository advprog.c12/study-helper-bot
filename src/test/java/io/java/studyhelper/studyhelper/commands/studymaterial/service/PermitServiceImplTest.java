package io.java.studyhelper.studyhelper.commands.studymaterial.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import io.java.studyhelper.studyhelper.commands.studymaterial.repository.PermitRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PermitServiceImplTest {
    @Mock
    private PermitRepository permitRepository;

    @InjectMocks
    private PermitServiceImpl permitService;

    private Permit permit;

    @BeforeEach
    public void setUp() {
        permit = new Permit("835350969038012468", "ADMINISTRATOR");
    }

    @Test
    public void testServiceCreatePermit() {
        lenient().when(permitService.createPermit("835350969038012468", "ADMINISTRATOR"))
                .thenReturn(permit);
    }

    @Test
    public void testServiceGetAllPermit() {
        List<Permit> permits = new ArrayList<>();
        permits.add(permit);
        when(permitRepository.findAllByServerId("835350969038012468")).thenReturn(permits);
        List<Permit> res = permitService.getAllPermit("835350969038012468");
        assertEquals("835350969038012468", res.get(0).getServerId());
        assertEquals("ADMINISTRATOR", res.get(0).getPermitValue());
    }

    @Test
    public void testServiceGetPermitByPermitValue() {
        when(permitRepository
                .findPermitByPermitValueAndServerId("ADMINISTRATOR", "835350969038012468"))
                .thenReturn(permit);
        Permit res = permitService.getPermitByPermitValue("ADMINISTRATOR", "835350969038012468");
        assertEquals(res.getPermitValue(), permit.getPermitValue());
    }

    @Test
    public void testServiceDeletePermitByPermitValue() {
        when(permitRepository
                .findPermitByPermitValueAndServerId("MANAGE_SERVER", "835350969038012468"))
                .thenReturn(permit);

        permitService.deletePermitByPermitValue("MANAGE_SERVER", "835350969038012468");
    }
}
