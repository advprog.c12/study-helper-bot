package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.CopyreminderExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksServiceImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CopyreminderCommandTest {

    @Mock
    private SchoolworksServiceImpl schoolworksService;

    private Class<?> copyreminderCommandClass;
    private CopyreminderCommand instance;

    @BeforeEach
    public void setUp() throws Exception {
        copyreminderCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.commands.CopyreminderCommand");
        instance = new CopyreminderCommand(schoolworksService);
    }

    @Test
    public void testCopyreminderCommandIsAPublicClass() {
        int classModifiers = copyreminderCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = copyreminderCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("copyreminder", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = copyreminderCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Copy schoolwork reminder with name form other user", commandDescription);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = copyreminderCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandCategory = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Schoolwork Reminder", commandCategory);
    }

    @Test
    public void testGetExecutionMethod() throws NoSuchMethodException {
        Method method = copyreminderCommandClass.getDeclaredMethod("getExecution");
        int methodModifiers = method.getModifiers();

        Execution execution = instance.getExecution();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(execution instanceof Execution);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = copyreminderCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteCommand() {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        User author = mock(User.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);
        when(message.getAuthor()).thenReturn(author);
        when(author.getName()).thenReturn("AuthorName");

        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(false);

        List<String> args = new ArrayList<>();
        args.add("TargetAuthor");
        args.add("ReminderName");

        instance.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
        verify(message, times(1)).getAuthor();
        verify(author, times(1)).getName();
        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString());
    }
}
