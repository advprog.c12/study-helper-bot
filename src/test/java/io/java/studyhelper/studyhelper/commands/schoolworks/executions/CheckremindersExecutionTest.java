package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class CheckremindersExecutionTest {
    @Mock
    private SchoolworksService schoolworksService;

    @InjectMocks
    private CheckremindersExecution checkremindersExecution;

    private Class<?> checkremindersExecutionClass;

    @BeforeEach
    public void setUp() throws Exception {
        checkremindersExecutionClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.executions.CheckremindersExecution");
        checkremindersExecution = new CheckremindersExecution(schoolworksService);
    }

    @Test
    public void testCheckremindersExecutionIsConcreteClass(){
        assertFalse(Modifier.isAbstract(checkremindersExecutionClass.getModifiers()));
    }

    @Test
    public void testCheckremindersExecutionIsAnExecution(){
        Collection<Type> interfaces = Arrays.asList(checkremindersExecutionClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution"))
        );
    }

    @Test
    public void testCheckremindersExecutionOverridesExecute() throws Exception {
        Class<?>[] executeArgs = new Class[2];
        executeArgs[0] = List.class;
        executeArgs[1] = Message.class;
        Method execute = checkremindersExecutionClass.getDeclaredMethod("execute", executeArgs);
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(2, execute.getParameterCount());
        assertTrue(Modifier.isPublic(execute.getModifiers()));
    }

    @Test
    public void testCheckremindersExecutionExecute(){
        CheckremindersExecution execution = new CheckremindersExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));
        Iterable<Schoolwork> schoolworks = new ArrayList<>(Arrays.asList(new Schoolwork("TES", "TES", "Mario")));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.getListSchoolwork(anyString())).thenReturn(schoolworks);

        execution.execute(args, message);

        verify(schoolworksService, times(1)).getListSchoolwork(author.getName());
    }

    @Test
    public void testRemindExecutionExecuteNoSchoolworks(){

        CheckremindersExecution execution = new CheckremindersExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));
        Iterable<Schoolwork> schoolworks = new ArrayList<>();

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.getListSchoolwork(anyString())).thenReturn(schoolworks);

        String EXPECTED_RESULT = String.format("%s's Reminders:\n", author.getName());
        EXPECTED_RESULT += "There is no schoolworks listed!";

        assertEquals(EXPECTED_RESULT, execution.execute(args, message));
    }

    @Test
    public void testRemindExecutionExecuteAllSchoolworks(){

        CheckremindersExecution execution = new CheckremindersExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES", "TES"));
        Iterable<Schoolwork> schoolworks = new ArrayList<Schoolwork>(Arrays.asList(new Schoolwork("TES", "TES", "Mario"), new Schoolwork("TES1", "TES1", "Mario")));

        User author = mock(User.class);
        Message message = mock(Message.class);

        when(author.getName()).thenReturn("Mario");
        when(message.getAuthor()).thenReturn(author);
        when(schoolworksService.getListSchoolwork(anyString())).thenReturn(schoolworks);

        String EXPECTED_RESULT = String.format("%s's Reminders:\n", author.getName());
        for(Schoolwork schoolwork : schoolworks){
            EXPECTED_RESULT += String.format("%s\n",schoolwork.getName());
        }

        assertEquals(EXPECTED_RESULT, execution.execute(args, message));
    }
}
