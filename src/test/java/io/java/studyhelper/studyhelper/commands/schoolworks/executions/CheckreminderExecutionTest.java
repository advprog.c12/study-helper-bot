package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class CheckreminderExecutionTest {
    @Mock
    private SchoolworksService schoolworksService;

    @InjectMocks
    private CheckreminderExecution checkreminderExecution;

    private Class<?> checkreminderExecutionClass;

    @BeforeEach
    public void setUp() throws Exception {
        checkreminderExecutionClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.executions.RemindExecution");
        checkreminderExecution = new CheckreminderExecution(schoolworksService);
    }

    @Test
    public void testCopyreminderExecutionIsConcreteClass(){
        assertFalse(Modifier.isAbstract(checkreminderExecutionClass.getModifiers()));
    }

    @Test
    public void testCopyreminderExecutionIsAnExecution(){
        Collection<Type> interfaces = Arrays.asList(checkreminderExecutionClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution"))
        );
    }

    @Test
    public void testCheckreminderExecutionOverridesExecute() throws Exception {
        Class<?>[] executeArgs = new Class[2];
        executeArgs[0] = List.class;
        executeArgs[1] = Message.class;
        Method execute = checkreminderExecutionClass.getDeclaredMethod("execute", executeArgs);
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(2, execute.getParameterCount());
        assertTrue(Modifier.isPublic(execute.getModifiers()));
    }

    @Test
    public void testRemindExecutionExecute(){
        CheckreminderExecution execution = new CheckreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Mario");

        when(message.getAuthor()).thenReturn(author);
        when(author.getName()).thenReturn("Mario");
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);


        execution.execute(args, message);

        verify(schoolworksService, times(1)).checkSchoolworkWithoutNameAndAuthorNameExist(args.get(0), author.getName());
        verify(schoolworksService, times(1)).getSchoolworkByNameAndAuthorName(args.get(0), author.getName());
    }

    @Test
    public void testCheckreminderExecutionExecuteInvalidCommandUsage(){
        CheckreminderExecution execution = new CheckreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("tes", "tes"));

        Message message = mock(Message.class);
        assertEquals("Invalid command usage!", execution.execute(args, message));
    }

    @Test
    public void testCheckreminderExecutionExecuteIfNameAndAuthorDoesNotExist(){
        CheckreminderExecution execution = new CheckreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Mario");

        when(message.getAuthor()).thenReturn(author);
        when(author.getName()).thenReturn("Mario");
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(false);

        assertEquals(String.format("Schoolwork with name **%s** does not exist!", args.get(0)), execution.execute(args, message));
    }

    @Test
    public void testCheckreminderExecutionExecuteIfNameAndAuthorDoesExist(){
        CheckreminderExecution execution = new CheckreminderExecution(this.schoolworksService);
        List<String> args = new ArrayList<>(Arrays.asList("TES"));

        User author = mock(User.class);
        Message message = mock(Message.class);
        Schoolwork schoolwork = new Schoolwork("TES", "TES", "Mario");

        when(message.getAuthor()).thenReturn(author);
        when(author.getName()).thenReturn("Mario");
        when(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(anyString(), anyString())).thenReturn(true);
        when(schoolworksService.getSchoolworkByNameAndAuthorName(anyString(), anyString())).thenReturn(schoolwork);

        assertEquals(String.format("From: %s\nReminder Name: %s\nDescription:\n%s", schoolwork.getAuthorName(), schoolwork.getName(), schoolwork.getDescription()), execution.execute(args, message));
    }
}
