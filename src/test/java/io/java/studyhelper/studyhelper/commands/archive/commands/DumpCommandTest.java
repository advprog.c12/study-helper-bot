package io.java.studyhelper.studyhelper.commands.archive.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.archive.model.UserMessage;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DumpCommandTest {

    @Mock
    private ArchiveServiceImpl archiveService;

    private Class<?> dumpCommandClass;
    private DumpCommand instance;

    @BeforeEach
    public void setUp() throws Exception {
        dumpCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.archive.commands.DumpCommand");
        instance = new DumpCommand(archiveService);
    }

    @Test
    public void testDumpCommandIsAPublicClass() {
        int classModifiers = dumpCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = dumpCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("dump", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = dumpCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Dump all message that mentions you since your last dump", commandDescription);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = dumpCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandCategory = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Chat Archive", commandCategory);
    }

    @Test
    public void testIsDMOnlyMethod() throws NoSuchMethodException {
        Method method = dumpCommandClass.getDeclaredMethod("isDMOnly");
        int methodModifiers = method.getModifiers();

        boolean isDMOnly = instance.isDMOnly();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(true, isDMOnly);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = dumpCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteCommandEmptyMessages() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getMessagesForUser(author)).thenReturn(new ArrayList<>());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getMessagesForUser(author);
    }

    @Test
    public void testExecuteCommandWithMessages() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        UserMessage savedMessage = mock(UserMessage.class);

        when(savedMessage.getMessageGuild()).thenReturn("My Discord Server");
        when(savedMessage.getMessageChannel()).thenReturn("my-channel");
        when(savedMessage.getMessageAuthorName()).thenReturn("Author");
        when(savedMessage.getMessageAuthorDiscriminator()).thenReturn("1234");
        when(savedMessage.getMessageContent()).thenReturn("This is a message");
        when(savedMessage.getMessageLink()).thenReturn("https://discord.com/channels/GUILD-01/CHANNEL-01/MESSAGE-01");

        List<UserMessage> savedMessages = new ArrayList<>();
        savedMessages.add(savedMessage);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getMessagesForUser(author)).thenReturn(savedMessages);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getMessagesForUser(author);
        verify(savedMessage, times(1)).getMessageGuild();
        verify(savedMessage, times(1)).getMessageChannel();
        verify(savedMessage, times(1)).getMessageAuthorName();
        verify(savedMessage, times(1)).getMessageAuthorDiscriminator();
        verify(savedMessage, times(1)).getMessageContent();
        verify(savedMessage, times(1)).getMessageLink();
    }
}
