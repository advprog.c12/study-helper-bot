package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ShowListCommandTest {
    private Class<?> showListCommandClass;
    private ShowListCommand instance;

    @Mock
    private FetcherServiceImpl fetcherService;

    @BeforeEach
    public void setUp() throws Exception {
        showListCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands."
                + "randomizer.commands.ShowListCommand");
        instance = new ShowListCommand(fetcherService);
    }

    @Test
    public void testRegisterCommandIsAPublicClass() {
        int classModifiers = showListCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testRegisterCommandOnGetCommandName() throws NoSuchMethodException {
        Method method = showListCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("showlist", commandName);

    }

    @Test
    public void testRegisterCommandOnGetCommandDescription() throws NoSuchMethodException {
        Method method = showListCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandDescription();

        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals("Show list of members", commandName);
    }



    @Test
    public void testRegisterCommandOnIsArgsRequired() {
        assertFalse(instance.isArgsRequired());

    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = showListCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Randomizer", commandName);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = showListCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testShowListCommandOnExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();

        String jsonString = String.format("{\n"
                + "    \"result\": \"List of anggota : \\n a\\nb\\n\""
                + "\n}");

        when(fetcherService.sendGetRequest(anyString())).thenReturn(jsonString);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }

    @Test
    public void testDeleteCommandExceptionOnExecuteCommand() throws Exception {
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        List<String> args = new ArrayList<>();

        doThrow(IllegalStateException.class).when(fetcherService).sendGetRequest(anyString());
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message,args);

        verify(fetcherService, times(1)).sendGetRequest(anyString());
    }
}
