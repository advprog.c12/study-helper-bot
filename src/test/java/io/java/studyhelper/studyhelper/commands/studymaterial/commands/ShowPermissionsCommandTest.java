package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ShowPermissionsCommandTest {

    @Mock
    private PermitService permitService;

    @InjectMocks
    private ShowPermissionsCommand showPermissionsCommand;

    private String prefix = "!";

    @BeforeEach
    public void setUp() {
        showPermissionsCommand = new ShowPermissionsCommand(permitService, prefix);
    }

    @Test
    public void testShowPermissionsCommandGetCommandName() {
        assertEquals("showpermissions", showPermissionsCommand.getCommandName());
    }

    @Test
    public void testShowPermissionsCommandGetCommandDescription() {
        assertEquals("show permissions command", showPermissionsCommand.getCommandDescription());
    }

    @Test
    public void testShowPermissionsCommandExecuteCommand() {
        List<String> args = new ArrayList<>(Arrays.asList("test1, halo"));

        Guild guild = mock(Guild.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        showPermissionsCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(any(MessageEmbed.class));
    }

    @Test
    public void testShowPermissionsCommandGetCommandCategory() {
        assertEquals("Study Material Storage", showPermissionsCommand.getCommandCategory());
    }

    @Test
    public void testShowPermissionsCommandIsGuildOnly() {
        assertTrue(showPermissionsCommand.isGuildOnly());
    }
}
