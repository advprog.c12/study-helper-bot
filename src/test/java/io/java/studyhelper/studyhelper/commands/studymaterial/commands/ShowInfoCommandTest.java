package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ShowInfoCommandTest {
    @Mock
    private MaterialService materialService;

    @InjectMocks
    private ShowInfoCommand showInfoCommand;

    @BeforeEach
    public void setUp() {
        showInfoCommand = new ShowInfoCommand(materialService);
    }

    @Test
    public void testShowInfoCommandGetCommandName() {
        assertEquals("showinfo", showInfoCommand.getCommandName());
    }

    @Test
    public void testShowInfoCommandGetCommandDescription() {
        assertEquals("show info command", showInfoCommand.getCommandDescription());
    }

    @Test
    public void testShowInfoCommandExecuteCommand() {
        List<String> args = new ArrayList<>(Arrays.asList("test1", "halo"));

        Guild guild = mock(Guild.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn("835350969038012468");
        when(materialService.getMaterialValueByCategory(anyString(), anyString())).thenReturn("halo");
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);

        showInfoCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(any(MessageEmbed.class));
    }

    @Test
    public void testShowInfoCommandIsArgsRequired() {
        assertTrue(showInfoCommand.isArgsRequired());
    }

    @Test
    public void testShowInfoCommandGetCommandUsage() {
        assertEquals("<info_category>", showInfoCommand.getCommandUsage());
    }

    @Test
    public void testShowInfoCommandGetCommandCategory() {
        assertEquals("Study Material Storage", showInfoCommand.getCommandCategory());
    }

    @Test
    public void testShowInfoCommandIsGuildOnly() {
        assertTrue(showInfoCommand.isGuildOnly());
    }
}
