package io.java.studyhelper.studyhelper.commands.archive.commands;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveServiceImpl;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ArchiveCommandTest {

    @Mock
    private ArchiveServiceImpl archiveService;

    private Class<?> archiveCommandClass;
    private ArchiveCommand instance;

    @BeforeEach
    public void setUp() throws Exception {
        archiveCommandClass = Class.forName("io.java.studyhelper.studyhelper.commands.archive.commands.ArchiveCommand");
        instance = new ArchiveCommand(archiveService, "!");
    }

    @Test
    public void testArchiveCommandIsAPublicClass() {
        int classModifiers = archiveCommandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(instance instanceof Command);
    }

    @Test
    public void testGetCommandNameMethod() throws NoSuchMethodException {
        Method method = archiveCommandClass.getDeclaredMethod("getCommandName");
        int methodModifiers = method.getModifiers();

        String commandName = instance.getCommandName();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("archive", commandName);
    }

    @Test
    public void testGetCommandDescriptionMethod() throws NoSuchMethodException {
        Method method = archiveCommandClass.getDeclaredMethod("getCommandDescription");
        int methodModifiers = method.getModifiers();

        String commandDescription = instance.getCommandDescription();
        String expected = "Multiple usage:\n" +
                        "Use `!archive` to toggle archiving on/off for the server\n" +
                        "Use `!archive status` to check the archiving status for the server\n" +
                        "Use `!archive list` to list all servers that are archiving";

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(expected, commandDescription);
    }

    @Test
    public void testGetCommandCategoryMethod() throws NoSuchMethodException {
        Method method = archiveCommandClass.getDeclaredMethod("getCommandCategory");
        int methodModifiers = method.getModifiers();

        String commandCategory = instance.getCommandCategory();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("Chat Archive", commandCategory);
    }

    @Test
    public void testGetCommandUsageMethod() throws NoSuchMethodException {
        Method method = archiveCommandClass.getDeclaredMethod("getCommandUsage");
        int methodModifiers = method.getModifiers();

        String commandUsage = instance.getCommandUsage();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("<none or status or list>", commandUsage);
    }

    @Test
    public void testHasExecuteCommandMethod() throws NoSuchMethodException {
        Method method = archiveCommandClass.getDeclaredMethod("executeCommand", Message.class, List.class);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testExecuteCommandTogglingOn() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        User author = mock(User.class);
        Guild guild = mock(Guild.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(message.getAuthor()).thenReturn(author);
        when(message.getGuild()).thenReturn(guild);
        when(archiveService.userWantsArchive(any(User.class), any(Guild.class))).thenReturn(true);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).toggleArchivingStatus(any(User.class), any(Guild.class));
        verify(archiveService, times(1)).userWantsArchive(any(User.class), any(Guild.class));
    }

    @Test
    public void testExecuteCommandTogglingOff() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        User author = mock(User.class);
        Guild guild = mock(Guild.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(message.getAuthor()).thenReturn(author);
        when(message.getGuild()).thenReturn(guild);
        when(archiveService.userWantsArchive(any(User.class), any(Guild.class))).thenReturn(false);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).toggleArchivingStatus(any(User.class), any(Guild.class));
        verify(archiveService, times(1)).userWantsArchive(any(User.class), any(Guild.class));
    }

    @Test
    public void testExecuteCommandTogglingInDM() {
        List<String> args = new ArrayList<>();

        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(0)).toggleArchivingStatus(any(User.class), any(Guild.class));
        verify(archiveService, times(0)).userWantsArchive(any(User.class), any(Guild.class));
    }


    @Test
    public void testExecuteCommandStatusIsOn() {
        List<String> args = new ArrayList<>();
        args.add("status");

        Message message = mock(Message.class);
        User author = mock(User.class);
        Guild guild = mock(Guild.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(message.getAuthor()).thenReturn(author);
        when(message.getGuild()).thenReturn(guild);
        when(archiveService.userWantsArchive(any(User.class), any(Guild.class))).thenReturn(true);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).userWantsArchive(any(User.class), any(Guild.class));
    }

    @Test
    public void testExecuteCommandStatusIsOff() {
        List<String> args = new ArrayList<>();
        args.add("status");

        Message message = mock(Message.class);
        User author = mock(User.class);
        Guild guild = mock(Guild.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(message.getAuthor()).thenReturn(author);
        when(message.getGuild()).thenReturn(guild);
        when(archiveService.userWantsArchive(any(User.class), any(Guild.class))).thenReturn(false);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).userWantsArchive(any(User.class), any(Guild.class));
    }

    @Test
    public void testExecuteCommandStatusInDM() {
        List<String> args = new ArrayList<>();
        args.add("status");

        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        instance.executeCommand(message, args);

        verify(archiveService, times(0)).userWantsArchive(any(User.class), any(Guild.class));
    }

    @Test
    public void testExecuteCommandListNoActiveGuilds() {
        List<String> args = new ArrayList<>();
        args.add("list");

        Message message = mock(Message.class);
        Message sentMessage = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        PrivateChannel privateMessageChannel = mock(PrivateChannel.class);
        RestAction<PrivateChannel> privateChannelAction = mock(RestAction.class);
        MessageAction privateMessageAction = mock(MessageAction.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getArchivingStatusesForUser(author)).thenReturn(new ArrayList<>());
        when(author.openPrivateChannel()).thenReturn(privateChannelAction);
        when(privateMessageChannel.sendMessage(anyString())).thenReturn(privateMessageAction);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        doAnswer(ans -> {
            Consumer<PrivateChannel> callback = ans.getArgument(0, Consumer.class);
            callback.accept(privateMessageChannel);
            return null;
        }).when(privateChannelAction).queue(any(Consumer.class));

        doAnswer(ans -> {
            Consumer<Message> callback = ans.getArgument(0, Consumer.class);
            callback.accept(sentMessage);
            return null;
        }).when(privateMessageAction).queue(any(Consumer.class));

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getArchivingStatusesForUser(author);
    }

    @Test
    public void testExecuteCommandListActiveGuilds() {
        List<String> args = new ArrayList<>();
        args.add("list");

        Message message = mock(Message.class);
        Message sentMessage = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        PrivateChannel privateMessageChannel = mock(PrivateChannel.class);
        RestAction<PrivateChannel> privateChannelAction = mock(RestAction.class);
        MessageAction privateMessageAction = mock(MessageAction.class);
        MessageAction messageAction = mock(MessageAction.class);

        ArchivingStatus archivingStatus = mock(ArchivingStatus.class);

        when(archivingStatus.isArchiving()).thenReturn(true);
        when(archivingStatus.getGuildName()).thenReturn("My Server");

        List<ArchivingStatus> archivingStatuses = new ArrayList<>();
        archivingStatuses.add(archivingStatus);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getArchivingStatusesForUser(author)).thenReturn(archivingStatuses);
        when(author.openPrivateChannel()).thenReturn(privateChannelAction);
        when(privateMessageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(privateMessageAction);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.TEXT);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        doAnswer(ans -> {
            Consumer<PrivateChannel> callback = ans.getArgument(0, Consumer.class);
            callback.accept(privateMessageChannel);
            return null;
        }).when(privateChannelAction).queue(any(Consumer.class));

        doAnswer(ans -> {
            Consumer<Message> callback = ans.getArgument(0, Consumer.class);
            callback.accept(sentMessage);
            return null;
        }).when(privateMessageAction).queue(any(Consumer.class));

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getArchivingStatusesForUser(author);
    }

    @Test
    public void testExecuteCommandListNoActiveGuildsInDM() {
        List<String> args = new ArrayList<>();
        args.add("list");

        Message message = mock(Message.class);
        Message sentMessage = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        PrivateChannel privateMessageChannel = mock(PrivateChannel.class);
        RestAction<PrivateChannel> privateChannelAction = mock(RestAction.class);
        MessageAction privateMessageAction = mock(MessageAction.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getArchivingStatusesForUser(author)).thenReturn(new ArrayList<>());
        when(author.openPrivateChannel()).thenReturn(privateChannelAction);
        when(privateMessageChannel.sendMessage(anyString())).thenReturn(privateMessageAction);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);

        doAnswer(ans -> {
            Consumer<PrivateChannel> callback = ans.getArgument(0, Consumer.class);
            callback.accept(privateMessageChannel);
            return null;
        }).when(privateChannelAction).queue(any(Consumer.class));

        doAnswer(ans -> {
            Consumer<Message> callback = ans.getArgument(0, Consumer.class);
            callback.accept(sentMessage);
            return null;
        }).when(privateMessageAction).queue(any(Consumer.class));

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getArchivingStatusesForUser(author);
    }

    @Test
    public void testExecuteCommandListActiveGuildsInDM() {
        List<String> args = new ArrayList<>();
        args.add("list");

        Message message = mock(Message.class);
        Message sentMessage = mock(Message.class);
        User author = mock(User.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        PrivateChannel privateMessageChannel = mock(PrivateChannel.class);
        RestAction<PrivateChannel> privateChannelAction = mock(RestAction.class);
        MessageAction privateMessageAction = mock(MessageAction.class);
        MessageAction messageAction = mock(MessageAction.class);

        ArchivingStatus archivingStatus = mock(ArchivingStatus.class);

        when(archivingStatus.isArchiving()).thenReturn(true);
        when(archivingStatus.getGuildName()).thenReturn("My Server");

        List<ArchivingStatus> archivingStatuses = new ArrayList<>();
        archivingStatuses.add(archivingStatus);

        when(message.getAuthor()).thenReturn(author);
        when(archiveService.getArchivingStatusesForUser(author)).thenReturn(archivingStatuses);
        when(author.openPrivateChannel()).thenReturn(privateChannelAction);
        when(privateMessageChannel.sendMessage(any(MessageEmbed.class))).thenReturn(privateMessageAction);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.getType()).thenReturn(ChannelType.PRIVATE);

        doAnswer(ans -> {
            Consumer<PrivateChannel> callback = ans.getArgument(0, Consumer.class);
            callback.accept(privateMessageChannel);
            return null;
        }).when(privateChannelAction).queue(any(Consumer.class));

        doAnswer(ans -> {
            Consumer<Message> callback = ans.getArgument(0, Consumer.class);
            callback.accept(sentMessage);
            return null;
        }).when(privateMessageAction).queue(any(Consumer.class));

        instance.executeCommand(message, args);

        verify(archiveService, times(1)).getArchivingStatusesForUser(author);
    }
}
