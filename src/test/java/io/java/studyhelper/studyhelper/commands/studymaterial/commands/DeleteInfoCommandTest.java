package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.commands.studymaterial.verifier.PermissionVerifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DeleteInfoCommandTest {
    @Mock
    private MaterialService materialService;

    @Mock
    private PermitService permitService;

    @InjectMocks
    private DeleteInfoCommand deleteInfoCommand;

    private List<Permit> permits;

    private String prefix = "!";

    private String serverId = "835350969038012468";

    @BeforeEach
    public void setUp() {

        permits = new ArrayList<>();

        permits.add(new Permit(serverId, "ADMINISTRATOR"));
        permits.add(new Permit(serverId, "BAN_MEMBERS"));
        permits.add(new Permit(serverId, "KICK_MEMBERS"));
        permits.add(new Permit(serverId, "MANAGE_CHANNEL"));
        permits.add(new Permit(serverId, "MANAGE_PERMISSIONS"));
        permits.add(new Permit(serverId, "MANAGE_ROLES"));
        permits.add(new Permit(serverId, "MANAGE_SERVER"));

        deleteInfoCommand = new DeleteInfoCommand(materialService, permitService, prefix);
    }

    @Test
    public void testDeleteInfoCommandGetCommandName() {
        assertEquals("deleteinfo", deleteInfoCommand.getCommandName());
    }

    @Test
    public void testDeleteInfoCommandGetCommandDescription() {
        assertEquals("delete info command", deleteInfoCommand.getCommandDescription());
    }

    @Test
    public void testDeleteInfoCommandExecuteCommandAllowed() {
        List<String> args = new ArrayList<>(Arrays.asList("test1, halo"));

        User user = mock(User.class);
        Guild guild = mock(Guild.class);
        Member member = mock(Member.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(guild.getId()).thenReturn(serverId);
        when(message.getAuthor()).thenReturn(user);
        when(guild.getMember(user)).thenReturn(member);

        when(permitService.getAllPermit(serverId)).thenReturn(permits);
        when(member.hasPermission(Permission.ADMINISTRATOR)).thenReturn(true);

        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        deleteInfoCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testDeleteInfoCommandExecuteCommandNotAllowed() {
        List<String> args = new ArrayList<>(Arrays.asList("test1, halo"));

        Guild guild = mock(Guild.class);
        Message message = mock(Message.class);
        MessageChannel messageChannel = mock(MessageChannel.class);
        MessageAction messageAction = mock(MessageAction.class);

        when(message.getGuild()).thenReturn(guild);
        when(message.getChannel()).thenReturn(messageChannel);
        when(messageChannel.sendMessage(anyString())).thenReturn(messageAction);

        deleteInfoCommand.executeCommand(message, args);

        verify(message, times(1)).getChannel();
        verify(messageChannel, times(1)).sendMessage(anyString());
    }

    @Test
    public void testDeleteInfoCommandIsArgsRequired() {
        assertTrue(deleteInfoCommand.isArgsRequired());
    }

    @Test
    public void testDeleteInfoCommandGetCommandUsage() {
        assertEquals(
                "<info_category>",
                deleteInfoCommand.getCommandUsage()
        );
    }

    @Test
    public void testDeleteInfoCommandGetCommandCategory() {
        assertEquals("Study Material Storage", deleteInfoCommand.getCommandCategory());
    }

    @Test
    public void testDeleteInfoCommandIsGuildOnly() {
        assertTrue(deleteInfoCommand.isGuildOnly());
    }
}
