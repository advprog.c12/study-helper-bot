package io.java.studyhelper.studyhelper.commands.schoolworks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SchoolworkIDTest {
    private Class<?> schoolworkIDClass;

    @BeforeEach
    public void setUp() throws Exception {
        schoolworkIDClass = Class.forName("io.java.studyhelper.studyhelper.commands.schoolworks.model.SchoolworkID");
    }

    @Test
    public void testSchoolworkIDIsConcreteClass(){
        assertFalse(Modifier.isAbstract(schoolworkIDClass.getModifiers()));
    }

    @Test
    public void testBlankConstructor(){
        SchoolworkID schoolworkID = new SchoolworkID();
        assertNotNull(schoolworkID);
    }

    @Test
    public void testNonBlankConstructor(){
        SchoolworkID schoolworkID = new SchoolworkID("TES", "TES");
        assertNotNull(schoolworkID);
    }

    @Test
    public void testSchoolworkIDGetNameMethodExist() throws Exception {
        Class<?>[] getNameArgs = new Class[0];
        Method getName = schoolworkIDClass.getDeclaredMethod("getName", getNameArgs);
        assertEquals(String.class, getName.getGenericReturnType());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSchoolworkIDGetAuthorNameMethodExist() throws Exception {
        Class<?>[] getAuthorNameArgs = new Class[0];
        Method getAuthorName = schoolworkIDClass.getDeclaredMethod("getAuthorName", getAuthorNameArgs);
        assertEquals(String.class, getAuthorName.getGenericReturnType());
        assertEquals(0, getAuthorName.getParameterCount());
        assertTrue(Modifier.isPublic(getAuthorName.getModifiers()));
    }

    @Test
    public void testSchoolworkIDSetNameMethodExist() throws Exception {
        Class<?>[] setNameArgs = new Class[1];
        setNameArgs[0] = String.class;
        Method setName = schoolworkIDClass.getDeclaredMethod("setName", setNameArgs);
        assertEquals(void.class, setName.getGenericReturnType());
        assertEquals(1, setName.getParameterCount());
        assertTrue(Modifier.isPublic(setName.getModifiers()));
    }

    @Test
    public void testSchoolworkIDSetAuthorNameMethodExist() throws Exception {
        Class<?>[] setAuthorNameArgs = new Class[1];
        setAuthorNameArgs[0] = String.class;
        Method setAuthorName = schoolworkIDClass.getDeclaredMethod("setAuthorName", setAuthorNameArgs);
        assertEquals(void.class, setAuthorName.getGenericReturnType());
        assertEquals(1, setAuthorName.getParameterCount());
        assertTrue(Modifier.isPublic(setAuthorName.getModifiers()));
    }

    @Test
    public void testSchoolworkIDOverrideEqualsMethod() throws Exception {
        Class<?>[] equalsArgs = new Class[1];
        equalsArgs[0] = Object.class;
        Method equals = schoolworkIDClass.getDeclaredMethod("equals", equalsArgs);
        assertEquals(boolean.class, equals.getGenericReturnType());
        assertEquals(1, equals.getParameterCount());
        assertTrue(Modifier.isPublic(equals.getModifiers()));
    }

    @Test
    public void testSchoolworkIDOverrideHashCodeMethod() throws Exception {
        Class<?>[] hashCodeArgs = new Class[0];
        Method hashCode = schoolworkIDClass.getDeclaredMethod("hashCode", hashCodeArgs);
        assertEquals(int.class, hashCode.getGenericReturnType());
        assertEquals(0, hashCode.getParameterCount());
        assertTrue(Modifier.isPublic(hashCode.getModifiers()));
    }

    @Test
    public void testSchoolworkIDGetNameOutputIsCorrect(){
        String EXPECTED_NAME = "GANTENG";
        SchoolworkID schoolworkID = new SchoolworkID(EXPECTED_NAME, "IRRELEVANT");
        assertEquals(EXPECTED_NAME, schoolworkID.getName());
    }

    @Test
    public void testSchoolworkIDGetAuthorNameOutputIsCorrect(){
        String EXPECTED_AUTHOR_NAME = "GANTENG";
        SchoolworkID schoolworkID = new SchoolworkID("IRRELEVANT", EXPECTED_AUTHOR_NAME);
        assertEquals(EXPECTED_AUTHOR_NAME, schoolworkID.getAuthorName());
    }

    @Test
    public void testSetNameSetsProperly() throws Exception {
        String EXPECTED_NAME = "GANTENG";
        final SchoolworkID schoolworkID = new SchoolworkID();

        schoolworkID.setName(EXPECTED_NAME);
        assertEquals(EXPECTED_NAME, schoolworkID.getName());
    }

    @Test
    public void testSetAuthorNameSetsProperly() throws Exception {
        String EXPECTED_AUTHOR_NAME = "GANTENG";
        final SchoolworkID schoolworkID = new SchoolworkID();

        schoolworkID.setAuthorName(EXPECTED_AUTHOR_NAME);
        assertEquals(EXPECTED_AUTHOR_NAME, schoolworkID.getAuthorName());
    }

    @Test
    public void testEqualsIfObject(){
        SchoolworkID schoolworkID = new SchoolworkID();
        SchoolworkID sameObjAddress = schoolworkID;
        assertTrue(schoolworkID.equals(sameObjAddress));
    }

    @Test
    public void testEqualsIfNullOrDifferentClassIfFalse(){
        String obj = "TES";
        Object nullObject = null;
        SchoolworkID schoolworkID = new SchoolworkID();
        assertFalse(schoolworkID.equals(nullObject));
        assertFalse(schoolworkID.equals(obj));
    }

    @Test
    public void testEqualsIfSameNameButDifferentAuthor(){
        SchoolworkID schoolworkIDOne = new SchoolworkID("TES", "AUTHOR1");
        SchoolworkID schoolworkIDTwo = new SchoolworkID("TES", "AUTHOR2");
        assertFalse(schoolworkIDOne.equals(schoolworkIDTwo));
    }

    @Test
    public void testEqualsIfDifferentNameButSameAuthor(){
        SchoolworkID schoolworkIDOne = new SchoolworkID("NAME1", "TES");
        SchoolworkID schoolworkIDTwo = new SchoolworkID("NAME2", "TES");
        assertFalse(schoolworkIDOne.equals(schoolworkIDTwo));
    }

    @Test
    public void testEqualsIfSameNameSameAuthor(){
        SchoolworkID schoolworkIDOne = new SchoolworkID("NAME", "AUTHORNAME");
        SchoolworkID schoolworkIDTwo = new SchoolworkID("NAME", "AUTHORNAME");
        assertTrue(schoolworkIDOne.equals(schoolworkIDTwo));
    }

    @Test
    public void testHashCodeIfBothNull(){
        int EXPECTED_RESULT = 7;
        SchoolworkID schoolwork = new SchoolworkID();

        EXPECTED_RESULT = 31 * EXPECTED_RESULT;
        EXPECTED_RESULT = 31 * EXPECTED_RESULT;
        assertEquals(EXPECTED_RESULT, schoolwork.hashCode());
    }

    @Test
    public void testHashCodeIfNameExistButAuthorNot(){
        int EXPECTED_RESULT = 7;
        String name = "NAME";

        SchoolworkID schoolworkID = new SchoolworkID(name, null);

        EXPECTED_RESULT = 31 * EXPECTED_RESULT + name.hashCode();
        EXPECTED_RESULT = 31 * EXPECTED_RESULT;
        assertEquals(EXPECTED_RESULT, schoolworkID.hashCode());
    }

    @Test
    public void testHashCodeIfNameNotExistButAuthorExist(){
        int EXPECTED_RESULT = 7;
        String authorName = "AUTHOR";

        SchoolworkID schoolworkID = new SchoolworkID(null, authorName);

        EXPECTED_RESULT = 31 * EXPECTED_RESULT;
        EXPECTED_RESULT = 31 * EXPECTED_RESULT + authorName.hashCode();
        assertEquals(EXPECTED_RESULT, schoolworkID.hashCode());
    }

    @Test
    public void testHashCodeIfNameExistButAuthorExist(){
        int EXPECTED_RESULT = 7;
        String authorName = "AUTHOR";
        String name = "NAME";

        SchoolworkID schoolworkID = new SchoolworkID(name, authorName);

        EXPECTED_RESULT = 31 * EXPECTED_RESULT + name.hashCode();
        EXPECTED_RESULT = 31 * EXPECTED_RESULT + authorName.hashCode();
        assertEquals(EXPECTED_RESULT, schoolworkID.hashCode());
    }
}
