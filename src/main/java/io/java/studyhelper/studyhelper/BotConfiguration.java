package io.java.studyhelper.studyhelper;

import io.java.studyhelper.studyhelper.listeners.CommandEventListener;
import io.java.studyhelper.studyhelper.listeners.MentionEventListener;
import io.java.studyhelper.studyhelper.listeners.ReadyEventListener;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BotConfiguration {

    @Value("${botToken}")
    private String token;

    @Autowired
    private CommandEventListener commandEventListener;

    @Autowired
    private ReadyEventListener readyEventListener;

    @Autowired
    private MentionEventListener mentionEventListener;

    @Bean
    public void configure() throws LoginException {
        JDA jdaClient = JDABuilder.createDefault(token)
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .addEventListeners(readyEventListener)
                .addEventListeners(commandEventListener)
                .addEventListeners(mentionEventListener)
                .build();
    }
}
