package io.java.studyhelper.studyhelper.repository;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;

public interface CommandRepository {

    List<Command> getAllCommands();

    Command getCommandByName(String commandName);

    void addCommand(Command command);
}
