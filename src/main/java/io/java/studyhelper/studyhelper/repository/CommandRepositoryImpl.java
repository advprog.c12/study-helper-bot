package io.java.studyhelper.studyhelper.repository;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class CommandRepositoryImpl implements CommandRepository {
    Map<String, Command> commands = new HashMap<>();
    Map<String, String> commandAliases = new HashMap<>();

    @Override
    public List<Command> getAllCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public Command getCommandByName(String commandName) {
        String actualCommandName = commandAliases.get(commandName);
        return commands.get(actualCommandName);
    }

    @Override
    public void addCommand(Command command) {
        commands.put(command.getCommandName(), command);
        commandAliases.put(command.getCommandName(), command.getCommandName());

        for (String alias : command.getCommandAliases()) {
            commandAliases.put(alias, command.getCommandName());
        }
    }
}
