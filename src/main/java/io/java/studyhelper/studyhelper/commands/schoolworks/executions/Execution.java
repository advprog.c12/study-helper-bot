package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public interface Execution {
    String execute(List<String> args, Message message);
}
