package io.java.studyhelper.studyhelper.commands.archive.service;

import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.model.UserEntity;
import io.java.studyhelper.studyhelper.commands.archive.model.UserMessage;
import io.java.studyhelper.studyhelper.commands.archive.repository.ArchivingStatusRepository;
import io.java.studyhelper.studyhelper.commands.archive.repository.UserEntityRepository;
import io.java.studyhelper.studyhelper.commands.archive.repository.UserMessagesRepository;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArchiveServiceImpl implements ArchiveService {

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private ArchivingStatusRepository archivingStatusRepository;

    @Autowired
    private UserMessagesRepository userMessagesRepository;

    @Override
    public boolean userWantsArchive(User user, Guild guild) {
        UserEntity userEntity = getUserEntity(user);
        ArchivingStatus archivingStatus = getUserArchivingStatusForGuild(userEntity, guild);
        return archivingStatus.isArchiving();
    }

    @Override
    public void toggleArchivingStatus(User user, Guild guild) {
        UserEntity userEntity = getUserEntity(user);
        ArchivingStatus archivingStatus = getUserArchivingStatusForGuild(userEntity, guild);

        archivingStatus.setArchiving(!archivingStatus.isArchiving());

        archivingStatusRepository.save(archivingStatus);
    }

    @Override
    public void saveMessageForUser(User user, Message message) {
        Guild guild = message.getGuild();

        if (userWantsArchive(user, guild)) {
            UserEntity userEntity = getUserEntity(user);
            UserMessage userMessage = new UserMessage(userEntity.getUserId(), message);

            userMessage.setUserEntity(userEntity);
            userEntity.getMessages().add(userMessage);

            userMessagesRepository.save(userMessage);
            userEntityRepository.save(userEntity);
        }
    }

    @Override
    public synchronized List<UserMessage> getMessagesForUser(User user) {
        UserEntity userEntity = getUserEntity(user);
        List<UserMessage> messages = new ArrayList<>(userEntity.getMessages());

        userEntity.getMessages().clear();
        userEntityRepository.save(userEntity);

        return messages;
    }

    @Override
    public List<ArchivingStatus> getArchivingStatusesForUser(User user) {
        UserEntity userEntity = getUserEntity(user);
        List<ArchivingStatus> archivingStatuses = new ArrayList<>(userEntity.getStatuses());

        return archivingStatuses;
    }

    private UserEntity getUserEntity(User user) {
        UserEntity userEntity = userEntityRepository.findByUserId(user.getId());

        if (userEntity == null) {
            userEntity = new UserEntity(user.getId());
            userEntityRepository.save(userEntity);
        }

        return userEntityRepository.findByUserId(user.getId());
    }

    private ArchivingStatus getUserArchivingStatusForGuild(UserEntity userEntity, Guild guild) {
        ArchivingStatus archivingStatus = archivingStatusRepository.findByUserIdAndGuildId(
                userEntity.getUserId(), guild.getId());

        if (archivingStatus == null) {
            archivingStatus = new ArchivingStatus(userEntity.getUserId(), guild);

            archivingStatus.setUserEntity(userEntity);
            userEntity.getStatuses().add(archivingStatus);

            archivingStatusRepository.save(archivingStatus);
            userEntityRepository.save(userEntity);
        }

        return archivingStatusRepository.findByUserIdAndGuildId(userEntity.getUserId(), guild.getId());
    }
}
