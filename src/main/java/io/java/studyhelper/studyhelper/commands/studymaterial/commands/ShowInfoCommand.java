package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class ShowInfoCommand implements Command {

    private MaterialService materialService;

    public ShowInfoCommand(MaterialService materialService) {
        this.materialService = materialService;
    }

    @Override
    public String getCommandName() {
        return "showinfo";
    }

    @Override
    public String getCommandDescription() {
        return "show info command";
    }

    @Override
    public String getCommandCategory() {
        return "Study Material Storage";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        EmbedBuilder embed = new EmbedBuilder();

        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("Info about " + args.get(0))
                .setDescription(materialService.getMaterialValueByCategory(args.get(0), message.getGuild().getId()))
                .setTimestamp(Instant.now())
                .setFooter("© Study Helper Bot 2021");

        message.getChannel().sendMessage(embed.build()).queue();
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandUsage() {
        return "<info_category>";
    }

    @Override
    public boolean isGuildOnly() {
        return true;
    }

    @Override
    public String[] getCommandAliases() {
        return new String[]{"swi"};
    }
}
