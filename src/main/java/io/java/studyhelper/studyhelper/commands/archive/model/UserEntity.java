package io.java.studyhelper.studyhelper.commands.archive.model;

import java.util.List;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "user_entity")
@Data
@NoArgsConstructor
public class UserEntity {

    @Id
    @Column(name = "user_id", updatable = false, nullable = false)
    private String userId;

    @OneToMany(targetEntity = ArchivingStatus.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ArchivingStatus> statuses;

    @OneToMany(targetEntity = UserMessage.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<UserMessage> messages;

    public UserEntity(String userId) {
        this.userId = userId;
    }
}
