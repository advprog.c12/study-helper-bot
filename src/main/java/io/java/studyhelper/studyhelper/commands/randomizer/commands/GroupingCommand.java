package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.json.JSONObject;

public class GroupingCommand implements Command {

    private FetcherService fetcherService;

    public GroupingCommand(FetcherService fetcherService) {
        this.fetcherService = fetcherService;
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandName() {
        return "grouping";
    }

    @Override
    public String getCommandDescription() {
        return "Grouping the list of members as you need and return the list to the channel";
    }

    @Override
    public String getCommandCategory() {
        return "Randomizer";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        if (args.size() == 2) {
            String strategyChoosen = args.get(0);
            int jumlahGroup = Integer.parseInt(args.get(1));
            try {
                String jsonString = fetcherService.sendGetRequest(" http://studyhelper-tools.herokuapp.com/randomizer/grouping/" + strategyChoosen + "/" + jumlahGroup);
                JSONObject jsonObject = new JSONObject(jsonString);
                message.getChannel().sendMessage(jsonObject.getString("result")).queue();
            } catch (Exception ex) {
                message.getChannel().sendMessage("There was an error");
            }
        } else {
            message.getChannel().sendMessage(
                    "The proper usage would be: --grouping <group/groupby> <jumlah_group/jumlah_per_group>"
            ).queue();
        }
    }

    @Override
    public String getCommandUsage() {
        return "<group/groupby> <jumlah_group/jumlah_per_group>";
    }

}
