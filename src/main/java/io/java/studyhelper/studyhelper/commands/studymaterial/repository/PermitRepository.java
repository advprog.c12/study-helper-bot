package io.java.studyhelper.studyhelper.commands.studymaterial.repository;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermitRepository extends JpaRepository<Permit, String> {
    Permit findPermitByPermitValueAndServerId(String permitValue, String serverId);

    List<Permit> findAllByServerId(String serverId);
}
