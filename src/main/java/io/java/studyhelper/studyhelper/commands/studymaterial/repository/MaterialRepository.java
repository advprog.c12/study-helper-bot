package io.java.studyhelper.studyhelper.commands.studymaterial.repository;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Material;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialRepository extends JpaRepository<Material, String> {
    Material findByCategoryAndServerId(String category, String serverId);

    List<Material> findAllByServerId(String serverId);
}
