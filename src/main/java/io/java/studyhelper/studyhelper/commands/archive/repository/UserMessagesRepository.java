package io.java.studyhelper.studyhelper.commands.archive.repository;

import io.java.studyhelper.studyhelper.commands.archive.model.UserMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserMessagesRepository extends JpaRepository<UserMessage, String> {
}
