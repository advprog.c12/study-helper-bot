package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.schoolworks.executions.CopyreminderExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class CopyreminderCommand implements SchoolworksCommand {
    public CopyreminderExecution copyreminderExecution;
    public CopyreminderCommand(SchoolworksService schoolworksService){
        this.copyreminderExecution = new CopyreminderExecution(schoolworksService);
    }

    @Override
    public String getCommandName(){
        return "copyreminder";
    }

    @Override
    public String getCommandDescription(){
        return "Copy schoolwork reminder with name form other user";
    }

    @Override
    public String getCommandCategory(){
        return "Schoolwork Reminder";
    }

    @Override
    public Execution getExecution(){
        return this.copyreminderExecution;
    }

    @Override
    public void executeCommand(Message message, List<String> args){
        String response = copyreminderExecution.execute(args, message);
        message.getChannel().sendMessage(response).queue();
    }
}
