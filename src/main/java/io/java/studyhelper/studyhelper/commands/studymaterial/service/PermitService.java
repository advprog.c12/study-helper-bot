package io.java.studyhelper.studyhelper.commands.studymaterial.service;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import java.util.List;

public interface PermitService {
    Permit createPermit(String serverId, String permitValue);

    List<Permit> getAllPermit(String serverId);

    Permit getPermitByPermitValue(String permitValue, String serverId);

    void deletePermitByPermitValue(String permitValue, String serverId);

}
