package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.DelremindExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class DelremindCommand implements SchoolworksCommand {
    private DelremindExecution delremindExecution;

    public DelremindCommand(SchoolworksService schoolworksService){
        this.delremindExecution = new DelremindExecution(schoolworksService);
    }

    @Override
    public String getCommandName(){
        return "delremind";
    }

    @Override
    public String getCommandDescription(){
        return "Command to be used to delete schoolwork reminders based on the reminder's name.";
    }

    @Override
    public String getCommandCategory() {
        return "Schoolwork Reminder";
    }

    @Override
    public Execution getExecution(){
        return this.delremindExecution;
    }

    @Override
    public void executeCommand(Message message, List<String> args){
        String response = delremindExecution.execute(args, message);
        message.getChannel().sendMessage(response).queue();
    }
}
