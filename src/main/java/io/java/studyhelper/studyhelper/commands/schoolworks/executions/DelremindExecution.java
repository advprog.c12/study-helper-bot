package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.List;

public class DelremindExecution implements Execution {
    private SchoolworksService schoolworksService;
    public DelremindExecution(SchoolworksService schoolworksService){
        this.schoolworksService = schoolworksService;
    }
    @Override
    public String execute(List<String> args, Message message){
        User author;
        String name;

        if(args.size() != 1)
            return "Invalid command usage!";

        author = message.getAuthor();
        name = args.get(0);

        if(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(name, author.getName())){
            schoolworksService.deleteSchoolworkByNameAndAuthorName(name, author.getName());
            return String.format("Successfully deleted schoolwork with name **%s**", name);
        }

        return String.format("Schoolwork with name **%s** does not exist!", name);
    }
}
