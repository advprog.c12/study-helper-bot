package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.CheckreminderExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class CheckreminderCommand implements SchoolworksCommand {
    private CheckreminderExecution checkreminderExecution;

    public CheckreminderCommand(SchoolworksService schoolworksService){
        this.checkreminderExecution = new CheckreminderExecution(schoolworksService);
    }

    @Override
    public String getCommandName(){
        return "checkreminder";
    }

    @Override
    public String getCommandDescription(){
        return "Gives out description for reminder's name";
    }

    @Override
    public String getCommandCategory() {
        return "Schoolwork Reminder";
    }

    @Override
    public Execution getExecution(){
        return this.checkreminderExecution;
    }

    @Override
    public void executeCommand(Message message, List<String> args){
        String response = checkreminderExecution.execute(args, message);
        message.getChannel().sendMessage(response).queue();
    }
}
