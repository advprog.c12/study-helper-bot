package io.java.studyhelper.studyhelper.commands.calculator.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.json.JSONObject;

public class ToRadiansCommand implements Command {

    private FetcherService fetcher;
    private String prefix;

    public ToRadiansCommand(FetcherService fetcherService, String prefix) {
        this.fetcher = fetcherService;
        this.prefix = prefix;
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandName() {
        return "toRad";
    }

    @Override
    public String getCommandDescription() {
        return "Converting to Radians";
    }

    @Override
    public String getCommandCategory() {
        return "Calculator";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        try {
            String response = fetcher.sendGetRequest(
                    "https://studyhelper-tools.herokuapp.com/calculator/trigonometry/toRad/" + args.get(0));
            JSONObject jsonObject = new JSONObject(response);
            String reply = String.format("Result: %s", jsonObject.getString("result"));
            message.getChannel().sendMessage(reply).queue();
        } catch (Exception ex) {
            message.getChannel().sendMessage("There was an error with your calculations. " +
                    "Make sure to include the correct arguments." +
                    String.format("The proper usage would be: `%s%s %s`", prefix, getCommandName(), getCommandUsage()));
        }
    }

    @Override
    public String getCommandUsage() {
        return "<num>";
    }
}
