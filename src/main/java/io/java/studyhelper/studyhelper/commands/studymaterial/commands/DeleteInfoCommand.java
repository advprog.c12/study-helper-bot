package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.commands.studymaterial.verifier.PermissionVerifier;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class DeleteInfoCommand implements Command {
    private MaterialService materialService;
    private PermitService permitService;
    private String prefix;

    public DeleteInfoCommand(MaterialService materialService, PermitService permitService, String prefix) {
        this.materialService = materialService;
        this.permitService = permitService;
        this.prefix = prefix;
    }

    @Override
    public String getCommandName() {
        return "deleteinfo";
    }

    @Override
    public String getCommandDescription() {
        return "delete info command";
    }

    @Override
    public String getCommandCategory() {
        return "Study Material Storage";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        PermissionVerifier permissionVerifier =
                new PermissionVerifier(prefix, message.getGuild().getId(), permitService);
        boolean isAllowed = permissionVerifier.verify(message.getGuild().getMember(message.getAuthor()));
        String notifMsg;

        if (isAllowed) {
            materialService.deleteMaterialByCategory(args.get(0), message.getGuild().getId());
            notifMsg = String.format("Info **%s** has been deleted!", args.get(0));
        } else {
            notifMsg = permissionVerifier.getRejectedMessage();
        }

        message.getChannel().sendMessage(notifMsg).queue();
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandUsage() {
        return "<info_category>";
    }

    @Override
    public boolean isGuildOnly() {
        return true;
    }

    @Override
    public String[] getCommandAliases() {
        return new String[]{"dli"};
    }
}
