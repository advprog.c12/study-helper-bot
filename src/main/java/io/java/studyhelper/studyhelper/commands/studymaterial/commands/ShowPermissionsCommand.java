package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.commands.studymaterial.verifier.PermissionVerifier;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class ShowPermissionsCommand implements Command {

    private PermitService permitService;

    private String prefix;

    public ShowPermissionsCommand(PermitService permitService, String prefix) {
        this.permitService = permitService;
        this.prefix = prefix;
    }

    @Override
    public String getCommandName() {
        return "showpermissions";
    }

    @Override
    public String getCommandDescription() {
        return "show permissions command";
    }

    @Override
    public String getCommandCategory() {
        return "Study Material Storage";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        PermissionVerifier permissionVerifier =
                new PermissionVerifier(prefix, message.getGuild().getId(), permitService);

        String msg = permissionVerifier.getPermissionListToString();

        EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("Allowed Permission List")
                .setDescription(msg)
                .setTimestamp(Instant.now())
                .setFooter("© Study Helper Bot 2021");

        message.getChannel().sendMessage(embed.build()).queue();
    }

    @Override
    public boolean isGuildOnly() {
        return true;
    }

    @Override
    public String[] getCommandAliases() {
        return new String[]{"sp"};
    }
}
