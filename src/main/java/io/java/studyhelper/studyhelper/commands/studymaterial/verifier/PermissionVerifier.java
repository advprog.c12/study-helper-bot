package io.java.studyhelper.studyhelper.commands.studymaterial.verifier;

import io.java.studyhelper.studyhelper.commands.studymaterial.commands.ShowPermissionsCommand;
import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;

public class PermissionVerifier {

    private List<Permit> permits;

    private List<String> permissionList;

    private String prefix;

    private String serverId;

    private PermitService permitService;

    public PermissionVerifier(String prefix, String serverId, PermitService permitService) {
        this.prefix = prefix;
        this.serverId = serverId;
        this.permitService = permitService;

        initPermissions();

        permits = permitService.getAllPermit(serverId);

    }

    private void initPermissions() {
        if (permitService.getAllPermit(serverId).isEmpty()) {
            permitService.createPermit(serverId, Permission.ADMINISTRATOR.name());
            permitService.createPermit(serverId, Permission.BAN_MEMBERS.name());
            permitService.createPermit(serverId, Permission.KICK_MEMBERS.name());
            permitService.createPermit(serverId, Permission.MANAGE_CHANNEL.name());
            permitService.createPermit(serverId, Permission.MANAGE_PERMISSIONS.name());
            permitService.createPermit(serverId, Permission.MANAGE_ROLES.name());
            permitService.createPermit(serverId, Permission.MANAGE_SERVER.name());
        }

        permissionList = new ArrayList<>();

        for (Permission permission : Permission.values()) {
            permissionList.add(permission.name());
        }
    }

    public String addPermission(String permissionName) {
        if (permissionList.contains(permissionName)) {
            permitService.createPermit(serverId, permissionName);
            return "Permission Allowed!";
        } else {
            StringBuilder msg = new StringBuilder();
            msg.append("There's no such permission!\nPermissions that you can add:\n");

            for (String s : permissionList) {
                msg.append(String.format("- `%s`\n", s));
            }
            return msg.toString();
        }
    }

    public void deletePermission(String permissionName) {
        permitService.deletePermitByPermitValue(permissionName, serverId);
    }

    public boolean verify(Member member) {
        for (Permit permit : permits) {
            if (member != null && member.hasPermission(Permission.valueOf(permit.getPermitValue()))) {
                return true;
            }
        }
        return false;
    }

    public String getPermissionListToString() {
        StringBuilder permissionList = new StringBuilder();

        for (Permit permit : permits) {
            permissionList.append(String.format("- `%s`\n", permit.getPermitValue()));
        }

        return permissionList.toString();
    }

    public String getRejectedMessage() {
        ShowPermissionsCommand showPermissionsCommand = new ShowPermissionsCommand(permitService, prefix);
        String commandName = showPermissionsCommand.getCommandName();
        String aliases = showPermissionsCommand.getCommandAliases()[0];

        StringBuilder rejectMsg = new StringBuilder();

        rejectMsg.append("You don't have the permission to use this command!\n");
        rejectMsg.append(
                String.format(
                        "Use `%s%s` or `%s%s` to see the list of allowed permissions",
                        prefix, commandName, prefix, aliases));
        return rejectMsg.toString();
    }
}
