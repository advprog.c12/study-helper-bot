package io.java.studyhelper.studyhelper.commands.misc;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class EchoCommand implements Command {

    @Override
    public String getCommandName() {
        return "echo";
    }

    @Override
    public String getCommandDescription() {
        return "Basic echo command";
    }

    @Override
    public String getCommandCategory() {
        return "Miscellaneous";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        message.getChannel().sendMessage(
                String.join(" ", args)
        ).queue();
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandUsage() {
        return "<args>";
    }
}
