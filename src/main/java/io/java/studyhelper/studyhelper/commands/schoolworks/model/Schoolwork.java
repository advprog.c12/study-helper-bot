package io.java.studyhelper.studyhelper.commands.schoolworks.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "schoolwork")
@Data
@NoArgsConstructor
@IdClass(SchoolworkID.class)
public class Schoolwork {
    @Id
    @Column(name = "name", updatable = false)
    private String name;

    @Id
    @Column(name = "author_name", updatable = false)
    private String authorName;

    @Column(name = "description")
    private String description;

    public Schoolwork(String name, String description, String authorName){
        this.name = name;
        this.description = description;
        this.authorName = authorName;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
