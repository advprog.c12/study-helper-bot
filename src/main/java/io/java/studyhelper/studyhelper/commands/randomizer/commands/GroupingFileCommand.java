package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.json.JSONObject;


public class GroupingFileCommand implements Command {

    private FetcherService fetcherService;

    public GroupingFileCommand(FetcherService fetcherService) {
        this.fetcherService = fetcherService;
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandName() {
        return "groupingfile";
    }

    @Override
    public String getCommandDescription() {
        return "Grouping the list of members as you need and write the result to a file";
    }

    @Override
    public String getCommandCategory() {
        return "Randomizer";
    }

    @Override
    public void executeCommand(Message message, List<String> args)  {
        if (args.size() == 3) {
            String strategyChoosen = args.get(0);
            int jumlahGroup = Integer.parseInt(args.get(1));
            String namaFile = args.get(2);

            try {
                String jsonString = fetcherService.sendGetRequest(" http://studyhelper-tools.herokuapp.com/randomizer/groupingfile/" + strategyChoosen + "/" + jumlahGroup);
                JSONObject jsonObject = new JSONObject(jsonString);
                String hasil = jsonObject.getString("result");
                if (hasil.contains("Please")) {
                    message.getChannel().sendMessage(hasil).queue();
                } else {
                    if (createFile(namaFile + ".txt")
                            && writeToFile(namaFile + ".txt", jsonObject.getString("result")) == true) {
                        File file = new File(namaFile + ".txt");
                        message.getChannel().sendFile(file).queue(
                            (message1) -> {
                                file.delete();
                            }
                        );

                    } else if (createFile(namaFile + ".txt") == false) {
                        message.getChannel().sendMessage(
                                "An error occurred while creating new file, file "
                                        + args.get(2) + ".txt" + " already exist \n" + hasil
                        ).queue();
                    } else {
                        message.getChannel().sendMessage(
                                "An error occurred while writing to the file"
                                        + args.get(2) + ".txt" + "\n" + hasil
                        ).queue();
                    }
                }
            } catch (Exception ex) {
                message.getChannel().sendMessage("There was an error");
            }
        } else {
            message.getChannel().sendMessage(
                    "The proper usage would be: --groupingfile <group/groupby> "
                            + "<jumlah_group/jumlah_per_group> "
                            + "<file_path_and_name_for_output. e.g : text_3>"
            ).queue();
        }
    }

    public boolean createFile(String filePath) {
        try {
            File file = new File(filePath);
            if (file.createNewFile()) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writeToFile(String filename, String hasil) {
        try {
            FileWriter myWriter = new FileWriter(filename);
            myWriter.write(hasil);
            myWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String getCommandUsage() {
        return "<group/groupby> <jumlah_group/jumlah_per_group> "
                + "<file_name_for_output. e.g : text_3>";
    }

}
