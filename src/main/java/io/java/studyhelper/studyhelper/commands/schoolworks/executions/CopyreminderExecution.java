package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.List;

public class CopyreminderExecution implements Execution {
    private SchoolworksService schoolworksService;
    public CopyreminderExecution(SchoolworksService schoolworksService){
        this.schoolworksService = schoolworksService;
    }

    @Override
    public String execute(List<String> args, Message message){
        User author;
        String name;
        String targetAuthor;
        String authorName;
        String description;
        Schoolwork targetSchoolwork;

        if(args.size() != 2)
            return "Invalid command usage!";

        author = message.getAuthor();
        authorName = author.getName();

        targetAuthor = args.get(0);
        name = args.get(1);

        if(!schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(name, targetAuthor))
            return String.format("Schoolwork with **%s** by user **%s** does not exist!", name, targetAuthor);

        targetSchoolwork = schoolworksService.getSchoolworkByNameAndAuthorName(name, targetAuthor);
        description = targetSchoolwork.getDescription();

        if(schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(targetSchoolwork.getName(), authorName)){
            return String.format("Schoolwork with name **%s** already exists, use --delremind to remove it first!", targetSchoolwork.getName());
        }

        schoolworksService.createSchoolwork(name, description, authorName);
        return String.format("Successfully copied reminder by user **%s** with name **%s** to your reminders!", authorName, name);
    }
}
