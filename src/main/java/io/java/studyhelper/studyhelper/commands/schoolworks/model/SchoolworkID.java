package io.java.studyhelper.studyhelper.commands.schoolworks.model;

import java.io.Serializable;

public class SchoolworkID implements Serializable {
    private String name;
    private String authorName;

    public SchoolworkID(){

    }

    public SchoolworkID(String name, String authorName) {
        this.name = name;
        this.authorName = authorName;
    }

    public String getName(){
        return this.name;
    }

    public String getAuthorName(){
        return this.authorName;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAuthorName(String authorName){
        this.authorName = authorName;
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        
        if(obj == null || obj.getClass() != this.getClass())
            return false;

        SchoolworkID schoolworkID = (SchoolworkID) obj;
        return (this.name.equals(schoolworkID.name)) && (this.authorName.equals(schoolworkID.authorName));
    }

    @Override
    public int hashCode(){
        int hash = 7;
        hash = 31 * hash + (name == null ? 0 : name.hashCode());
        hash = 31 * hash + (authorName == null ? 0 : authorName.hashCode());
        return hash;
    }

}
