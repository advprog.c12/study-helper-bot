package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.RemindExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class RemindCommand implements SchoolworksCommand {
    private RemindExecution remindExecution;

    public RemindCommand(SchoolworksService schoolworksService){
        this.remindExecution = new RemindExecution(schoolworksService);
    }

    @Override
    public String getCommandName(){
        return "remind";
    }

    @Override
    public String getCommandDescription(){
        return "Basic reminding command";
    }

    @Override
    public String getCommandCategory() {
        return "Schoolwork Reminder";
    }

    @Override
    public Execution getExecution(){
        return this.remindExecution;
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        String response = remindExecution.execute(args, message);
        message.getChannel().sendMessage(response).queue();
    }
}
