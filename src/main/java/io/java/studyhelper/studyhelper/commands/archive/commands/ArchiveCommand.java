package io.java.studyhelper.studyhelper.commands.archive.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveService;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;

public class ArchiveCommand implements Command {

    private ArchiveService archiveService;
    private String prefix;

    public ArchiveCommand(ArchiveService archiveService, String prefix) {
        this.archiveService = archiveService;
        this.prefix = prefix;
    }

    @Override
    public String getCommandName() {
        return "archive";
    }

    @Override
    public String getCommandDescription() {
        return String.format("Multiple usage:\n" +
                "Use `%s%s` to toggle archiving on/off for the server\n" +
                "Use `%s%s status` to check the archiving status for the server\n" +
                "Use `%s%s list` to list all servers that are archiving",
                prefix, getCommandName(),
                prefix, getCommandName(),
                prefix, getCommandName());
    }

    @Override
    public String getCommandCategory() {
        return "Chat Archive";
    }

    @Override
    public String getCommandUsage() {
        return "<none or status or list>";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        if (args.isEmpty()) {
            sendArchiveToggleMessage(message);

        } else if (args.get(0).equals("status")) {
            sendArchiveStatusMessage(message);

        } else if (args.get(0).equals("list")) {
            sendArchiveListMessage(message);
        }
    }

    private void sendArchiveToggleMessage(Message message) {
        if (message.getChannel().getType() == ChannelType.TEXT) {
            archiveService.toggleArchivingStatus(message.getAuthor(), message.getGuild());
            message.getChannel().sendMessage(
                    archiveService.userWantsArchive(message.getAuthor(), message.getGuild()) ?
                            "Turning Archiving **On**" : "Turning Archiving **Off**"
            ).queue();

        } else if (message.getChannel().getType() == ChannelType.PRIVATE) {
            message.getChannel().sendMessage("This command cannot be executed inside DMs!").queue();
        }
    }

    private void sendArchiveStatusMessage(Message message) {
        if (message.getChannel().getType() == ChannelType.TEXT) {
            message.getChannel().sendMessage(
                    archiveService.userWantsArchive(message.getAuthor(), message.getGuild()) ?
                            "Archiving: **On**" : "Archiving: **Off**"
            ).queue();

        } else if (message.getChannel().getType() == ChannelType.PRIVATE) {
            message.getChannel().sendMessage("This command cannot be executed inside DMs!").queue();
        }
    }

    private void sendArchiveListMessage(Message message) {
        User author = message.getAuthor();
        List<ArchivingStatus> archivingStatuses = archiveService.getArchivingStatusesForUser(author);

        List<String> activeGuilds = archivingStatuses.stream()
                .filter(archivingStatus -> archivingStatus.isArchiving())
                .map(archivingStatus -> archivingStatus.getGuildName())
                .collect(Collectors.toList());

        if (activeGuilds.isEmpty()) {
            message.getAuthor().openPrivateChannel().queue(
                (privateChannel) -> privateChannel.sendMessage("I'm currently not archiving anywhere").queue(
                    (success) -> {
                        if (message.getChannel().getType() == ChannelType.TEXT) {
                            message.getChannel().sendMessage("I've sent you a DM, go check it out!").queue();
                        }
                    })
            );

        } else {
            message.getAuthor().openPrivateChannel().queue(
                (privateChannel) -> privateChannel.sendMessage(constructMessagesEmbed(activeGuilds)).queue(
                    (success) -> {
                        if (message.getChannel().getType() == ChannelType.TEXT) {
                            message.getChannel().sendMessage("I've sent you a DM, go check it out!").queue();
                        }
                    })
            );
        }
    }

    private MessageEmbed constructMessagesEmbed(List<String> guildNames) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("List of Archiving Servers")
                .setDescription(String.join("\n", guildNames))
                .setTimestamp(Instant.now())
                .setFooter("© StudyHelper Bot 2021", "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg");

        return embed.build();
    }
}
