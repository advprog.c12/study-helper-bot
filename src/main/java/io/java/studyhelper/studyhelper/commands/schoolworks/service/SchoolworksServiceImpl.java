package io.java.studyhelper.studyhelper.commands.schoolworks.service;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.repository.SchoolworksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SchoolworksServiceImpl implements SchoolworksService {
    @Autowired
    private SchoolworksRepository schoolworksRepository;

    @Override
    public void createSchoolwork(String name, String description, String authorName){
        Schoolwork schoolwork = new Schoolwork(name, description, authorName);
        schoolworksRepository.save(schoolwork);
    }

    @Override
    public Iterable<Schoolwork> getListSchoolwork(String authorName){
        return schoolworksRepository.findSchoolworksByAuthorName(authorName);
    }

    @Override
    public Schoolwork getSchoolworkByNameAndAuthorName(String name, String authorName){
        return schoolworksRepository.findByNameAndAuthorName(name, authorName);
    }

    @Override
    public void updateSchoolworkByNameAndAuthorName(String name, String authorName, Schoolwork schoolwork){
        schoolwork.setName(name);
        schoolwork.setAuthorName(authorName);
        schoolworksRepository.save(schoolwork);
    }

    @Override
    public void deleteSchoolworkByNameAndAuthorName(String name, String authorName){
        Schoolwork schoolwork = schoolworksRepository.findByNameAndAuthorName(name, authorName);
        schoolworksRepository.delete(schoolwork);
    }

    @Override
    public boolean checkSchoolworkWithoutNameAndAuthorNameExist(String name, String authorName){
        Schoolwork schoolwork = schoolworksRepository.findByNameAndAuthorName(name, authorName);
        return schoolwork != null;
    }

}
