package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.commands.SchoolworksCommand;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.UpdateremindExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class UpdateremindCommand implements SchoolworksCommand {
    private UpdateremindExecution updateremindExecution;

    public UpdateremindCommand(SchoolworksService schoolworksService){
        this.updateremindExecution = new UpdateremindExecution(schoolworksService);
    }

    @Override
    public String getCommandName(){
        return "updateremind";
    }

    @Override
    public String getCommandDescription(){
        return "Update command description from command name";
    }

    @Override
    public String getCommandCategory() {
        return "Schoolwork Reminder";
    }

    @Override
    public Execution getExecution(){
        return this.updateremindExecution;
    }

    @Override
    public void executeCommand(Message message, List<String> args){
        String response = updateremindExecution.execute(args, message);
        message.getChannel().sendMessage(response).queue();
    }
}
