package io.java.studyhelper.studyhelper.commands;

import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public interface Command {

    String getCommandName();

    String getCommandDescription();

    void executeCommand(Message message, List<String> args);

    default String getCommandCategory() {
        return "Other";
    }

    default boolean isArgsRequired() {
        return false;
    }

    default String getCommandUsage() {
        return "";
    }

    default boolean isGuildOnly() {
        return false;
    }

    default boolean isDMOnly() {
        return false;
    }

    default String[] getCommandAliases() {
        return new String[0];
    }
}
