package io.java.studyhelper.studyhelper.commands.archive.model;

import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.Message;

@Entity
@Table(name = "user_messages")
@Data
@NoArgsConstructor
public class UserMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "user_messages_id", updatable = false, nullable = false)
    private int userMessagesId;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "message_id")
    private String messageId;

    @Column(name = "message_guild")
    private String messageGuild;

    @Column(name = "message_channel")
    private String messageChannel;

    @Column(name = "message_author_name")
    private String messageAuthorName;

    @Column(name = "message_author_discriminator")
    private String messageAuthorDiscriminator;

    @Column(name = "message_content")
    private String messageContent;

    @Column(name = "message_link")
    private String messageLink;

    @ManyToOne
    private UserEntity userEntity;

    public UserMessage(String userId, Message message) {
        this.userId = userId;
        this.messageId = message.getId();
        this.messageGuild = message.getGuild().getName();
        this.messageChannel = message.getChannel().getName();
        this.messageAuthorName = message.getAuthor().getName();
        this.messageAuthorDiscriminator = message.getAuthor().getDiscriminator();
        this.messageContent = message.getContentRaw();
        this.messageLink = String.format("https://discord.com/channels/%s/%s/%s",
                message.getGuild().getId(), message.getChannel().getId(), message.getId());
    }
}
