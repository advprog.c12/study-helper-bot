package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.commands.studymaterial.verifier.PermissionVerifier;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class AllowPermissionCommand implements Command {
    private PermitService permitService;
    private String prefix;

    public AllowPermissionCommand(PermitService permitService, String prefix) {
        this.permitService = permitService;
        this.prefix = prefix;
    }

    @Override
    public String getCommandName() {
        return "allowpermission";
    }

    @Override
    public String getCommandDescription() {
        return "allow permission command";
    }

    @Override
    public String getCommandCategory() {
        return "Study Material Storage";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        String notifMsg;

        PermissionVerifier permissionVerifier =
                new PermissionVerifier(prefix, message.getGuild().getId(), permitService);

        if (message.getGuild().getMember(message.getAuthor()).isOwner()) {
            notifMsg = permissionVerifier.addPermission(args.get(0));
        } else {
            notifMsg = String.format("Command restricted to the server owner!");
        }

        message.getChannel().sendMessage(notifMsg).queue();
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandUsage() {
        return "<PERMISSION_NAME>";
    }

    @Override
    public boolean isGuildOnly() {
        return true;
    }

    @Override
    public String[] getCommandAliases() {
        return new String[]{"ap"};
    }
}
