package io.java.studyhelper.studyhelper.commands.archive.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.archive.model.UserMessage;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveService;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;

public class DumpCommand implements Command {

    private ArchiveService archiveService;

    public DumpCommand(ArchiveService archiveService) {
        this.archiveService = archiveService;
    }

    @Override
    public String getCommandName() {
        return "dump";
    }

    @Override
    public String getCommandDescription() {
        return "Dump all message that mentions you since your last dump";
    }

    @Override
    public String getCommandCategory() {
        return "Chat Archive";
    }

    @Override
    public boolean isDMOnly() {
        return true;
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        User author = message.getAuthor();
        List<UserMessage> userMessageList = archiveService.getMessagesForUser(author);

        if (userMessageList.isEmpty()) {
            message.getChannel().sendMessage("You don't have any mentions").queue();
        } else {
            message.getChannel().sendMessage(
                    constructMessagesEmbed(userMessageList)
            ).queue();
        }
    }

    private MessageEmbed constructMessagesEmbed(List<UserMessage> messages) {
        Map<String, List<UserMessage>> groupedMessages = messages.stream()
                .collect(Collectors.groupingBy(userMessage -> userMessage.getMessageGuild()));

        EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("Your recent mentions")
                .setTimestamp(Instant.now())
                .setFooter("© StudyHelper Bot 2021", "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg");

        for (String guild : groupedMessages.keySet()) {
            StringBuilder messagesContents = new StringBuilder();

            for (UserMessage message : groupedMessages.get(guild)) {
                messagesContents.append(String.format(
                        "**#%s** from **%s#%s**: %s [Jump to message](%s)\n",
                        message.getMessageChannel(), message.getMessageAuthorName(),
                        message.getMessageAuthorDiscriminator(), message.getMessageContent(),
                        message.getMessageLink()
                ));
            }

            embed.addField(guild, messagesContents.toString(), false);
        }

        return embed.build();
    }
}
