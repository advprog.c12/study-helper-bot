package io.java.studyhelper.studyhelper.commands.archive.repository;

import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArchivingStatusRepository extends JpaRepository<ArchivingStatus, String> {
    ArchivingStatus findByUserIdAndGuildId(String userId, String guildId);
}
