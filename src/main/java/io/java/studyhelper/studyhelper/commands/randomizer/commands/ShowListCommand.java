package io.java.studyhelper.studyhelper.commands.randomizer.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.json.JSONObject;

public class ShowListCommand implements Command {

    private FetcherService fetcherService;

    public ShowListCommand(FetcherService fetcherService) {
        this.fetcherService = fetcherService;
    }

    @Override
    public boolean isArgsRequired() {
        return false;
    }

    @Override
    public String getCommandName() {
        return "showlist";
    }

    @Override
    public String getCommandDescription() {
        return "Show list of members";
    }

    @Override
    public String getCommandCategory() {
        return "Randomizer";
    }

    @Override
    public void executeCommand(Message message, List<String> args)  {
        try {
            String jsonString = fetcherService.sendGetRequest(" http://studyhelper-tools.herokuapp.com/randomizer/showlist");
            JSONObject jsonObject = new JSONObject(jsonString);
            message.getChannel().sendMessage(jsonObject.getString("result")).queue();
        } catch (Exception ex) {
            message.getChannel().sendMessage("There was an error");
        }
    }
}
