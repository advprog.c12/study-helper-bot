package io.java.studyhelper.studyhelper.commands.info;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.repository.CommandRepository;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class HelpCommand implements Command {

    private String prefix;
    private CommandRepository commandRepository;

    public HelpCommand(CommandRepository commandRepository, String prefix) {
        this.commandRepository = commandRepository;
        this.prefix = prefix;
    }

    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public String getCommandDescription() {
        return "Lists all of my commands or get info about a specific command";
    }

    @Override
    public String getCommandCategory() {
        return "Information";
    }

    @Override
    public String getCommandUsage() {
        return "<none or command name>";
    }

    @Override
    public String[] getCommandAliases() {
        return new String[] {"h"};
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        if (args.isEmpty()) {
            List<Command> commands = commandRepository.getAllCommands();
            Map<String, List<Command>> commandsByCategory = commands.stream()
                    .collect(Collectors.groupingBy(command -> command.getCommandCategory()));

            EmbedBuilder helpEmbed = new EmbedBuilder();
            helpEmbed.setColor(Color.decode("0xfc6a51"))
                    .setTitle("StudyHelper\'s List of Commands")
                    .setDescription(String.format(
                            "Use `%shelp <command name>` to get help for a specific command", prefix))
                    .setThumbnail("https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg")
                    .setTimestamp(Instant.now())
                    .setFooter("© StudyHelper Bot 2021", "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg");

            for (String category : commandsByCategory.keySet()) {
                List<String> commandsByCategoryList = commandsByCategory.get(category).stream()
                        .map(command -> command.getCommandName()).collect(Collectors.toList());

                helpEmbed.addField(category, String.format("`%s`", String.join("`\n`", commandsByCategoryList)), true);
            }

            message.getChannel().sendMessage(helpEmbed.build()).queue();
            return;
        }

        Command command = commandRepository.getCommandByName(args.get(0));

        String commandName = command.getCommandName();
        String commandDescription = command.getCommandDescription();
        String commandCategory = command.getCommandCategory();
        String commandUsage = command.getCommandUsage();
        commandUsage = (commandUsage.length() == 0) ? commandUsage : " " + commandUsage;
        String[] commandAliases = command.getCommandAliases();

        EmbedBuilder specificHelpEmbed = new EmbedBuilder();
        specificHelpEmbed.setColor(Color.decode("0xfc6a51"))
                .setTitle(String.format("`%s%s`", prefix, commandName))
                .setDescription(commandDescription)
                .setThumbnail("https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg")
                .addField("Category", commandCategory, false)
                .addField("Usage", String.format("`%s%s%s`", prefix, commandName, commandUsage), false)
                .setTimestamp(Instant.now())
                .setFooter("© StudyHelper Bot 2021", "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg");

        if (commandAliases.length != 0) {
            specificHelpEmbed.addField("Aliases", String.format("`%s`", String.join("` `", commandAliases)), false);
        }

        message.getChannel().sendMessage(specificHelpEmbed.build()).queue();
    }
}
