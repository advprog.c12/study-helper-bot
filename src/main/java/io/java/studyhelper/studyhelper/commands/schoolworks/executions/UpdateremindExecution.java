package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.List;

public class UpdateremindExecution implements Execution {
    private SchoolworksService schoolworksService;
    public UpdateremindExecution(SchoolworksService schoolworksService){
        this.schoolworksService = schoolworksService;
    }
    @Override
    public String execute(List<String> args, Message message){
        User author;
        String name;
        String authorName;
        Schoolwork schoolwork;
        String description;

        if(args.size() < 2)
            return "Invalid command usage!";

        author = message.getAuthor();
        name = args.get(0);
        authorName = author.getName();
        schoolwork = schoolworksService.getSchoolworkByNameAndAuthorName(name, authorName);
        description = String.join(" ", args.subList(1, args.size()));

        if (!schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(name, authorName))
            return String.format("Schoolwork with name **%s** does not exist!", name);

        schoolwork.setDescription(description);
        schoolworksService.updateSchoolworkByNameAndAuthorName(name, authorName, schoolwork);
        return String.format("Updated reminder for %s named **%s** with description:\"%s\"", authorName, name, description);
    }
}
