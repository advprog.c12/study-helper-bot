package io.java.studyhelper.studyhelper.commands.archive.model;

import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.Guild;

@Entity
@Table(name = "archiving_status")
@Data
@NoArgsConstructor
public class ArchivingStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "archiving_status_id", updatable = false, nullable = false)
    private int archivingStatusId;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "guild_id")
    private String guildId;

    @Column(name = "guild_name")
    private String guildName;

    @Column(name = "status")
    private boolean archiving;

    @ManyToOne
    private UserEntity userEntity;

    public ArchivingStatus(String userId, Guild guild) {
        this.userId = userId;
        this.guildId = guild.getId();
        this.guildName = guild.getName();
        this.archiving = false;
    }
}
