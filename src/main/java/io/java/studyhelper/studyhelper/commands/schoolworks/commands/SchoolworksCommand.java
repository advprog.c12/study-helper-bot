package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;

public interface SchoolworksCommand extends Command {
    Execution getExecution();
}
