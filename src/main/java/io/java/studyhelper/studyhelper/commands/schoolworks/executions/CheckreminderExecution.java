package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.List;

public class CheckreminderExecution implements Execution {
    private SchoolworksService schoolworksService;
    public CheckreminderExecution(SchoolworksService schoolworksService){
        this.schoolworksService = schoolworksService;
    }

    @Override
    public String execute(List<String> args, Message message){
        User author;
        String name;
        Schoolwork schoolwork;

        if(args.size() != 1)
            return "Invalid command usage!";
        
        author = message.getAuthor();
        name = args.get(0);
        if(!schoolworksService.checkSchoolworkWithoutNameAndAuthorNameExist(name, author.getName()))
            return String.format("Schoolwork with name **%s** does not exist!", name);

        schoolwork = schoolworksService.getSchoolworkByNameAndAuthorName(name, author.getName());

        return String.format("From: %s\nReminder Name: %s\nDescription:\n%s", schoolwork.getAuthorName(), schoolwork.getName(), schoolwork.getDescription());
    }
}
