package io.java.studyhelper.studyhelper.commands.studymaterial.service;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Material;

public interface MaterialService {
    Material createMaterial(String serverId, String category, String materialValue);

    String getAllMaterial(String serverId);

    String getMaterialValueByCategory(String category, String serverId);

    void deleteMaterialByCategory(String category, String serverId);
}
