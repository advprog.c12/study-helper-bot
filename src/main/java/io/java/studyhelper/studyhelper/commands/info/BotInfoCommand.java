package io.java.studyhelper.studyhelper.commands.info;

import io.java.studyhelper.studyhelper.commands.Command;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class BotInfoCommand implements Command {

    @Override
    public String getCommandName() {
        return "botinfo";
    }

    @Override
    public String getCommandDescription() {
        return "Get more information about StudyHelper";
    }

    @Override
    public String getCommandCategory() {
        return "Information";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        EmbedBuilder embed = new EmbedBuilder();

        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("StudyHelper Bot Embed Message Template")
                .setAuthor("StudyHelper Bot", null, "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg")
                .setDescription("StudyHelper Bot is a Discord Bot that has a variety of tools "
                        + "that can aid teaching assistants and students in their learning process.")
                .setThumbnail("https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg")
                .addField(
                        "Developers",
                        "- Gabriel Enrique\n"
                                + "- Lazuardi P.P. Nusantara\n"
                                + "- Kaysa Syifa Wijdan Amin\n"
                                + "- Mario Serano\n"
                                + "- Matthew T.P.",
                        false
                )
                .addField("GitLab", "[Click Here](https://gitlab.com/advprog.c12/study-helper-bot)", true)
                .addField("Contribute", "[Click Here](https://gitlab.com/advprog.c12/study-helper-bot#contribute)", true)
                .setTimestamp(Instant.now())
                .setFooter("© StudyHelper Bot 2021", "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg");

        message.getChannel().sendMessage(embed.build()).queue();
    }
}
