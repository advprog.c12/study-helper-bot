package io.java.studyhelper.studyhelper.commands.schoolworks.executions;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.apache.commons.collections4.IterableUtils;

import java.util.List;

public class CheckremindersExecution implements Execution {
    private SchoolworksService schoolworksService;

    public CheckremindersExecution(SchoolworksService schoolworksService){
        this.schoolworksService = schoolworksService;
    }

    @Override
    public String execute(List<String> args, Message message){
        User author;
        String authorName;
        Iterable<Schoolwork> schoolworks;
        String response;

        author = message.getAuthor();
        authorName = author.getName();

        schoolworks = schoolworksService.getListSchoolwork(authorName);
        response = String.format("%s's Reminders:\n", authorName);

        if(IterableUtils.size(schoolworks) == 0){
            response += "There is no schoolworks listed!";
            return response;
        }

        for(Schoolwork schoolwork : schoolworks){
            response += String.format("%s\n",schoolwork.getName());
        }

        return response;
    }
}
