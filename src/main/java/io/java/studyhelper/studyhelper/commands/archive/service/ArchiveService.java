package io.java.studyhelper.studyhelper.commands.archive.service;

import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.model.UserMessage;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

public interface ArchiveService {

    boolean userWantsArchive(User user, Guild guild);

    void toggleArchivingStatus(User user, Guild guild);

    void saveMessageForUser(User user, Message message);

    List<UserMessage> getMessagesForUser(User user);

    List<ArchivingStatus> getArchivingStatusesForUser(User user);
}
