package io.java.studyhelper.studyhelper.commands.archive.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.archive.model.ArchivingStatus;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveService;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;

public class ArchiveListCommand implements Command {

    private ArchiveService archiveService;

    public ArchiveListCommand(ArchiveService archiveService) {
        this.archiveService = archiveService;
    }

    @Override
    public String getCommandName() {
        return "archiveList";
    }

    @Override
    public String getCommandDescription() {
        return "Lists all the servers that I'm archiving";
    }

    @Override
    public String getCommandCategory() {
        return "Chat Archive";
    }

    @Override
    public boolean isDMOnly() {
        return true;
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        User author = message.getAuthor();
        List<ArchivingStatus> archivingStatuses = archiveService.getArchivingStatusesForUser(author);

        List<String> activeGuilds = archivingStatuses.stream()
                .filter(archivingStatus -> archivingStatus.isArchiving())
                .map(archivingStatus -> archivingStatus.getGuildName())
                .collect(Collectors.toList());

        if (activeGuilds.isEmpty()) {
            message.getChannel().sendMessage("I'm currently not archiving anywhere").queue();
        } else {
            message.getChannel().sendMessage(
                constructMessagesEmbed(activeGuilds)
            ).queue();
        }
    }

    private MessageEmbed constructMessagesEmbed(List<String> guildNames) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("List of Archiving Servers")
                .setDescription(String.join("\n", guildNames))
                .setTimestamp(Instant.now())
                .setFooter("© StudyHelper Bot 2021", "https://i.ibb.co/R3mdCB9/studyhelper-thumbnail.jpg");

        return embed.build();
    }
}
