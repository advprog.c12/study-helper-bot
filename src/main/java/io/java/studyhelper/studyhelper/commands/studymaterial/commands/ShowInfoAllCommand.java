package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import java.awt.*;
import java.time.Instant;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class ShowInfoAllCommand implements Command {

    private MaterialService materialService;

    public ShowInfoAllCommand(MaterialService materialService) {
        this.materialService = materialService;
    }

    @Override
    public String getCommandName() {
        return "showinfoall";
    }

    @Override
    public String getCommandDescription() {
        return "show info all command";
    }

    @Override
    public String getCommandCategory() {
        return "Study Material Storage";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        EmbedBuilder embed = new EmbedBuilder();
        embed.setColor(Color.decode("0xfc6a51"))
                .setTitle("Study Material List")
                .setDescription(materialService.getAllMaterial(message.getGuild().getId()))
                .setTimestamp(Instant.now())
                .setFooter("© Study Helper Bot 2021");

        message.getChannel().sendMessage(embed.build()).queue();
    }

    @Override
    public boolean isGuildOnly() {
        return true;
    }

    @Override
    public String[] getCommandAliases() {
        return new String[]{"swia"};
    }
}
