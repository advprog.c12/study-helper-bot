package io.java.studyhelper.studyhelper.commands.studymaterial.model;

import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "permit")
@Data
@NoArgsConstructor
public class Permit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "permit_id", updatable = false, nullable = false)
    private int permitId;

    @Column(name = "server_id")
    private String serverId;

    @Column(name = "permit_value")
    private String permitValue;

    public Permit(String serverId, String permitValue) {
        this.serverId = serverId;
        this.permitValue = permitValue;
    }
}
