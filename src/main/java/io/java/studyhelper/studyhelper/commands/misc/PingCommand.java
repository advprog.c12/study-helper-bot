package io.java.studyhelper.studyhelper.commands.misc;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class PingCommand implements Command {

    @Override
    public String getCommandName() {
        return "ping";
    }

    @Override
    public String getCommandDescription() {
        return "Ping pong!";
    }

    @Override
    public String getCommandCategory() {
        return "Miscellaneous";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        message.getChannel().sendMessage(
                "Pong!"
        ).queue();
    }
}
