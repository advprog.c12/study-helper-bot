package io.java.studyhelper.studyhelper.commands.schoolworks.repository;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SchoolworksRepository extends JpaRepository<Schoolwork, String> {
    Schoolwork findByNameAndAuthorName(String name, String authorName);
    List<Schoolwork> findSchoolworksByAuthorName(String authorName);
}
