package io.java.studyhelper.studyhelper.commands.misc;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.json.JSONObject;

public class APITestCommand implements Command {

    private FetcherService fetcherService;

    public APITestCommand(FetcherService fetcherService) {
        this.fetcherService = fetcherService;
    }

    @Override
    public String getCommandName() {
        return "api";
    }

    @Override
    public String getCommandDescription() {
        return "API testing";
    }

    @Override
    public String getCommandCategory() {
        return "Miscellaneous";
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandUsage() {
        return "<get id> or <post id data>";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        if (args.get(0).equals("get")) {
            String id = args.get(1);

            try {
                String jsonString = fetcherService.sendGetRequest("http://localhost:8080/sample/" + id);
                JSONObject jsonObject = new JSONObject(jsonString);
                message.getChannel().sendMessage(jsonObject.getString("dataString")).queue();
            } catch (Exception ex) {
                message.getChannel().sendMessage("There was an error");
            }

        } else if (args.get(0).equals("post")) {
            String payload = String.format("{\n"
                    + "    \"sampleId\": \"%s\",\n"
                    + "    \"dataString\": \"%s\"\n"
                    + "}", args.get(1), args.get(2));

            try {
                String jsonString = fetcherService.sendPostRequest("http://localhost:8080/sample", payload);
                JSONObject jsonObject = new JSONObject(jsonString);
                message.getChannel().sendMessage("Successfully sent").queue();
            } catch (Exception ex) {
                message.getChannel().sendMessage("There was an error");
            }
        }
    }
}
