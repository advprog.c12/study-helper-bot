package io.java.studyhelper.studyhelper.commands.studymaterial.model;

import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "material")
@Data
@NoArgsConstructor
public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "material_id", updatable = false, nullable = false)
    private int materialId;

    @Column(name = "server_id")
    private String serverId;

    @Column(name = "category")
    private String category;

    @Column(name = "material_value")
    private String materialValue;

    public Material(String serverId, String category, String materialValue) {
        this.serverId = serverId;
        this.category = category;
        this.materialValue = materialValue;
    }
}
