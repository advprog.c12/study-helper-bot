package io.java.studyhelper.studyhelper.commands.studymaterial.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.commands.studymaterial.verifier.PermissionVerifier;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class SaveInfoCommand implements Command {

    private MaterialService materialService;
    private PermitService permitService;
    private String prefix;

    public SaveInfoCommand(MaterialService materialService, PermitService permitService, String prefix) {
        this.materialService = materialService;
        this.permitService = permitService;
        this.prefix = prefix;
    }

    @Override
    public String getCommandName() {
        return "saveinfo";
    }

    @Override
    public String getCommandDescription() {
        return "save info command";
    }

    @Override
    public String getCommandCategory() {
        return "Study Material Storage";
    }

    @Override
    public void executeCommand(Message message, List<String> args) {
        PermissionVerifier permissionVerifier =
                new PermissionVerifier(prefix, message.getGuild().getId(), permitService);
        boolean isAllowed = permissionVerifier.verify(message.getGuild().getMember(message.getAuthor()));
        String notifMsg;

        if (isAllowed) {
            materialService.createMaterial(
                    message.getGuild().getId(), args.get(0), String.join("\n", args.subList(1, args.size())));
            notifMsg = String.format("Info **%s** successfully created!", args.get(0));
        } else {
            notifMsg = permissionVerifier.getRejectedMessage();
        }

        message.getChannel().sendMessage(notifMsg).queue();
    }

    @Override
    public boolean isArgsRequired() {
        return true;
    }

    @Override
    public String getCommandUsage() {
        return "<info_category> <info_value> or <info_category> <info_value_1> <info_value_2> ... <info_value_n>";
    }

    @Override
    public boolean isGuildOnly() {
        return true;
    }

    @Override
    public String[] getCommandAliases() {
        return new String[]{"svi"};
    }
}
