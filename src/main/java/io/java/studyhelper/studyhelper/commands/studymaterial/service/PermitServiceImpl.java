package io.java.studyhelper.studyhelper.commands.studymaterial.service;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Material;
import io.java.studyhelper.studyhelper.commands.studymaterial.model.Permit;
import io.java.studyhelper.studyhelper.commands.studymaterial.repository.PermitRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermitServiceImpl implements PermitService {
    @Autowired
    private PermitRepository permitRepository;

    @Override
    public Permit createPermit(String serverId, String permitValue) {
        Permit permit = permitRepository
                .findPermitByPermitValueAndServerId(permitValue, serverId);
        if (permit == null) {
            permitRepository.save(new Permit(serverId, permitValue));
        }

        return permit;
    }

    @Override
    public List<Permit> getAllPermit(String serverId) {
        return permitRepository.findAllByServerId(serverId);
    }

    @Override
    public Permit getPermitByPermitValue(String permitValue, String serverId) {
        return permitRepository.findPermitByPermitValueAndServerId(permitValue, serverId);
    }

    @Override
    public void deletePermitByPermitValue(String permitValue, String serverId) {
        Permit permit = permitRepository.findPermitByPermitValueAndServerId(permitValue, serverId);
        permitRepository.delete(permit);
    }
}
