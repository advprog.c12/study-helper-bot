package io.java.studyhelper.studyhelper.commands.schoolworks.commands;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.CheckremindersExecution;
import io.java.studyhelper.studyhelper.commands.schoolworks.executions.Execution;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import net.dv8tion.jda.api.entities.Message;

import java.util.List;

public class CheckremindersCommand implements SchoolworksCommand {
    private CheckremindersExecution checkremindersExecution;

    public CheckremindersCommand(SchoolworksService schoolworksService){
        this.checkremindersExecution = new CheckremindersExecution(schoolworksService);
    }

    @Override
    public String getCommandName(){
        return "checkreminders";
    }

    @Override
    public String getCommandDescription(){
        return "Get all reminders that is created by that user";
    }

    @Override
    public String getCommandCategory() {
        return "Schoolwork Reminder";
    }

    @Override
    public Execution getExecution(){
        return this.checkremindersExecution;
    }

    @Override
    public void executeCommand(Message message, List<String> args){
        String response = checkremindersExecution.execute(args, message);
        message.getChannel().sendMessage(response).queue();
    }
}
