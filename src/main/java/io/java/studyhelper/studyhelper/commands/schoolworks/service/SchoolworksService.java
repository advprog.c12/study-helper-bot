package io.java.studyhelper.studyhelper.commands.schoolworks.service;

import io.java.studyhelper.studyhelper.commands.schoolworks.model.Schoolwork;

public interface SchoolworksService {
    void createSchoolwork(String name, String description, String authorName);

    Iterable<Schoolwork> getListSchoolwork(String authorName);

    Schoolwork getSchoolworkByNameAndAuthorName(String name, String authorName);

    void updateSchoolworkByNameAndAuthorName(String name, String authorName, Schoolwork schoolwork);

    void deleteSchoolworkByNameAndAuthorName(String name, String authorName);

    boolean checkSchoolworkWithoutNameAndAuthorNameExist(String name, String authorName);
}
