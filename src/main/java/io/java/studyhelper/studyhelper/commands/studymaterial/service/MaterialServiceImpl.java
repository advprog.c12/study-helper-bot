package io.java.studyhelper.studyhelper.commands.studymaterial.service;

import io.java.studyhelper.studyhelper.commands.studymaterial.model.Material;
import io.java.studyhelper.studyhelper.commands.studymaterial.repository.MaterialRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MaterialServiceImpl implements MaterialService {
    @Autowired
    private MaterialRepository materialRepository;

    @Override
    public Material createMaterial(String serverId, String category, String materialValue) {
        Material newMaterial = materialRepository.findByCategoryAndServerId(category, serverId);

        if (newMaterial == null) {
            materialRepository.save(new Material(serverId, category, materialValue));
        } else {
            int matId = newMaterial.getMaterialId();

            newMaterial.setMaterialId(matId);
            newMaterial.setServerId(serverId);
            newMaterial.setCategory(category);
            newMaterial.setMaterialValue(materialValue);

            materialRepository.save(newMaterial);
        }

        return newMaterial;
    }

    @Override
    public String getAllMaterial(String serverId) {
        StringBuilder result = new StringBuilder();
        List<Material> materials = materialRepository.findAllByServerId(serverId);

        for (int i = 0; i < materials.size(); i++) {
            String[] values = materials.get(i).getMaterialValue().split("\\n");
            StringBuilder newValues = new StringBuilder();
            for (String value : values) {
                newValues.append(String.format("- %s\n", value));
            }

            result.append(String.format("\n**%s. %s**\n%s", (i + 1), materials.get(i).getCategory(), newValues));
        }
        if (result.toString().equalsIgnoreCase("")) {
            result.append("The list is currently empty...");
        }
        return result.toString();
    }

    @Override
    public String getMaterialValueByCategory(String category, String serverId) {
        String[] values = materialRepository
                .findByCategoryAndServerId(category, serverId)
                .getMaterialValue().split("\\n");
        StringBuilder resMaterial = new StringBuilder();
        if (values.length == 1) {
            resMaterial = new StringBuilder(values[0]);
        } else {
            for (int i = 0; i < values.length; i++) {
                resMaterial.append(String.format("%s. %s \n", (i + 1), values[i]));
            }
        }

        return resMaterial.toString();
    }

    @Override
    public void deleteMaterialByCategory(String category, String serverId) {
        Material material = materialRepository.findByCategoryAndServerId(category, serverId);
        materialRepository.delete(material);
    }
}
