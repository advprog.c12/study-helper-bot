package io.java.studyhelper.studyhelper.service;

import net.dv8tion.jda.api.entities.Message;

public interface CommandService {
    void processMessage(Message message);
}
