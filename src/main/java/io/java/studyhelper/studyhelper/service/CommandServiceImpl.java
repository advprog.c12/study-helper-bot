package io.java.studyhelper.studyhelper.service;

import static io.java.studyhelper.studyhelper.core.tools.CommandParser.extractCommandArgs;
import static io.java.studyhelper.studyhelper.core.tools.CommandParser.extractCommandName;

import io.java.studyhelper.studyhelper.commands.Command;
import io.java.studyhelper.studyhelper.core.validator.CommandValidator;
import io.java.studyhelper.studyhelper.core.validator.ValidityStatus;
import io.java.studyhelper.studyhelper.repository.CommandRepository;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CommandServiceImpl implements CommandService {

    private CommandValidator commandValidator = new CommandValidator();

    @Autowired
    private CommandRepository commandRepository;

    @Value("${botPrefix}")
    private String prefix;

    @Override
    public void processMessage(Message message) {
        String commandName = extractCommandName(message, prefix);
        List<String> commandArgs = extractCommandArgs(message);
        Command command = commandRepository.getCommandByName(commandName);

        ValidityStatus validityStatus = commandValidator.validate(message, commandArgs, command);

        if (validityStatus == ValidityStatus.VALID) {
            command.executeCommand(message, commandArgs);
        } else {
            sendInvalidResponse(validityStatus, message, command);
        }
    }

    private void sendInvalidResponse(ValidityStatus validityStatus, Message message, Command command) {
        String reply = "";

        switch (validityStatus) {
            case INVALID_COMMAND:
                return;

            case INVALID_ARGS:
                reply = String.format(
                        "You did not provide any arguments!\n"
                                + "The proper usage would be: `%s%s %s`",
                        prefix, command.getCommandName(), command.getCommandUsage()
                );
                break;

            case INVALID_CHANNEL_GUILD_ONLY:
                reply = "This command cannot be executed inside DMs!";
                break;

            case INVALID_CHANNEL_DM_ONLY:
                reply = "I can only execute this command inside DMs!";
                break;
        }

        message.getChannel().sendMessage(reply).queue();
    }
}
