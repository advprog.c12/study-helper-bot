package io.java.studyhelper.studyhelper.service;

import net.dv8tion.jda.api.entities.Message;

public interface MentionService {
    void saveMessage(Message message);
}
