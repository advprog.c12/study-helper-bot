package io.java.studyhelper.studyhelper.service;

import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveService;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MentionServiceImpl implements MentionService {

    @Autowired
    private ArchiveService archiveService;

    @Override
    public void saveMessage(Message message) {
        List<User> mentionedUsers = message.getMentionedUsers();

        for (User user : mentionedUsers) {
            if (archiveService.userWantsArchive(user, message.getGuild())) {
                archiveService.saveMessageForUser(user, message);
            }
        }
    }
}
