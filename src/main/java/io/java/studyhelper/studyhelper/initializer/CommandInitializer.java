package io.java.studyhelper.studyhelper.initializer;

import io.java.studyhelper.studyhelper.commands.archive.commands.ArchiveCommand;
import io.java.studyhelper.studyhelper.commands.archive.commands.DumpCommand;
import io.java.studyhelper.studyhelper.commands.archive.service.ArchiveService;
import io.java.studyhelper.studyhelper.commands.calculator.commands.*;
import io.java.studyhelper.studyhelper.commands.info.BotInfoCommand;
import io.java.studyhelper.studyhelper.commands.info.HelpCommand;
import io.java.studyhelper.studyhelper.commands.misc.*;
import io.java.studyhelper.studyhelper.commands.randomizer.commands.*;
import io.java.studyhelper.studyhelper.commands.schoolworks.commands.*;
import io.java.studyhelper.studyhelper.commands.schoolworks.service.SchoolworksService;
import io.java.studyhelper.studyhelper.commands.studymaterial.commands.*;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.MaterialService;
import io.java.studyhelper.studyhelper.commands.studymaterial.service.PermitService;
import io.java.studyhelper.studyhelper.core.fetcher.FetcherService;
import io.java.studyhelper.studyhelper.repository.CommandRepository;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommandInitializer {

    @Value("${botPrefix}")
    private String prefix;

    @Autowired
    private CommandRepository commandRepository;

    @Autowired
    private ArchiveService archiveService;

    @Autowired
    private SchoolworksService schoolworksService;

    @Autowired
    private MaterialService materialService;

    @Autowired
    private PermitService permitService;

    @Autowired
    private FetcherService fetcherService;

    @PostConstruct
    public void init() {

        // General commands
        commandRepository.addCommand(new PingCommand());
        commandRepository.addCommand(new EchoCommand());
        commandRepository.addCommand(new BotInfoCommand());
        commandRepository.addCommand(new HelpCommand(commandRepository, prefix));
        commandRepository.addCommand(new APITestCommand(fetcherService));

        // Chat Archive commands
        commandRepository.addCommand(new ArchiveCommand(archiveService, prefix));
        commandRepository.addCommand(new DumpCommand(archiveService));

        // Schoolwork Reminder commands
        commandRepository.addCommand(new RemindCommand(schoolworksService));
        commandRepository.addCommand(new CheckremindersCommand(schoolworksService));
        commandRepository.addCommand(new CheckreminderCommand(schoolworksService));
        commandRepository.addCommand(new DelremindCommand(schoolworksService));
        commandRepository.addCommand(new UpdateremindCommand(schoolworksService));
        commandRepository.addCommand(new CopyreminderCommand(schoolworksService));

        // Calculator commands
        commandRepository.addCommand(new AdditionCommand(fetcherService, prefix));
        commandRepository.addCommand(new SubtractionCommand(fetcherService, prefix));
        commandRepository.addCommand(new MultiplicationCommand(fetcherService, prefix));
        commandRepository.addCommand(new DivisionCommand(fetcherService, prefix));
        commandRepository.addCommand(new ModuloCommand(fetcherService, prefix));
        commandRepository.addCommand(new MeanCommand(fetcherService, prefix));
        commandRepository.addCommand(new MedianCommand(fetcherService, prefix));
        commandRepository.addCommand(new ModeCommand(fetcherService, prefix));
        commandRepository.addCommand(new CosecantCommand(fetcherService, prefix));
        commandRepository.addCommand(new CosinusCommand(fetcherService, prefix));
        commandRepository.addCommand(new CotangentCommand(fetcherService, prefix));
        commandRepository.addCommand(new SecantCommand(fetcherService, prefix));
        commandRepository.addCommand(new SinusCommand(fetcherService, prefix));
        commandRepository.addCommand(new TangentCommand(fetcherService, prefix));
        commandRepository.addCommand(new ToDegreesCommand(fetcherService, prefix));
        commandRepository.addCommand(new ToRadiansCommand(fetcherService, prefix));
        commandRepository.addCommand(new CNDFCommand(fetcherService, prefix));
        commandRepository.addCommand(new ToDecimalCommand(fetcherService, prefix));
        commandRepository.addCommand(new ToBinaryCommand(fetcherService, prefix));

        // Study Material Storage commands
        commandRepository.addCommand(new SaveInfoCommand(materialService, permitService, prefix));
        commandRepository.addCommand(new ShowInfoCommand(materialService));
        commandRepository.addCommand(new ShowInfoAllCommand(materialService));
        commandRepository.addCommand(new DeleteInfoCommand(materialService, permitService, prefix));
        commandRepository.addCommand(new ShowPermissionsCommand(permitService, prefix));
        commandRepository.addCommand(new AllowPermissionCommand(permitService, prefix));
        commandRepository.addCommand(new DeletePermissionCommand(permitService, prefix));

        // Randomizer commands
        commandRepository.addCommand(new GroupingCommand(fetcherService));
        commandRepository.addCommand(new GroupingFileCommand(fetcherService));
        commandRepository.addCommand(new RegisterCommand(fetcherService));
        commandRepository.addCommand(new DeleteCommand(fetcherService));
        commandRepository.addCommand(new ShowListCommand(fetcherService));
    }
}
