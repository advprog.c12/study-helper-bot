package io.java.studyhelper.studyhelper.core.fetcher;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.springframework.stereotype.Service;

@Service
public class FetcherServiceImpl implements FetcherService {

    @Override
    public String sendGetRequest(String requestUrl) throws Exception {
        URL urlForGetRequest = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();

        connection.setRequestMethod("GET");

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer jsonString = new StringBuffer();

        String line;
        while ((line = br.readLine()) != null) {
            jsonString.append(line);
        }
        br.close();

        connection.disconnect();

        return jsonString.toString();
    }

    @Override
    public String sendPostRequest(String requestUrl, String payload) throws Exception {
        URL url = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        writer.write(payload);
        writer.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer jsonString = new StringBuffer();

        String line;
        while ((line = br.readLine()) != null) {
            jsonString.append(line);
        }
        br.close();

        connection.disconnect();

        return jsonString.toString();
    }
}
