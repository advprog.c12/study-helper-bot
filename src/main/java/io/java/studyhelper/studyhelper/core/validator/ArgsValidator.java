package io.java.studyhelper.studyhelper.core.validator;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.beans.factory.annotation.Value;

public class ArgsValidator implements Validator {

    @Value("${botPrefix}")
    private String prefix;

    private Validator nextValidator;

    public void setNextValidator(Validator validator) {
        this.nextValidator = validator;
    }

    @Override
    public ValidityStatus validate(Message message, List<String> args, Command command) {
        boolean commandNeedArgs = command.isArgsRequired();
        boolean noArgsProvided = args.isEmpty();

        if (commandNeedArgs && noArgsProvided) {
            return ValidityStatus.INVALID_ARGS;
        }

        return nextValidator == null ? ValidityStatus.VALID : nextValidator.validate(message, args, command);
    }
}
