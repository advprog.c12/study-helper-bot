package io.java.studyhelper.studyhelper.core.tools;

import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class CommandParser {

    private static String[] tokenizeMessage(Message message) {
        return message.getContentRaw().split("\\s+");
    }

    public static String extractCommandName(Message message, String prefix) {
        return tokenizeMessage(message)[0].substring(prefix.length());
    }

    public static List<String> extractCommandArgs(Message message) {
        String[] tokenizedMessage = tokenizeMessage(message);
        List<String> messageArgs = new ArrayList<>();

        for (int i = 1; i < tokenizedMessage.length; i++) {
            messageArgs.add(tokenizedMessage[i]);
        }

        return messageArgs;
    }
}
