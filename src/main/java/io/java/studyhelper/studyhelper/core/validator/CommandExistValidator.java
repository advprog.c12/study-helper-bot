package io.java.studyhelper.studyhelper.core.validator;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class CommandExistValidator implements Validator {

    private Validator nextValidator;

    public void setNextValidator(Validator validator) {
        this.nextValidator = validator;
    }

    @Override
    public ValidityStatus validate(Message message, List<String> args, Command command) {
        if (command == null) {
            return ValidityStatus.INVALID_COMMAND;
        }

        return nextValidator == null ? ValidityStatus.VALID : nextValidator.validate(message, args, command);
    }
}
