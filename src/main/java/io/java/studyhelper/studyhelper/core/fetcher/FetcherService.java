package io.java.studyhelper.studyhelper.core.fetcher;

public interface FetcherService {

    String sendGetRequest(String requestUrl) throws Exception;

    String sendPostRequest(String requestUrl, String payload) throws Exception;
}
