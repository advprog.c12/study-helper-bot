package io.java.studyhelper.studyhelper.core.validator;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public interface Validator {
    ValidityStatus validate(Message message, List<String> args, Command command);
}
