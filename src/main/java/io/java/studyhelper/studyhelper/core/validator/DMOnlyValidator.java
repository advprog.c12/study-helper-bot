package io.java.studyhelper.studyhelper.core.validator;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;

public class DMOnlyValidator implements Validator {

    private Validator nextValidator;

    public void setNextValidator(Validator validator) {
        this.nextValidator = validator;
    }

    @Override
    public ValidityStatus validate(Message message, List<String> args, Command command) {
        boolean commandIsDMOnly = command.isDMOnly();
        MessageChannel messageChannel = message.getChannel();

        if (commandIsDMOnly && messageChannel.getType() != ChannelType.PRIVATE) {
            return ValidityStatus.INVALID_CHANNEL_DM_ONLY;
        }

        return nextValidator == null ? ValidityStatus.VALID : nextValidator.validate(message, args, command);
    }
}
