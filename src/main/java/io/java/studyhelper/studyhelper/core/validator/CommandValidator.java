package io.java.studyhelper.studyhelper.core.validator;

import io.java.studyhelper.studyhelper.commands.Command;
import java.util.List;
import net.dv8tion.jda.api.entities.Message;

public class CommandValidator implements Validator {

    private Validator validatorChain;

    public CommandValidator() {
        CommandExistValidator commandExistValidator = new CommandExistValidator();
        ArgsValidator argsValidator = new ArgsValidator();
        GuildOnlyValidator guildOnlyValidator = new GuildOnlyValidator();
        DMOnlyValidator dmOnlyValidator = new DMOnlyValidator();

        commandExistValidator.setNextValidator(argsValidator);
        argsValidator.setNextValidator(guildOnlyValidator);
        guildOnlyValidator.setNextValidator(dmOnlyValidator);

        this.validatorChain = commandExistValidator;
    }

    @Override
    public ValidityStatus validate(Message message, List<String> args, Command command) {
        return validatorChain.validate(message, args, command);
    }
}
