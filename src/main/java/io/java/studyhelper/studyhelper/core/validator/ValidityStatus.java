package io.java.studyhelper.studyhelper.core.validator;

public enum ValidityStatus {
    VALID, INVALID_COMMAND, INVALID_ARGS, INVALID_CHANNEL_GUILD_ONLY, INVALID_CHANNEL_DM_ONLY
}
