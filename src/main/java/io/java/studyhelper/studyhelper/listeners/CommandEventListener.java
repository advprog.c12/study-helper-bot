package io.java.studyhelper.studyhelper.listeners;

import io.java.studyhelper.studyhelper.service.CommandService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CommandEventListener extends ListenerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(CommandEventListener.class);

    @Value("${botPrefix}")
    private String prefix;

    @Autowired
    private CommandService commandService;

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        Message message = event.getMessage();

        if (message.getAuthor().isBot()) {
            return;
        }

        if (message.getContentRaw().startsWith(prefix)) {
            try {
                commandService.processMessage(message);
            } catch (Exception ex) {
                message.getChannel().sendMessage(
                        "There was an error when trying to execute your command..."
                ).queue();
                logger.error("Failed to process message: " + message.getContentRaw());
                ex.printStackTrace();
            }
        }
    }
}
