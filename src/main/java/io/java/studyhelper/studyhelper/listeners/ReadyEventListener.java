package io.java.studyhelper.studyhelper.listeners;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.SelfUser;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ReadyEventListener extends ListenerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ReadyEventListener.class);

    @Value("${botPrefix}")
    private String prefix;

    private Activity[] activities;
    private int currentActivity;
    private ScheduledExecutorService threadPool;

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        final JDA jdaClient = event.getJDA();
        final SelfUser self = jdaClient.getSelfUser();

        configurePresence(jdaClient);

        logger.info("Ready!");
        logger.info(String.format(
                "Logged in as %s#%s", self.getName(), self.getDiscriminator()
        ));
        logger.info(String.format(
                "Bot prefix is '%s'", prefix
        ));
    }

    private void configurePresence(JDA jdaClient) {
        this.activities = new Activity[] {
                Activity.watching("Class Recordings"),
                Activity.listening(prefix + "help")
        };

        this.currentActivity = 0;

        this.threadPool = Executors.newSingleThreadScheduledExecutor(r -> {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        });

        this.threadPool.scheduleWithFixedDelay(() -> {
            jdaClient.getPresence().setActivity(activities[currentActivity]);
            currentActivity = (currentActivity + 1) % activities.length;
        }, 0, 30, TimeUnit.SECONDS);
    }
}
