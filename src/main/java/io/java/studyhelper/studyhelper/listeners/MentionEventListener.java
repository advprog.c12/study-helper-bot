package io.java.studyhelper.studyhelper.listeners;

import io.java.studyhelper.studyhelper.service.MentionService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MentionEventListener extends ListenerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(MentionEventListener.class);

    @Autowired
    private MentionService mentionService;

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        Message message = event.getMessage();

        if (message.getAuthor().isBot()) {
            return;
        }

        if (!message.getMentionedUsers().isEmpty()) {
            try {
                mentionService.saveMessage(message);
            } catch (Exception ex) {
                logger.error("Failed to save message: " + message.getContentRaw());
                ex.printStackTrace();
            }
        }
    }
}
