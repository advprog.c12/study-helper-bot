[![pipeline status](https://gitlab.com/advprog.c12/study-helper-bot/badges/master/pipeline.svg)](https://gitlab.com/advprog.c12/study-helper-bot/-/commits/master)
[![coverage report](https://gitlab.com/advprog.c12/study-helper-bot/badges/master/coverage.svg)](https://gitlab.com/advprog.c12/study-helper-bot/-/commits/master)

[![studyhelper tools](https://img.shields.io/badge/-StudyHelper%20Tools-blue.svg?logo=discord&logoColor=ffffff)](https://gitlab.com/advprog.c12/studyhelper-tools)

# StudyHelper Bot (Discord Bot)

StudyHelper Bot is a Discord Bot that has a variety of tools which can help teaching assistants and students in their learning process.

## Key Features

### Chat Archive

Archives any chat that contains mentions to you. You can then retrieve all of those messages later via the bot’s DMs. 

### Schoolwork Reminder

Allow specific superusers, such as admin or whatever roles defined, to create schoolwork reminders that will be stored in the database. After which, users will be able to type the command to remind them what schoolwork that is currently available.

### Calculator

Helps the students to calculate things such as arithmetics and statistics. Students can calculate their tasks using this feature.

### Study Material Storage

Allow users with certain roles, such as admin or roles with similar levels, to store certain topic related materials; i.e., links or other information that can be called later on by any member of the server.

### Randomizer

Helps to create groups for group projects with random people that are registered in the server randomly or based on certain filters.

## Developers

| Name                               | NPM        | GitLab                                                | PIC                    |
| ---------------------------------- | ---------- | ----------------------------------------------------- | ---------------------- |
| Gabriel Enrique                    | 1906293032 | [gabriel.enrique](https://gitlab.com/gabriel.enrique) | Chat Archive           |
| Lazuardi P.P. Nusantara            | 1906398225 | [ardinusantara](https://gitlab.com/ardinusantara)     | Study Material Storage |
| Kaysa Syifa Wijdan Amin            | 1906400362 | [kaysaswa](https://gitlab.com/kaysaswa)               | Randomizer             |
| Mario Serano                       | 1906398484 | [mario_serano](https://gitlab.com/mario_serano)       | Schoolworks Reminder   |
| Matthew T.P.                       | 1906308500 | [matthewsolomon_](https://gitlab.com/matthewsolomon_) | Calculator             |

## Contributing

#### 1. Create a Discord Application in the [Discord Developer Portal](https://discord.com/login?redirect_to=%2Fdevelopers%2Fapplications)

 - Add a Bot User to your Application by clicking the `Add Bot` button under the `Bot` tab
 - Take note of your Bot's token which can be obtained under the `Bot` tab
 - Note that you need to keep your Bot's token to yourself
 - You can regenerate a new token if you need to by clicking the `Regenerate` button

#### 2. Invite your Bot to a Discord Server using the link generated from [Discord Permissions Calculator](https://discordapi.com/permissions.html)

 - You need to provide your Bot's `Client ID` which can be obtained from the `OAuth2` tab

#### 3. Clone this repository

```shell
git clone https://gitlab.com/advprog.c12/study-helper-bot.git
```

#### 4. Add the `botToken` variable in the `application.properties` file with the value of your Bot's token

 - Remember to not include your Bot's token when committing and pushing
 - Regenerate a new token as soon as possible if this happens

```
botToken=yourTokenHere
```

#### 5. (Optional) Change the value of `botPrefix` to your liking

 - This becomes mandatory if you are testing your Bot inside the [Advprog - C12 Discord Server](https://discord.gg/S8Ah9zCVMM)
 - Make sure that the prefix which you will use has not already been used by any other developers inside the server

```
botPrefix=yourPrefixHere
```
